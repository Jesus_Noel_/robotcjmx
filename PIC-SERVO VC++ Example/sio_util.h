[DllImport("NMCLIB04.dll", EntryPoint = "ErrorPrinting", CharSet = Unicode)]
void ErrorPrinting(int f);

[DllImport("NMCLIB04.dll", EntryPoint = "ErrorMsgBox", CharSet = Unicode)]
int ErrorMsgBox(char *msgstr);

[DllImport("NMCLIB04.dll", EntryPoint = "SimpleMsgBox", CharSet = Unicode)]
int SimpleMsgBox(char *msgstr);

[DllImport("NMCLIB04.dll", EntryPoint = "SioOpen", CharSet = Unicode)]
HANDLE SioOpen(char *name, unsigned int baudrate);

[DllImport("NMCLIB04.dll", EntryPoint = "SioPutChars", CharSet = Unicode)]
BOOL SioPutChars(HANDLE ComPort, char *stuff, int n);

[DllImport("NMCLIB04.dll", EntryPoint = "SioGetChars", CharSet = Unicode)]
DWORD SioGetChars(HANDLE ComPort, char *stuff, int n);

[DllImport("NMCLIB04.dll", EntryPoint = "SioTest", CharSet = Unicode)]
DWORD SioTest(HANDLE ComPort);

[DllImport("NMCLIB04.dll", EntryPoint = "SioClrInbuf", CharSet = Unicode)]
BOOL SioClrInbuf(HANDLE ComPort);

[DllImport("NMCLIB04.dll", EntryPoint = "SioChangeBaud", CharSet = Unicode)]
BOOL SioChangeBaud(HANDLE ComPort, unsigned int baudrate);

[DllImport("NMCLIB04.dll", EntryPoint = "SioClose", CharSet = Unicode)]
BOOL SioClose(HANDLE ComPort);







