#pragma once

namespace PICSERVOVCExample
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	//Added
	using namespace System::IO;
	using namespace System::Text;




	//For using DLL's add this namespace (Needed for DllImportAttribute)
	using namespace System::Runtime::InteropServices;

#include <windows.h>
#include <stdio.h>

#include "sio_util.h"
#include "nmccom.h"
#include "picservo.h"
#include "picio.h"
//finite value of "pi"
#define pi 3.141592653589793
	int inicioOK = 0;
	int nummod = 0;
	int numdesp = 0; // Number of end-effector diplacements
	int efector = 0; // gripper ON/OFF [1,0]
//variables que definen la posici�n final articular
	int M1posDes = 0;
	int M2posDes = 0;
	int M3posDes = 0;
//variables que definen la velocidad y aceleraci�n de cualquier movimiento
	int VelGen = 0;
	int AcelGen = 0;
//variables que definen la posici�n final del robot
	double Xp=0;
	double Yp=0;
	double Zp=0;
//Variables correspondientes al control
	int kp1;
	int kd1;
	int ki1;
	int kp2;
	int kd2;
	int ki2;
	int kp3;
	int kd3;
	int ki3;
	int kp01;
	int kd01;
	int ki01;
	int kp02;
	int kd02;
	int ki02;
	int kp03;
	int kd03;
	int ki03;
	int antihome1;
	int antihome2;
	int antihome3;
	unsigned char addr; //module address for PIC-IO

//Variables relacionadas a la cinem�tica inversa; se hicieron globales solo para su 
// consulta en cualquier momento siempre y cuando la funcion: inversekinematics(Xp,Yp,Zp)
// o tambi�n la funci�n func_2()no est� ejecutandose en otro hilo.
	double Phi[4];
	double TH1[4];
	double TH1ant[4];
	double TH1deg[4];
	double TH1pulses[4];
	double TH1pulses_ant[4];
	double Pf[4];	//columna
	double Cf[16];	//columna
	double Ti[16];	//columna

	double a[4];
	double b[4];
	double h[4];
	double H[4];
	double M[4];
	double N[4];
	double Q[4];
	double R[4];
	int sleepingtime;

// Variables para la secuencia, maximo 50 puntos
	double VXp[50];
	double VYp[50];
	double VZp[50];
	int Vefector[50];
	int Vespera[50];
	int VVelGen[50];
	int VAcelGen[50];

#define AMP_DISABLE 0

	/// <summary> 
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the 
	///          'Resource File Name' property for the managed resource compiler tool 
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public __gc class Form1 : public System::Windows::Forms::Form
	{	
	public:
		Form1(void)
		{
			InitializeComponent();
		}

	protected:
		void Dispose(Boolean disposing)
		{
			if (disposing && components)
			{
				components->Dispose();
			}
			__super::Dispose(disposing);
		}
	private: System::Windows::Forms::Button *  InitButton;
	private: System::Windows::Forms::Button *  SetGainsButton;



	private: System::Windows::Forms::Button *  ExitButton;

	private: System::Windows::Forms::TextBox*  textBox1;
	private: System::Windows::Forms::Timer*  timer1;

	private: System::Windows::Forms::TextBox*  textBox2;
	private: System::Windows::Forms::TextBox*  textBox3;

	private: System::Windows::Forms::Label*  label2;
	private: System::Windows::Forms::Label*  label3;
	private: System::Windows::Forms::Label*  label4;
	private: System::Windows::Forms::Label*  label5;




	private: System::Windows::Forms::Button*  button1;














	private: System::Windows::Forms::Label*  label10;
	private: System::Windows::Forms::Label*  label11;
	private: System::Windows::Forms::Label*  label12;
	private: System::Windows::Forms::Label*  label13;
	private: System::Windows::Forms::Label*  label14;
	private: System::Windows::Forms::Label*  label15;
	private: System::Windows::Forms::Button*  button4;







	private: System::Windows::Forms::TextBox*  textBox5;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown1;
	private: System::Windows::Forms::Label*  label19;
	private: System::Windows::Forms::Label*  label20;
	private: System::Windows::Forms::Label*  label21;





	private: System::Windows::Forms::Label*  label25;


	private: System::Windows::Forms::Label*  label26;
	private: System::Windows::Forms::Label*  label27;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown2;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown3;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown4;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown5;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown6;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown7;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown8;
	private: System::Windows::Forms::DomainUpDown*  domainUpDown9;
	private: System::Windows::Forms::Button*  button5;
	private: System::Windows::Forms::Button*  button6;


	private: System::Windows::Forms::GroupBox*  groupBox1;
	private: System::Windows::Forms::RadioButton*  radioButton2;
	private: System::Windows::Forms::RadioButton*  radioButton1;








	private: System::Windows::Forms::NumericUpDown*  numericUpDown1;
	private: System::Windows::Forms::NumericUpDown*  numericUpDown2;
	private: System::Windows::Forms::NumericUpDown*  numericUpDown3;
	private: System::Windows::Forms::Button*  button10;
	private: System::Windows::Forms::NumericUpDown*  numericUpDown4;
	private: System::Windows::Forms::NumericUpDown*  numericUpDown5;
	private: System::Windows::Forms::Label*  label29;
	private: System::Windows::Forms::Label*  label30;


	private: System::Windows::Forms::Timer*  timer2;

	private: System::Windows::Forms::SaveFileDialog*  saveFileDialog1;


	private: System::Windows::Forms::NumericUpDown*  numericUpDown6;
	private: System::Windows::Forms::Label*  label1;
	private: System::Windows::Forms::Button*  button13;
private: System::Windows::Forms::NumericUpDown*  numericUpDown7;
private: System::Windows::Forms::NumericUpDown*  numericUpDown8;
private: System::Windows::Forms::NumericUpDown*  numericUpDown9;
private: System::Windows::Forms::Button*  button2;
private: System::Windows::Forms::GroupBox*  groupBox2;
private: System::Windows::Forms::GroupBox*  groupBox3;
private: System::Windows::Forms::GroupBox*  groupBox4;
private: System::Windows::Forms::RadioButton*  radioButton4;
private: System::Windows::Forms::RadioButton*  radioButton3;
private: System::Windows::Forms::GroupBox*  groupBox5;
private: System::Windows::Forms::Button*  button9;
private: System::Windows::Forms::Button*  button8;
private: System::Windows::Forms::Button*  button7;
private: System::Windows::Forms::Button*  button3;
private: System::Windows::Forms::GroupBox*  groupBox6;
private: System::Windows::Forms::Button*  button11;
private: System::Windows::Forms::CheckBox*  checkBox1;
private: System::Windows::Forms::ListBox*  listBox1;
private: System::Windows::Forms::ListBox*  listBox2;
private: System::Windows::Forms::ListBox*  listBox3;
private: System::Windows::Forms::ListBox*  listBox4;
private: System::Windows::Forms::ListBox*  listBox5;
private: System::Windows::Forms::Button*  button12;
private: System::Windows::Forms::TextBox*  textBox4;
private: System::Windows::Forms::Label*  label6;
private: System::Windows::Forms::Button*  button14;
private: System::Windows::Forms::Button*  button15;










	private: System::ComponentModel::IContainer*  components;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (new System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager*  resources = (new System::ComponentModel::ComponentResourceManager(__typeof(Form1)));
			this->InitButton = (new System::Windows::Forms::Button());
			this->SetGainsButton = (new System::Windows::Forms::Button());
			this->ExitButton = (new System::Windows::Forms::Button());
			this->textBox1 = (new System::Windows::Forms::TextBox());
			this->timer1 = (new System::Windows::Forms::Timer(this->components));
			this->textBox2 = (new System::Windows::Forms::TextBox());
			this->textBox3 = (new System::Windows::Forms::TextBox());
			this->label2 = (new System::Windows::Forms::Label());
			this->label3 = (new System::Windows::Forms::Label());
			this->label4 = (new System::Windows::Forms::Label());
			this->label5 = (new System::Windows::Forms::Label());
			this->button1 = (new System::Windows::Forms::Button());
			this->label10 = (new System::Windows::Forms::Label());
			this->label11 = (new System::Windows::Forms::Label());
			this->label12 = (new System::Windows::Forms::Label());
			this->label13 = (new System::Windows::Forms::Label());
			this->label14 = (new System::Windows::Forms::Label());
			this->label15 = (new System::Windows::Forms::Label());
			this->button4 = (new System::Windows::Forms::Button());
			this->textBox5 = (new System::Windows::Forms::TextBox());
			this->domainUpDown1 = (new System::Windows::Forms::DomainUpDown());
			this->label19 = (new System::Windows::Forms::Label());
			this->label20 = (new System::Windows::Forms::Label());
			this->label21 = (new System::Windows::Forms::Label());
			this->label25 = (new System::Windows::Forms::Label());
			this->label26 = (new System::Windows::Forms::Label());
			this->label27 = (new System::Windows::Forms::Label());
			this->domainUpDown2 = (new System::Windows::Forms::DomainUpDown());
			this->domainUpDown3 = (new System::Windows::Forms::DomainUpDown());
			this->domainUpDown4 = (new System::Windows::Forms::DomainUpDown());
			this->domainUpDown5 = (new System::Windows::Forms::DomainUpDown());
			this->domainUpDown6 = (new System::Windows::Forms::DomainUpDown());
			this->domainUpDown7 = (new System::Windows::Forms::DomainUpDown());
			this->domainUpDown8 = (new System::Windows::Forms::DomainUpDown());
			this->domainUpDown9 = (new System::Windows::Forms::DomainUpDown());
			this->button5 = (new System::Windows::Forms::Button());
			this->button6 = (new System::Windows::Forms::Button());
			this->groupBox1 = (new System::Windows::Forms::GroupBox());
			this->checkBox1 = (new System::Windows::Forms::CheckBox());
			this->radioButton2 = (new System::Windows::Forms::RadioButton());
			this->radioButton1 = (new System::Windows::Forms::RadioButton());
			this->label29 = (new System::Windows::Forms::Label());
			this->label30 = (new System::Windows::Forms::Label());
			this->numericUpDown4 = (new System::Windows::Forms::NumericUpDown());
			this->numericUpDown5 = (new System::Windows::Forms::NumericUpDown());
			this->numericUpDown1 = (new System::Windows::Forms::NumericUpDown());
			this->numericUpDown2 = (new System::Windows::Forms::NumericUpDown());
			this->numericUpDown3 = (new System::Windows::Forms::NumericUpDown());
			this->button10 = (new System::Windows::Forms::Button());
			this->timer2 = (new System::Windows::Forms::Timer(this->components));
			this->saveFileDialog1 = (new System::Windows::Forms::SaveFileDialog());
			this->numericUpDown6 = (new System::Windows::Forms::NumericUpDown());
			this->label1 = (new System::Windows::Forms::Label());
			this->button13 = (new System::Windows::Forms::Button());
			this->numericUpDown7 = (new System::Windows::Forms::NumericUpDown());
			this->numericUpDown8 = (new System::Windows::Forms::NumericUpDown());
			this->numericUpDown9 = (new System::Windows::Forms::NumericUpDown());
			this->button2 = (new System::Windows::Forms::Button());
			this->groupBox2 = (new System::Windows::Forms::GroupBox());
			this->groupBox3 = (new System::Windows::Forms::GroupBox());
			this->button9 = (new System::Windows::Forms::Button());
			this->button8 = (new System::Windows::Forms::Button());
			this->button7 = (new System::Windows::Forms::Button());
			this->button3 = (new System::Windows::Forms::Button());
			this->groupBox4 = (new System::Windows::Forms::GroupBox());
			this->radioButton4 = (new System::Windows::Forms::RadioButton());
			this->radioButton3 = (new System::Windows::Forms::RadioButton());
			this->groupBox5 = (new System::Windows::Forms::GroupBox());
			this->groupBox6 = (new System::Windows::Forms::GroupBox());
			this->button11 = (new System::Windows::Forms::Button());
			this->listBox1 = (new System::Windows::Forms::ListBox());
			this->listBox2 = (new System::Windows::Forms::ListBox());
			this->listBox3 = (new System::Windows::Forms::ListBox());
			this->listBox4 = (new System::Windows::Forms::ListBox());
			this->listBox5 = (new System::Windows::Forms::ListBox());
			this->button12 = (new System::Windows::Forms::Button());
			this->textBox4 = (new System::Windows::Forms::TextBox());
			this->label6 = (new System::Windows::Forms::Label());
			this->button14 = (new System::Windows::Forms::Button());
			this->button15 = (new System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown4))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown5))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown1))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown2))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown3))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown6))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown7))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown8))->BeginInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown9))->BeginInit();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->SuspendLayout();
			// 
			// InitButton
			// 
			this->InitButton->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->InitButton->Image = (__try_cast<System::Drawing::Image*  >(resources->GetObject(S"InitButton.Image")));
			this->InitButton->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->InitButton->Location = System::Drawing::Point(12, 15);
			this->InitButton->Name = S"InitButton";
			this->InitButton->Size = System::Drawing::Size(180, 45);
			this->InitButton->TabIndex = 0;
			this->InitButton->Text = S"INICIAR\r\nCONEXION USB";
			this->InitButton->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->InitButton->Click += new System::EventHandler(this, &Form1::InitButton_Click);
			// 
			// SetGainsButton
			// 
			this->SetGainsButton->BackColor = System::Drawing::SystemColors::Control;
			this->SetGainsButton->Enabled = false;
			this->SetGainsButton->Location = System::Drawing::Point(323, 112);
			this->SetGainsButton->Name = S"SetGainsButton";
			this->SetGainsButton->Size = System::Drawing::Size(90, 46);
			this->SetGainsButton->TabIndex = 1;
			this->SetGainsButton->Text = S"Ganancias originales";
			this->SetGainsButton->UseVisualStyleBackColor = false;
			this->SetGainsButton->Click += new System::EventHandler(this, &Form1::SetGainsButton_Click);
			// 
			// ExitButton
			// 
			this->ExitButton->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->ExitButton->Image = (__try_cast<System::Drawing::Image*  >(resources->GetObject(S"ExitButton.Image")));
			this->ExitButton->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->ExitButton->Location = System::Drawing::Point(12, 290);
			this->ExitButton->Name = S"ExitButton";
			this->ExitButton->Size = System::Drawing::Size(180, 45);
			this->ExitButton->TabIndex = 5;
			this->ExitButton->Text = S"  SALIR";
			this->ExitButton->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->ExitButton->Click += new System::EventHandler(this, &Form1::ExitButton_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(10, 90);
			this->textBox1->Name = S"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(71, 22);
			this->textBox1->TabIndex = 7;
			this->textBox1->TextChanged += new System::EventHandler(this, &Form1::textBox1_TextChanged);
			// 
			// timer1
			// 
			this->timer1->Interval = 50;
			this->timer1->Tick += new System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(110, 90);
			this->textBox2->Name = S"textBox2";
			this->textBox2->ReadOnly = true;
			this->textBox2->Size = System::Drawing::Size(71, 22);
			this->textBox2->TabIndex = 9;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(210, 90);
			this->textBox3->Name = S"textBox3";
			this->textBox3->ReadOnly = true;
			this->textBox3->Size = System::Drawing::Size(71, 22);
			this->textBox3->TabIndex = 10;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->BackColor = System::Drawing::Color::Red;
			this->label2->Location = System::Drawing::Point(13, 34);
			this->label2->Name = S"label2";
			this->label2->Size = System::Drawing::Size(52, 16);
			this->label2->TabIndex = 12;
			this->label2->Text = S"Motor 1";
			this->label2->Click += new System::EventHandler(this, &Form1::label2_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BackColor = System::Drawing::Color::Red;
			this->label3->Location = System::Drawing::Point(107, 34);
			this->label3->Name = S"label3";
			this->label3->Size = System::Drawing::Size(52, 16);
			this->label3->TabIndex = 13;
			this->label3->Text = S"Motor 2";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BackColor = System::Drawing::Color::Red;
			this->label4->Location = System::Drawing::Point(207, 33);
			this->label4->Name = S"label4";
			this->label4->Size = System::Drawing::Size(52, 16);
			this->label4->TabIndex = 14;
			this->label4->Text = S"Motor 3";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(564, 7);
			this->label5->Name = S"label5";
			this->label5->Size = System::Drawing::Size(0, 16);
			this->label5->TabIndex = 15;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(31, 206);
			this->button1->Name = S"button1";
			this->button1->Size = System::Drawing::Size(81, 31);
			this->button1->TabIndex = 19;
			this->button1->Text = S"Ejecutar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += new System::EventHandler(this, &Form1::button1_Click);
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->BackColor = System::Drawing::Color::Red;
			this->label10->Location = System::Drawing::Point(27, 27);
			this->label10->Name = S"label10";
			this->label10->Size = System::Drawing::Size(38, 16);
			this->label10->TabIndex = 33;
			this->label10->Text = S"Eje X";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->BackColor = System::Drawing::Color::Red;
			this->label11->Location = System::Drawing::Point(126, 27);
			this->label11->Name = S"label11";
			this->label11->Size = System::Drawing::Size(40, 16);
			this->label11->TabIndex = 34;
			this->label11->Text = S"Eje Y";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->BackColor = System::Drawing::Color::Red;
			this->label12->Location = System::Drawing::Point(226, 27);
			this->label12->Name = S"label12";
			this->label12->Size = System::Drawing::Size(38, 16);
			this->label12->TabIndex = 35;
			this->label12->Text = S"Eje Z";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(50, 74);
			this->label13->Name = S"label13";
			this->label13->Size = System::Drawing::Size(15, 16);
			this->label13->TabIndex = 36;
			this->label13->Text = S"x";
			this->label13->Click += new System::EventHandler(this, &Form1::label13_Click);
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(151, 74);
			this->label14->Name = S"label14";
			this->label14->Size = System::Drawing::Size(15, 16);
			this->label14->TabIndex = 37;
			this->label14->Text = S"y";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(249, 74);
			this->label15->Name = S"label15";
			this->label15->Size = System::Drawing::Size(15, 16);
			this->label15->TabIndex = 38;
			this->label15->Text = S"z";
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::SystemColors::Control;
			this->button4->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->button4->Image = (__try_cast<System::Drawing::Image*  >(resources->GetObject(S"button4.Image")));
			this->button4->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button4->Location = System::Drawing::Point(12, 70);
			this->button4->Name = S"button4";
			this->button4->Size = System::Drawing::Size(180, 45);
			this->button4->TabIndex = 39;
			this->button4->Text = S"POSICION\r\nDE INICIO";
			this->button4->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += new System::EventHandler(this, &Form1::button4_Click);
			// 
			// textBox5
			// 
			this->textBox5->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->textBox5->Font = (new System::Drawing::Font(S"Arial", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->textBox5->Location = System::Drawing::Point(11, 37);
			this->textBox5->Multiline = true;
			this->textBox5->Name = S"textBox5";
			this->textBox5->ReadOnly = true;
			this->textBox5->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->textBox5->Size = System::Drawing::Size(417, 161);
			this->textBox5->TabIndex = 44;
			this->textBox5->TextChanged += new System::EventHandler(this, &Form1::textBox5_TextChanged);
			// 
			// domainUpDown1
			// 
			this->domainUpDown1->Location = System::Drawing::Point(64, 56);
			this->domainUpDown1->Name = S"domainUpDown1";
			this->domainUpDown1->Size = System::Drawing::Size(80, 22);
			this->domainUpDown1->TabIndex = 45;
			this->domainUpDown1->Text = S"Kp1";
			this->domainUpDown1->Click += new System::EventHandler(this, &Form1::domainUpDown1_Click);
			this->domainUpDown1->SelectedItemChanged += new System::EventHandler(this, &Form1::domainUpDown1_SelectedItemChanged);
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(84, 37);
			this->label19->Name = S"label19";
			this->label19->Size = System::Drawing::Size(24, 16);
			this->label19->TabIndex = 46;
			this->label19->Text = S"Kp";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(171, 37);
			this->label20->Name = S"label20";
			this->label20->Size = System::Drawing::Size(24, 16);
			this->label20->TabIndex = 47;
			this->label20->Text = S"Kd";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(256, 37);
			this->label21->Name = S"label21";
			this->label21->Size = System::Drawing::Size(20, 16);
			this->label21->TabIndex = 48;
			this->label21->Text = S"Ki";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(6, 62);
			this->label25->Name = S"label25";
			this->label25->Size = System::Drawing::Size(52, 16);
			this->label25->TabIndex = 52;
			this->label25->Text = S"Motor 1";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(6, 101);
			this->label26->Name = S"label26";
			this->label26->Size = System::Drawing::Size(52, 16);
			this->label26->TabIndex = 53;
			this->label26->Text = S"Motor 2";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(6, 141);
			this->label27->Name = S"label27";
			this->label27->Size = System::Drawing::Size(52, 16);
			this->label27->TabIndex = 54;
			this->label27->Text = S"Motor 3";
			// 
			// domainUpDown2
			// 
			this->domainUpDown2->Location = System::Drawing::Point(150, 56);
			this->domainUpDown2->Name = S"domainUpDown2";
			this->domainUpDown2->Size = System::Drawing::Size(80, 22);
			this->domainUpDown2->TabIndex = 55;
			this->domainUpDown2->Text = S"Kd1";
			this->domainUpDown2->Click += new System::EventHandler(this, &Form1::domainUpDown2_Click);
			// 
			// domainUpDown3
			// 
			this->domainUpDown3->Location = System::Drawing::Point(236, 56);
			this->domainUpDown3->Name = S"domainUpDown3";
			this->domainUpDown3->Size = System::Drawing::Size(80, 22);
			this->domainUpDown3->TabIndex = 56;
			this->domainUpDown3->Text = S"Ki1";
			this->domainUpDown3->Click += new System::EventHandler(this, &Form1::domainUpDown3_Click);
			// 
			// domainUpDown4
			// 
			this->domainUpDown4->Location = System::Drawing::Point(64, 95);
			this->domainUpDown4->Name = S"domainUpDown4";
			this->domainUpDown4->Size = System::Drawing::Size(80, 22);
			this->domainUpDown4->TabIndex = 57;
			this->domainUpDown4->Text = S"Kp2";
			this->domainUpDown4->Click += new System::EventHandler(this, &Form1::domainUpDown4_Click);
			// 
			// domainUpDown5
			// 
			this->domainUpDown5->Location = System::Drawing::Point(150, 95);
			this->domainUpDown5->Name = S"domainUpDown5";
			this->domainUpDown5->Size = System::Drawing::Size(80, 22);
			this->domainUpDown5->TabIndex = 58;
			this->domainUpDown5->Text = S"Kd2";
			this->domainUpDown5->Click += new System::EventHandler(this, &Form1::domainUpDown5_Click);
			// 
			// domainUpDown6
			// 
			this->domainUpDown6->Location = System::Drawing::Point(236, 95);
			this->domainUpDown6->Name = S"domainUpDown6";
			this->domainUpDown6->Size = System::Drawing::Size(80, 22);
			this->domainUpDown6->TabIndex = 59;
			this->domainUpDown6->Text = S"Ki2";
			this->domainUpDown6->Click += new System::EventHandler(this, &Form1::domainUpDown6_Click);
			// 
			// domainUpDown7
			// 
			this->domainUpDown7->Location = System::Drawing::Point(64, 136);
			this->domainUpDown7->Name = S"domainUpDown7";
			this->domainUpDown7->Size = System::Drawing::Size(80, 22);
			this->domainUpDown7->TabIndex = 60;
			this->domainUpDown7->Text = S"Kp3";
			this->domainUpDown7->Click += new System::EventHandler(this, &Form1::domainUpDown7_Click);
			// 
			// domainUpDown8
			// 
			this->domainUpDown8->Location = System::Drawing::Point(150, 135);
			this->domainUpDown8->Name = S"domainUpDown8";
			this->domainUpDown8->Size = System::Drawing::Size(80, 22);
			this->domainUpDown8->TabIndex = 61;
			this->domainUpDown8->Text = S"Kd3";
			this->domainUpDown8->Click += new System::EventHandler(this, &Form1::domainUpDown8_Click);
			// 
			// domainUpDown9
			// 
			this->domainUpDown9->Location = System::Drawing::Point(236, 135);
			this->domainUpDown9->Name = S"domainUpDown9";
			this->domainUpDown9->Size = System::Drawing::Size(80, 22);
			this->domainUpDown9->TabIndex = 62;
			this->domainUpDown9->Text = S"Ki3";
			this->domainUpDown9->Click += new System::EventHandler(this, &Form1::domainUpDown9_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(323, 49);
			this->button5->Name = S"button5";
			this->button5->Size = System::Drawing::Size(90, 46);
			this->button5->TabIndex = 63;
			this->button5->Text = S"Aplicar ganancias";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += new System::EventHandler(this, &Form1::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(306, 8);
			this->button6->Name = S"button6";
			this->button6->Size = System::Drawing::Size(122, 29);
			this->button6->TabIndex = 64;
			this->button6->Text = S"Limpiar ventana";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += new System::EventHandler(this, &Form1::button6_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->checkBox1);
			this->groupBox1->Controls->Add(this->radioButton2);
			this->groupBox1->Controls->Add(this->radioButton1);
			this->groupBox1->Controls->Add(this->label29);
			this->groupBox1->Controls->Add(this->label30);
			this->groupBox1->Controls->Add(this->numericUpDown4);
			this->groupBox1->Controls->Add(this->numericUpDown5);
			this->groupBox1->Location = System::Drawing::Point(233, 15);
			this->groupBox1->Name = S"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(294, 131);
			this->groupBox1->TabIndex = 67;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = S"TIPO DE DESPLAZAMIENTO";
			this->groupBox1->Enter += new System::EventHandler(this, &Form1::groupBox1_Enter);
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Location = System::Drawing::Point(68, 98);
			this->checkBox1->Name = S"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(209, 20);
			this->checkBox1->TabIndex = 98;
			this->checkBox1->Text = S"Modificar par�metros de control";
			this->checkBox1->UseVisualStyleBackColor = true;
			this->checkBox1->CheckedChanged += new System::EventHandler(this, &Form1::checkBox1_CheckedChanged_1);
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(9, 63);
			this->radioButton2->Name = S"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(88, 20);
			this->radioButton2->TabIndex = 1;
			this->radioButton2->Text = S"Cartesiano";
			this->radioButton2->UseVisualStyleBackColor = true;
			this->radioButton2->CheckedChanged += new System::EventHandler(this, &Form1::radioButton2_CheckedChanged);
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Checked = true;
			this->radioButton1->Location = System::Drawing::Point(10, 42);
			this->radioButton1->Name = S"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(74, 20);
			this->radioButton1->TabIndex = 0;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = S"Articular";
			this->radioButton1->UseVisualStyleBackColor = true;
			this->radioButton1->CheckedChanged += new System::EventHandler(this, &Form1::radioButton1_CheckedChanged_1);
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(121, 34);
			this->label29->Name = S"label29";
			this->label29->Size = System::Drawing::Size(65, 16);
			this->label29->TabIndex = 80;
			this->label29->Text = S"Velocidad";
			this->label29->Click += new System::EventHandler(this, &Form1::label29_Click);
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(112, 67);
			this->label30->Name = S"label30";
			this->label30->Size = System::Drawing::Size(76, 16);
			this->label30->TabIndex = 81;
			this->label30->Text = S"Aceleraci�n";
			// 
			// numericUpDown4
			// 
			this->numericUpDown4->Location = System::Drawing::Point(192, 28);
			System::Int32 __mcTemp__1[] = new System::Int32[4];
			__mcTemp__1[0] = 160000;
			__mcTemp__1[1] = 0;
			__mcTemp__1[2] = 0;
			__mcTemp__1[3] = 0;
			this->numericUpDown4->Maximum = System::Decimal(__mcTemp__1);
			this->numericUpDown4->Name = S"numericUpDown4";
			this->numericUpDown4->Size = System::Drawing::Size(85, 22);
			this->numericUpDown4->TabIndex = 78;
			this->numericUpDown4->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown4_ValueChanged);
			// 
			// numericUpDown5
			// 
			this->numericUpDown5->Location = System::Drawing::Point(192, 61);
			this->numericUpDown5->Name = S"numericUpDown5";
			this->numericUpDown5->Size = System::Drawing::Size(85, 22);
			this->numericUpDown5->TabIndex = 79;
			this->numericUpDown5->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown5_ValueChanged);
			// 
			// numericUpDown1
			// 
			this->numericUpDown1->Location = System::Drawing::Point(10, 60);
			System::Int32 __mcTemp__2[] = new System::Int32[4];
			__mcTemp__2[0] = 1300;
			__mcTemp__2[1] = 0;
			__mcTemp__2[2] = 0;
			__mcTemp__2[3] = 0;
			this->numericUpDown1->Maximum = System::Decimal(__mcTemp__2);
			this->numericUpDown1->Name = S"numericUpDown1";
			this->numericUpDown1->Size = System::Drawing::Size(71, 22);
			this->numericUpDown1->TabIndex = 74;
			this->numericUpDown1->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown1_ValueChanged_1);
			// 
			// numericUpDown2
			// 
			this->numericUpDown2->Location = System::Drawing::Point(110, 60);
			System::Int32 __mcTemp__3[] = new System::Int32[4];
			__mcTemp__3[0] = 1300;
			__mcTemp__3[1] = 0;
			__mcTemp__3[2] = 0;
			__mcTemp__3[3] = 0;
			this->numericUpDown2->Maximum = System::Decimal(__mcTemp__3);
			this->numericUpDown2->Name = S"numericUpDown2";
			this->numericUpDown2->Size = System::Drawing::Size(71, 22);
			this->numericUpDown2->TabIndex = 75;
			this->numericUpDown2->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown2_ValueChanged_1);
			// 
			// numericUpDown3
			// 
			this->numericUpDown3->Location = System::Drawing::Point(210, 60);
			System::Int32 __mcTemp__4[] = new System::Int32[4];
			__mcTemp__4[0] = 1300;
			__mcTemp__4[1] = 0;
			__mcTemp__4[2] = 0;
			__mcTemp__4[3] = 0;
			this->numericUpDown3->Maximum = System::Decimal(__mcTemp__4);
			this->numericUpDown3->Name = S"numericUpDown3";
			this->numericUpDown3->Size = System::Drawing::Size(71, 22);
			this->numericUpDown3->TabIndex = 76;
			this->numericUpDown3->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown3_ValueChanged_1);
			// 
			// button10
			// 
			this->button10->Location = System::Drawing::Point(107, 128);
			this->button10->Name = S"button10";
			this->button10->Size = System::Drawing::Size(81, 34);
			this->button10->TabIndex = 77;
			this->button10->Text = S"Ejecutar";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += new System::EventHandler(this, &Form1::button10_Click_1);
			// 
			// timer2
			// 
			this->timer2->Tick += new System::EventHandler(this, &Form1::timer2_Tick);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->FileOk += new System::ComponentModel::CancelEventHandler(this, &Form1::saveFileDialog1_FileOk);
			// 
			// numericUpDown6
			// 
			System::Int32 __mcTemp__5[] = new System::Int32[4];
			__mcTemp__5[0] = 1000;
			__mcTemp__5[1] = 0;
			__mcTemp__5[2] = 0;
			__mcTemp__5[3] = 0;
			this->numericUpDown6->Increment = System::Decimal(__mcTemp__5);
			this->numericUpDown6->Location = System::Drawing::Point(175, 128);
			System::Int32 __mcTemp__6[] = new System::Int32[4];
			__mcTemp__6[0] = 5000;
			__mcTemp__6[1] = 0;
			__mcTemp__6[2] = 0;
			__mcTemp__6[3] = 0;
			this->numericUpDown6->Maximum = System::Decimal(__mcTemp__6);
			this->numericUpDown6->Name = S"numericUpDown6";
			this->numericUpDown6->Size = System::Drawing::Size(96, 22);
			this->numericUpDown6->TabIndex = 87;
			this->numericUpDown6->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown6_ValueChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(172, 108);
			this->label1->Name = S"label1";
			this->label1->Size = System::Drawing::Size(83, 16);
			this->label1->TabIndex = 88;
			this->label1->Text = S"Esperar [ms]";
			this->label1->Click += new System::EventHandler(this, &Form1::label1_Click);
			// 
			// button13
			// 
			this->button13->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->button13->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->button13->Image = (__try_cast<System::Drawing::Image*  >(resources->GetObject(S"button13.Image")));
			this->button13->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button13->Location = System::Drawing::Point(12, 125);
			this->button13->Name = S"button13";
			this->button13->Size = System::Drawing::Size(180, 45);
			this->button13->TabIndex = 89;
			this->button13->Text = S"CALIBRACION";
			this->button13->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += new System::EventHandler(this, &Form1::button13_Click);
			// 
			// numericUpDown7
			// 
			this->numericUpDown7->Location = System::Drawing::Point(26, 50);
			System::Int32 __mcTemp__7[] = new System::Int32[4];
			__mcTemp__7[0] = 150;
			__mcTemp__7[1] = 0;
			__mcTemp__7[2] = 0;
			__mcTemp__7[3] = 0;
			this->numericUpDown7->Maximum = System::Decimal(__mcTemp__7);
			System::Int32 __mcTemp__8[] = new System::Int32[4];
			__mcTemp__8[0] = 150;
			__mcTemp__8[1] = 0;
			__mcTemp__8[2] = 0;
			__mcTemp__8[3] = -2147483648;
			this->numericUpDown7->Minimum = System::Decimal(__mcTemp__8);
			this->numericUpDown7->Name = S"numericUpDown7";
			this->numericUpDown7->Size = System::Drawing::Size(71, 22);
			this->numericUpDown7->TabIndex = 30;
			this->numericUpDown7->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown7_ValueChanged);
			// 
			// numericUpDown8
			// 
			this->numericUpDown8->Location = System::Drawing::Point(126, 50);
			System::Int32 __mcTemp__9[] = new System::Int32[4];
			__mcTemp__9[0] = 150;
			__mcTemp__9[1] = 0;
			__mcTemp__9[2] = 0;
			__mcTemp__9[3] = 0;
			this->numericUpDown8->Maximum = System::Decimal(__mcTemp__9);
			System::Int32 __mcTemp__10[] = new System::Int32[4];
			__mcTemp__10[0] = 150;
			__mcTemp__10[1] = 0;
			__mcTemp__10[2] = 0;
			__mcTemp__10[3] = -2147483648;
			this->numericUpDown8->Minimum = System::Decimal(__mcTemp__10);
			this->numericUpDown8->Name = S"numericUpDown8";
			this->numericUpDown8->Size = System::Drawing::Size(71, 22);
			this->numericUpDown8->TabIndex = 90;
			this->numericUpDown8->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown8_ValueChanged);
			// 
			// numericUpDown9
			// 
			this->numericUpDown9->Location = System::Drawing::Point(226, 50);
			System::Int32 __mcTemp__11[] = new System::Int32[4];
			__mcTemp__11[0] = 520;
			__mcTemp__11[1] = 0;
			__mcTemp__11[2] = 0;
			__mcTemp__11[3] = 0;
			this->numericUpDown9->Maximum = System::Decimal(__mcTemp__11);
			System::Int32 __mcTemp__12[] = new System::Int32[4];
			__mcTemp__12[0] = 190;
			__mcTemp__12[1] = 0;
			__mcTemp__12[2] = 0;
			__mcTemp__12[3] = 0;
			this->numericUpDown9->Minimum = System::Decimal(__mcTemp__12);
			this->numericUpDown9->Name = S"numericUpDown9";
			this->numericUpDown9->Size = System::Drawing::Size(71, 22);
			this->numericUpDown9->TabIndex = 91;
			System::Int32 __mcTemp__13[] = new System::Int32[4];
			__mcTemp__13[0] = 190;
			__mcTemp__13[1] = 0;
			__mcTemp__13[2] = 0;
			__mcTemp__13[3] = 0;
			this->numericUpDown9->Value = System::Decimal(__mcTemp__13);
			this->numericUpDown9->ValueChanged += new System::EventHandler(this, &Form1::numericUpDown9_ValueChanged);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::SystemColors::Control;
			this->button2->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->button2->Image = (__try_cast<System::Drawing::Image*  >(resources->GetObject(S"button2.Image")));
			this->button2->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button2->Location = System::Drawing::Point(12, 180);
			this->button2->Name = S"button2";
			this->button2->Size = System::Drawing::Size(180, 45);
			this->button2->TabIndex = 92;
			this->button2->Text = S"  DETENER";
			this->button2->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += new System::EventHandler(this, &Form1::button2_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button10);
			this->groupBox2->Controls->Add(this->numericUpDown3);
			this->groupBox2->Controls->Add(this->numericUpDown2);
			this->groupBox2->Controls->Add(this->numericUpDown1);
			this->groupBox2->Controls->Add(this->label4);
			this->groupBox2->Controls->Add(this->label3);
			this->groupBox2->Controls->Add(this->label2);
			this->groupBox2->Controls->Add(this->textBox3);
			this->groupBox2->Controls->Add(this->textBox2);
			this->groupBox2->Controls->Add(this->textBox1);
			this->groupBox2->Location = System::Drawing::Point(233, 161);
			this->groupBox2->Name = S"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(294, 173);
			this->groupBox2->TabIndex = 93;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = S"DESPLAZAMIENTO ARTICULAR [cuentas]";
			this->groupBox2->Enter += new System::EventHandler(this, &Form1::groupBox2_Enter);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->button9);
			this->groupBox3->Controls->Add(this->button8);
			this->groupBox3->Controls->Add(this->button7);
			this->groupBox3->Controls->Add(this->button3);
			this->groupBox3->Controls->Add(this->groupBox4);
			this->groupBox3->Controls->Add(this->numericUpDown9);
			this->groupBox3->Controls->Add(this->numericUpDown8);
			this->groupBox3->Controls->Add(this->numericUpDown7);
			this->groupBox3->Controls->Add(this->label15);
			this->groupBox3->Controls->Add(this->numericUpDown6);
			this->groupBox3->Controls->Add(this->label1);
			this->groupBox3->Controls->Add(this->label14);
			this->groupBox3->Controls->Add(this->label13);
			this->groupBox3->Controls->Add(this->label12);
			this->groupBox3->Controls->Add(this->label11);
			this->groupBox3->Controls->Add(this->label10);
			this->groupBox3->Controls->Add(this->button1);
			this->groupBox3->Location = System::Drawing::Point(566, 15);
			this->groupBox3->Name = S"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(322, 319);
			this->groupBox3->TabIndex = 94;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = S"DESPLAZAMIENTO CARTESIANO [mm]";
			this->groupBox3->Enter += new System::EventHandler(this, &Form1::groupBox3_Enter);
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(175, 276);
			this->button9->Name = S"button9";
			this->button9->Size = System::Drawing::Size(129, 31);
			this->button9->TabIndex = 98;
			this->button9->Text = S"Borrar sequencia";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += new System::EventHandler(this, &Form1::button9_Click_1);
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(175, 186);
			this->button8->Name = S"button8";
			this->button8->Size = System::Drawing::Size(129, 31);
			this->button8->TabIndex = 97;
			this->button8->Text = S"Ver sequencia";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += new System::EventHandler(this, &Form1::button8_Click_1);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(175, 231);
			this->button7->Name = S"button7";
			this->button7->Size = System::Drawing::Size(129, 31);
			this->button7->TabIndex = 96;
			this->button7->Text = S"Ejecutar sequencia";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += new System::EventHandler(this, &Form1::button7_Click_1);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(31, 253);
			this->button3->Name = S"button3";
			this->button3->Size = System::Drawing::Size(81, 31);
			this->button3->TabIndex = 95;
			this->button3->Text = S"Guardar";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += new System::EventHandler(this, &Form1::button3_Click_1);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->radioButton4);
			this->groupBox4->Controls->Add(this->radioButton3);
			this->groupBox4->Location = System::Drawing::Point(19, 98);
			this->groupBox4->Name = S"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(127, 75);
			this->groupBox4->TabIndex = 94;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = S"Efector final";
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Location = System::Drawing::Point(9, 47);
			this->radioButton4->Name = S"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(75, 20);
			this->radioButton4->TabIndex = 93;
			this->radioButton4->Text = S"Activado";
			this->radioButton4->UseVisualStyleBackColor = true;
			this->radioButton4->CheckedChanged += new System::EventHandler(this, &Form1::radioButton4_CheckedChanged);
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Checked = true;
			this->radioButton3->Location = System::Drawing::Point(9, 21);
			this->radioButton3->Name = S"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(96, 20);
			this->radioButton3->TabIndex = 92;
			this->radioButton3->TabStop = true;
			this->radioButton3->Text = S"Desactivado";
			this->radioButton3->UseVisualStyleBackColor = true;
			this->radioButton3->CheckedChanged += new System::EventHandler(this, &Form1::radioButton3_CheckedChanged);
			// 
			// groupBox5
			// 
			this->groupBox5->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->groupBox5->Controls->Add(this->button5);
			this->groupBox5->Controls->Add(this->domainUpDown9);
			this->groupBox5->Controls->Add(this->domainUpDown8);
			this->groupBox5->Controls->Add(this->domainUpDown7);
			this->groupBox5->Controls->Add(this->domainUpDown6);
			this->groupBox5->Controls->Add(this->domainUpDown5);
			this->groupBox5->Controls->Add(this->domainUpDown4);
			this->groupBox5->Controls->Add(this->domainUpDown3);
			this->groupBox5->Controls->Add(this->domainUpDown2);
			this->groupBox5->Controls->Add(this->label27);
			this->groupBox5->Controls->Add(this->label26);
			this->groupBox5->Controls->Add(this->label25);
			this->groupBox5->Controls->Add(this->label21);
			this->groupBox5->Controls->Add(this->label20);
			this->groupBox5->Controls->Add(this->label19);
			this->groupBox5->Controls->Add(this->domainUpDown1);
			this->groupBox5->Controls->Add(this->SetGainsButton);
			this->groupBox5->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->groupBox5->Location = System::Drawing::Point(469, 373);
			this->groupBox5->Name = S"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(419, 196);
			this->groupBox5->TabIndex = 95;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = S"PARAMETROS DE CONTROL";
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->button6);
			this->groupBox6->Controls->Add(this->textBox5);
			this->groupBox6->Location = System::Drawing::Point(10, 365);
			this->groupBox6->Name = S"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(442, 204);
			this->groupBox6->TabIndex = 96;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = S"MENSAJES";
			// 
			// button11
			// 
			this->button11->BackColor = System::Drawing::SystemColors::Control;
			this->button11->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->button11->Image = (__try_cast<System::Drawing::Image*  >(resources->GetObject(S"button11.Image")));
			this->button11->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button11->Location = System::Drawing::Point(12, 235);
			this->button11->Name = S"button11";
			this->button11->Size = System::Drawing::Size(180, 45);
			this->button11->TabIndex = 97;
			this->button11->Text = S"HABILITAR\r\nAMPLIFICADORES";
			this->button11->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button11->UseVisualStyleBackColor = false;
			this->button11->Click += new System::EventHandler(this, &Form1::button11_Click_1);
			// 
			// listBox1
			// 
			this->listBox1->FormattingEnabled = true;
			this->listBox1->ItemHeight = 16;
			this->listBox1->Location = System::Drawing::Point(912, 24);
			this->listBox1->Name = S"listBox1";
			this->listBox1->Size = System::Drawing::Size(54, 436);
			this->listBox1->TabIndex = 98;
			// 
			// listBox2
			// 
			this->listBox2->FormattingEnabled = true;
			this->listBox2->ItemHeight = 16;
			this->listBox2->Location = System::Drawing::Point(972, 24);
			this->listBox2->Name = S"listBox2";
			this->listBox2->Size = System::Drawing::Size(54, 436);
			this->listBox2->TabIndex = 99;
			// 
			// listBox3
			// 
			this->listBox3->FormattingEnabled = true;
			this->listBox3->ItemHeight = 16;
			this->listBox3->Location = System::Drawing::Point(1032, 24);
			this->listBox3->Name = S"listBox3";
			this->listBox3->Size = System::Drawing::Size(54, 436);
			this->listBox3->TabIndex = 100;
			// 
			// listBox4
			// 
			this->listBox4->FormattingEnabled = true;
			this->listBox4->ItemHeight = 16;
			this->listBox4->Location = System::Drawing::Point(1092, 24);
			this->listBox4->Name = S"listBox4";
			this->listBox4->Size = System::Drawing::Size(54, 436);
			this->listBox4->TabIndex = 101;
			// 
			// listBox5
			// 
			this->listBox5->FormattingEnabled = true;
			this->listBox5->ItemHeight = 16;
			this->listBox5->Location = System::Drawing::Point(1152, 24);
			this->listBox5->Name = S"listBox5";
			this->listBox5->Size = System::Drawing::Size(76, 436);
			this->listBox5->TabIndex = 102;
			// 
			// button12
			// 
			this->button12->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->button12->Location = System::Drawing::Point(912, 474);
			this->button12->Name = S"button12";
			this->button12->Size = System::Drawing::Size(81, 34);
			this->button12->TabIndex = 78;
			this->button12->Text = S"Importar";
			this->button12->UseVisualStyleBackColor = true;
			// 
			// textBox4
			// 
			this->textBox4->Font = (new System::Drawing::Font(S"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->textBox4->Location = System::Drawing::Point(1003, 488);
			this->textBox4->Name = S"textBox4";
			this->textBox4->Size = System::Drawing::Size(210, 26);
			this->textBox4->TabIndex = 103;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->label6->Location = System::Drawing::Point(1067, 527);
			this->label6->Name = S"label6";
			this->label6->Size = System::Drawing::Size(67, 16);
			this->label6->TabIndex = 104;
			this->label6->Text = S"ARCHIVO";
			this->label6->Click += new System::EventHandler(this, &Form1::label6_Click);
			// 
			// button14
			// 
			this->button14->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->button14->Location = System::Drawing::Point(912, 514);
			this->button14->Name = S"button14";
			this->button14->Size = System::Drawing::Size(81, 34);
			this->button14->TabIndex = 105;
			this->button14->Text = S"Cargar";
			this->button14->UseVisualStyleBackColor = true;
			// 
			// button15
			// 
			this->button15->Location = System::Drawing::Point(521, 341);
			this->button15->Name = S"button15";
			this->button15->Size = System::Drawing::Size(76, 24);
			this->button15->TabIndex = 106;
			this->button15->Text = S"button15";
			this->button15->UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this->AutoScaleBaseSize = System::Drawing::Size(6, 15);
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->ClientSize = System::Drawing::Size(1240, 589);
			this->Controls->Add(this->button15);
			this->Controls->Add(this->button14);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->button12);
			this->Controls->Add(this->listBox5);
			this->Controls->Add(this->listBox4);
			this->Controls->Add(this->listBox3);
			this->Controls->Add(this->listBox2);
			this->Controls->Add(this->listBox1);
			this->Controls->Add(this->button11);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button13);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->ExitButton);
			this->Controls->Add(this->InitButton);
			this->Font = (new System::Drawing::Font(S"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				(System::Byte)0));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (__try_cast<System::Drawing::Icon*  >(resources->GetObject(S"$this.Icon")));
			this->Name = S"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = S"ROBWIN  Version 4.0           PARALLIX LKF-2040: INTERFACE GRAFICA";
			this->Closing += new System::ComponentModel::CancelEventHandler(this, &Form1::Form1_Closing);
			this->Load += new System::EventHandler(this, &Form1::Form1_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown4))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown5))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown1))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown2))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown3))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown6))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown7))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown8))->EndInit();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->numericUpDown9))->EndInit();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}	
	private: System::Void InitButton_Click(System::Object *  sender, System::EventArgs *  e)
			 {

				 //				 char datastr[80];
				 numdesp=0;
				 //textBox5->Text=" SEARCHING USB ON COM3 ... ! \r\n";
				 nummod = NmcInit("COM4:", 19200);  //Controllers on COM4, use 19200 baud
				 //Returns the number of modules found
				 if (nummod==0)  
					{
						//SimpleMsgBox(" USB IS NOT CONNECTED TO COM4 !");
						textBox5->Text=" Error: USB IS NOT CONNECTED TO COM4 ! \r\n";
						Close();
					}
				 else
				 {
					if ((NmcGetModType(1) == SERVOMODTYPE) &(nummod==4))  //Determine the type for module 1
										 {
						 //InitButton->Text=" USB CONECTADO ";
						 textBox5->Text=" USB STARTED SUCCESSFULLY IO 16.... \r\n";
						 inicioOK = 1;
						 // Inicializar PIC-IO
						addr=1;
						IoBitDirOut(4, 6); //addr=1, set I/O bit 7 to be an output, use number 6 for bit #7
						Sleep(2000);
						IoClrOutBit(4, 6); //addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
						//IoSetOutBit(4, 6); //addr=1, Set output bit 7 to logic 1, use number 6 for bit #7
						
						 kp01=900;
						 kd01=600;
						 ki01=20;
						 kp02=900;
						 kd02=600;
						 ki02=20;
						 kp03=900;
						 kd03=600;
						 ki03=20;
						 setGains( 1,kp01,kd01,ki01);//setGains( int Motor,Kp,Kd,Ki)
						 setGains( 2,kp02,kd02,ki02);
						 setGains( 3,kp03,kd03,ki03);
						 motEnableDis(1,1);
						 motEnableDis(2,1);
						 motEnableDis(3,1);

						 //InitButton->Text=String::Concat(nummod.ToString()," Servos Encontrados");
						 textBox5->Text=System::String::Concat(textBox5->Text," 3 SERVOMOTORS FOUND ON COM4.\r\n");
						 //InitButton->BackColor=System::Drawing::Color::Green;
						 //InitButton->Enabled=false;
//						 EnableServoButton->Enabled = true;

						 //checkBox1->Enabled = true;
						 //vScrollBar1->Enabled=true;
						 //vScrollBar2->Enabled=true;
						 //vScrollBar3->Enabled=true;
						 //EnableServoButton->Focus();
//						 MoveButton->Enabled = true;
//						 MoveButton->Focus();
						 //button3->Enabled=true;
						 button4->Enabled=true;
						 button13->Enabled=false;
						 button2->Enabled=true;
						 //button4->BackColor=System::Drawing::Color::Green;
						 //SetGainsButton->Enabled=true;
//						 ReadPosButton->Enabled=true;
						 //vScrollBar4->Enabled=true;
						 //vScrollBar5->Enabled=true;
						 //vScrollBar6->Enabled=true;
						 //vScrollBar7->Enabled=true;
						 //checkBox1->Checked=true;
						 timer1->Enabled=true;
						 InitButton->Enabled = false;
						 //groupBox1->Enabled=true;
						 //EnDisBars(0);


					 }
					 else
					 {
						 //InitButton->BackColor=System::Drawing::Color::Red;
						 //InitButton->Text="PIC-SERVO1 Not found";
						 Close();
					 }
				 } 
			 }

	private: System::Void SetGainsButton_Click(System::Object *  sender, System::EventArgs *  e)
			 {
				 domainUpDown1->SelectedIndex=kp01;
				 domainUpDown2->SelectedIndex=kd01;
				 domainUpDown3->SelectedIndex=ki01;
				 domainUpDown4->SelectedIndex=kp02;
				 domainUpDown5->SelectedIndex=kd02;
				 domainUpDown6->SelectedIndex=ki02;
				 domainUpDown7->SelectedIndex=kp03;
				 domainUpDown8->SelectedIndex=kd03;
				 domainUpDown9->SelectedIndex=ki03;
				 setGains( 1,kp01,kd01,ki01);//setGains( int Motor,Kp,Kd,Ki)
				 setGains( 2,kp02,kd02,ki02);
				 setGains( 3,kp03,kd03,ki03);
				 SetGainsButton->Enabled=false;
                 textBox5->Text=System::String::Concat(textBox5->Text," FACTORY GAINS RECOVERED. \r\n");
				 Cajadetexto();
				 //EnableServoButton->Enabled = true;
				 //EnableServoButton->Focus();
			 }

	private: System::Void EnableServoButton_Click(System::Object *  sender, System::EventArgs *  e)
			 {

				 ServoStopMotor(1, AMP_ENABLE | MOTOR_OFF);  //enable amp
				 ServoStopMotor(1, AMP_ENABLE | STOP_ABRUPT);  //stop at current pos.
				 ServoResetPos(1);                             //reset the posiiton counter to 0
				 ServoStopMotor(2, AMP_ENABLE | MOTOR_OFF);  //enable amp
				 ServoStopMotor(2, AMP_ENABLE | STOP_ABRUPT);  //stop at current pos.
				 ServoResetPos(2);                             //reset the posiiton counter to 0
				 ServoStopMotor(3, AMP_ENABLE | MOTOR_OFF);  //enable amp
				 ServoStopMotor(3, AMP_ENABLE | STOP_ABRUPT);  //stop at current pos.
				 ServoResetPos(3);                             //reset the posiiton counter to 0

//				 MoveButton->Enabled = true;
//				 MoveButton->Focus();

			 }

	private: System::Void MoveButton_Click(System::Object *  sender, System::EventArgs *  e)
			 {
				 Pos3To(-4000, 800000, 400,-4000, 800000, 400,-4000, 800000, 400);
				 //do
				 //{
				 // NmcNoOp(1);	//poll controller to get current status data
				 // statbyte1 = NmcGetStat(1);
				 // NmcNoOp(2);	//poll controller to get current status data
				 // statbyte2 = NmcGetStat(2);
				 // NmcNoOp(3);	//poll controller to get current status data
				 // statbyte3 = NmcGetStat(3);
				 //}
				 //while ( !(statbyte1 &statbyte2 &statbyte3 & MOVE_DONE) );  //wait for MOVE_DONE bit to go HIGH

//				 ReadPosButton->Focus();
			 }

	private: System::Void ReadPosButton_Click(System::Object *  sender, System::EventArgs *  e)
			 {
				 long position;
				 //				 char msgstr[40];

				 //Cause PIC-SERVO controller to send back position data with each command
				 //Position data will also be sent back with this command
				 NmcDefineStatus(1,    		//addr = 1
					 SEND_POS    //Send back position data only
					 );
				 //Retrieve the position data from the local data structure
				 position = ServoGetPos(1);
				 //sprintf(msgstr,"Current Position: %ld",position);
				 //SimpleMsgBox(msgstr);
				 textBox1->Text=Convert::ToString(position);
				 NmcDefineStatus(2,    		//addr = 1
					 SEND_POS    //Send back position data only
					 );
				 position = ServoGetPos(2);
				 textBox2->Text=Convert::ToString(position);

			 }

	private: System::Void ExitButton_Click(System::Object *  sender, System::EventArgs *  e)
			 {
				 if (inicioOK == 1) {
				 textBox5->Text=" ";
				 Xp=0;
				 Yp=0;
				 if(Zp==double(-numericUpDown9->Maximum)){
					 Close();				 }
				 else{
					 Zp=double(-numericUpDown9->Maximum);
					 func_2();
				 }
				 //backgroundWorker2->RunWorkerAsync( );
				 Close();
				 } else {
				 Close();}
			 }

	private: System::Void Form1_Closing(System::Object *  sender, System::ComponentModel::CancelEventArgs *  e)
			 {
				 if (nummod == 0) return;
				 NmcShutdown();

			 }

	private: System::Void trackBar1_Scroll(System::Object*  sender, System::EventArgs*  e) {

			 }
	private: System::Void trackBar1_Scroll_1(System::Object*  sender, System::EventArgs*  e) {
			 }
	private: System::Void trackBar1_MouseLeave(System::Object*  sender, System::EventArgs*  e) {
			 }



	private: System::Void timer1_Tick(System::Object*  sender, System::EventArgs*  e) {
				 long position;
				 //				 char msgstr[40];
				 //Cause PIC-SERVO controller to send back position data with each command
				 //Position data will also be sent back with this command
				 NmcDefineStatus(1,    		//addr = 1
					 SEND_POS    //Send back position data only
					 );
				 //Retrieve the position data from the local data structure
				 position = ServoGetPos(1);
				 textBox1->Text=Convert::ToString(position);
				 NmcDefineStatus(2,    		//addr = 1
					 SEND_POS    //Send back position data only
					 );
				 //Retrieve the position data from the local data structure
				 position = ServoGetPos(2);
				 textBox2->Text=Convert::ToString(position);
				 NmcDefineStatus(3,    		//addr = 1
					 SEND_POS    //Send back position data only
					 );
				 //Retrieve the position data from the local data structure
				 position = ServoGetPos(3);
				 textBox3->Text=Convert::ToString(position);

				 //label7->Text=M1posDes.ToString();
				 //label8->Text=M2posDes.ToString();
				 //label9->Text=M3posDes.ToString();


			 }
	private: System::Void trackBar2_Scroll(System::Object*  sender, System::EventArgs*  e) {
			 }
	private: System::Void trackBar2_MouseLeave(System::Object*  sender, System::EventArgs*  e) {
			 }
	private: System::Void vScrollBar1_Scroll(System::Object*  sender, System::Windows::Forms::ScrollEventArgs*  e) {
				 //M1posDes=-vScrollBar1->Value+vScrollBar1->Maximum/2;
				 //label7->Text=M1posDes.ToString();
				 //label16->Text=Pulses2Rad(M1posDes).ToString();
				 numericUpDown1->Value=M1posDes;
				 //position= vScrollBar1->Value;
			 }
	private: System::Void vScrollBar2_Scroll(System::Object*  sender, System::Windows::Forms::ScrollEventArgs*  e) {
				 //M2posDes=-vScrollBar2->Value+vScrollBar2->Maximum/2;
				 //label8->Text=M2posDes.ToString();
				 //label17->Text=Pulses2Rad(M2posDes).ToString();
				 numericUpDown2->Value=M2posDes;
			 }
	private: System::Void vScrollBar3_Scroll(System::Object*  sender, System::Windows::Forms::ScrollEventArgs*  e) {
				 //M3posDes=-vScrollBar3->Value+vScrollBar3->Maximum/2;
				 //label9->Text=M3posDes.ToString();
				 //label18->Text=Pulses2Rad(M3posDes).ToString();
				 numericUpDown3->Value=M3posDes;
			 }



			 void setGains( int Motor,int Kp,int Kd,int Ki)
			 {
				 ServoSetGain(Motor,     //axis = 1
					 Kp,	//Kp = 100
					 Kd,	//Kd = 1000
					 Ki,		//Ki = 0
					 //200,		//IL = 200
					 0,			// IL=0
					 255,	//OL = 255
					 0,		//CL = 0
					 4000,	//EL = 4000
					 1,		//SR = 1
					 0		//DC = 0
					 );
			 };

			 int Rad2Pulses( double Rd)
			 {
				 int pulses=0;
				 //pulses=int(Rd*100*20800/(2*pi));     //4000*5.2 = 20800
				//pulses=int(Rd*100*(400*6)/(2*pi));     // Motores Bernio
				//pulses=int(Rd*100*(400*6)/(2*pi));  // Motores Bernio, Sydney-CICATA
				pulses=int(Rd*100*(400*14)/(2*pi));  // Motores Maxon, Sydney-CICATA
				 return((pulses/100));
			 };

			 double Pulses2Rad( int pulses)
			 {
				 double Rd=0;
				 //Rd=2*pi*double(pulses)/(20800);    //1 vuelta a la salida = (5.2 *4000) =20800
				//Rd=2*pi*double(pulses)/(400*6);		// Motores Bernio
				Rd=2*pi*double(pulses)/(400*14);		// Motores Maxon Sydney-CICATA

				 return(Rd);
			 };

			 void PosTo(int Motor,int Pos, int Vel, int Acel){
				 byte statbyte;
				 ServoLoadTraj(Motor, 		//addr = 0
					 LOAD_POS | LOAD_VEL | LOAD_ACC | ENABLE_SERVO | START_NOW, 
					 Pos,		//pos = 2000
					 Vel, 	//vel = 100,000
					 Acel, 		//acc = 100
					 0			//pwm = 0
					 );
				 do
				 {
					 NmcNoOp(1);	//poll controller to get current status data
					 statbyte = NmcGetStat(1);
				 }
				 while ( !(statbyte & MOVE_DONE) );  //wait for MOVE_DONE bit to go HIGH

			 };
			 void Pos3To(int P1, int V1, int A1,int P2, int V2, int A2,int P3, int V3, int A3){
				 byte statbyte1;
				 byte statbyte2;
				 byte statbyte3;
				 ServoLoadTraj(1, 		//addr = 0
					 LOAD_POS | LOAD_VEL | LOAD_ACC | ENABLE_SERVO | START_NOW, 
					 P1,		//pos = 2000
					 V1, 	//vel = 100,000
					 A1, 		//acc = 100
					 0			//pwm = 0
					 );
				 ServoLoadTraj(2, 		//addr = 0
					 LOAD_POS | LOAD_VEL | LOAD_ACC | ENABLE_SERVO | START_NOW, 
					 P2,		//pos = 2000
					 V2, 	//vel = 100,000
					 A2, 		//acc = 100
					 0			//pwm = 0
					 );
				 ServoLoadTraj(3, 		//addr = 0
					 LOAD_POS | LOAD_VEL | LOAD_ACC | ENABLE_SERVO | START_NOW, 
					 P3,		//pos = 2000
					 V3, 	//vel = 100,000
					 A3, 		//acc = 100
					 0			//pwm = 0
					 );

				 do
				 {
					 NmcNoOp(1);	//poll controller to get current status data
					 statbyte1 = NmcGetStat(1);
					 NmcNoOp(2);	//poll controller to get current status data
					 statbyte2 = NmcGetStat(2);
					 NmcNoOp(3);	//poll controller to get current status data
					 statbyte3 = NmcGetStat(3);
				 }
				 while ( !(statbyte1 &statbyte2 &statbyte3 & MOVE_DONE) );  //wait for MOVE_DONE bit to go HIGH

			 };

			 void motEnableDis(int Motor,int EnDis)
			 {
				 if(EnDis==1){
					 ServoStopMotor(Motor, AMP_ENABLE | MOTOR_OFF);  //enable amp
					 ServoStopMotor(Motor, AMP_ENABLE | STOP_ABRUPT);  //stop at current pos.
					 ServoResetPos(Motor);   
				 }else{
					 ServoStopMotor(Motor, AMP_DISABLE | MOTOR_OFF);  //enable amp
					 ServoStopMotor(Motor, AMP_DISABLE | STOP_ABRUPT);  //stop at current pos.
					 ServoResetPos(Motor);   

				 }
				 //reset the posiiton counter to 0
			 };
			 void EnDisBars(int EnDis)
			 {
				 if(EnDis==1){
					 //						 checkBox1->Enabled = true;
					 //vScrollBar1->Enabled=true;
					 //vScrollBar2->Enabled=true;
					 //vScrollBar3->Enabled=true;
//					 MoveButton->Enabled = true;
					 //button3->Enabled=true;
					 //SetGainsButton->Enabled=true;
//					 ReadPosButton->Enabled=true;
					 //vScrollBar4->Enabled=true;
					 //vScrollBar5->Enabled=true;
					 //vScrollBar6->Enabled=true;
					 //vScrollBar7->Enabled=true;
					 //domainUpDown1->Enabled=true;
					 //domainUpDown1->Enabled=true;
					 //domainUpDown2->Enabled=true;
					 //domainUpDown3->Enabled=true;
					 //domainUpDown4->Enabled=true;
					 //domainUpDown5->Enabled=true;
					 //domainUpDown6->Enabled=true;
					 //domainUpDown7->Enabled=true;
					 //domainUpDown8->Enabled=true;
					 //domainUpDown9->Enabled=true;
					 //button5->Enabled=true;
					 //button9->Enabled=true;
					 //button11->Enabled=true;
					 //button13->Enabled=true;
					 //button1->Enabled=true;
					 //label5->Enabled=true;
					 //textBox4->Enabled=true;

				 }else{
					 //						 checkBox1->Enabled = false;
					 //vScrollBar1->Enabled=false;
					 //vScrollBar2->Enabled=false;
					 //vScrollBar3->Enabled=false;
//					 MoveButton->Enabled = false;
					 //button3->Enabled=false;
					 //SetGainsButton->Enabled=false;
					 //vScrollBar4->Enabled=false;
//					 ReadPosButton->Enabled=false;
					 //vScrollBar5->Enabled=false;
					 //vScrollBar6->Enabled=false;
					 //vScrollBar7->Enabled=false;
					 //domainUpDown1->Enabled=false;
					 //domainUpDown1->Enabled=false;
					 //domainUpDown2->Enabled=false;
					 //domainUpDown3->Enabled=false;
					 //domainUpDown4->Enabled=false;
					 //domainUpDown5->Enabled=false;
					 //domainUpDown6->Enabled=false;
					 //domainUpDown7->Enabled=false;
					 //domainUpDown8->Enabled=false;
					 //domainUpDown9->Enabled=false;
					 //button5->Enabled=false;
					 //button9->Enabled=false;
					 //button11->Enabled=false;
					 //button13->Enabled=false;
					 //button1->Enabled=false;
					 //label5->Enabled=false;
					 //textBox4->Enabled=false;

				 }
				 //reset the posiiton counter to 0
			 };


	private: System::Void Form1_Load(System::Object*  sender, System::EventArgs*  e) {
				 int i=0;
				 int j=0;
				 int k=0;
				 double Rot[16];
				 

				 numericUpDown4->Value=50000; // velocidad
				 numericUpDown5->Value=50; // aceleracion
				 //sleepingtime=10000;
				 sleepingtime=0;
				 numericUpDown6->Value=sleepingtime;
				 VelGen=int(numericUpDown4->Value);
				 AcelGen=int(numericUpDown5->Value);
				 //checkBox1->Enabled = false;
				 //vScrollBar1->Enabled=false;
				 //vScrollBar2->Enabled=false;
				 //vScrollBar3->Enabled=false;
				 //vScrollBar4->Enabled=false;

				 //vScrollBar5->Enabled=false;
				 //vScrollBar6->Enabled=false;
				 //vScrollBar7->Enabled=false;

				 //SetGainsButton->Enabled = false;
//				 EnableServoButton->Enabled = false;
//				 MoveButton->Enabled = false;
//				 ReadPosButton->Enabled = false;
				 //SetGainsButton->Enabled = false;
//				 ReadPosButton->Enabled = false;
				 //button3->Enabled=false;
				 button4->Enabled=false;
				 button2->Enabled=false;
				 button13->Enabled=false;
//				 button7->Enabled=false;
//				 button8->Enabled=false;
				 //SetGainsButton->Enabled=false;
//				 ReadPosButton->Enabled=false;
				 button11->Enabled=false;

				 label2->BackColor = System::Drawing::Color::Red;
				 label3->BackColor = System::Drawing::Color::Red;
				 label4->BackColor = System::Drawing::Color::Red;
				 label10->BackColor = System::Drawing::Color::Red;
				 label11->BackColor = System::Drawing::Color::Red;
				 label12->BackColor = System::Drawing::Color::Red;
				 //label6->Text=vScrollBar4->Value.ToString();
				 //checkBox1->Checked=false;
				 //vScrollBar5->Value=vScrollBar5->Maximum/2;
				 //vScrollBar6->Value=vScrollBar6->Maximum/2;
				 //vScrollBar7->Value=vScrollBar7->Maximum/2;

				 //vScrollBar1->Maximum=int(numericUpDown1->Maximum)-int(numericUpDown1->Minimum);
				 //vScrollBar2->Maximum=int(numericUpDown2->Maximum)-int(numericUpDown2->Minimum);
				 //vScrollBar3->Maximum=int(numericUpDown3->Maximum)-int(numericUpDown3->Minimum);
				 //vScrollBar4->Maximum=int(numericUpDown3->Maximum)-int(numericUpDown3->Minimum);

				 //vScrollBar1->Value=vScrollBar1->Maximum/2;
				 //vScrollBar2->Value=vScrollBar2->Maximum/2;
				 //vScrollBar3->Value=vScrollBar3->Maximum/2;
				 //vScrollBar4->Value=vScrollBar3->Maximum/2;
				 //Zp=-vScrollBar7->Value;//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 //Yp=vScrollBar6->Maximum/2-vScrollBar6->Value;
				 //Xp=vScrollBar5->Maximum/2-vScrollBar5->Value;
				 Zp=double(-numericUpDown9->Value);
				 Yp=double(numericUpDown7->Value);
				 Xp=double(numericUpDown8->Value);
				 label13->Text=Xp.ToString();
				 label14->Text=Yp.ToString();
				 label15->Text=Zp.ToString();
				 //label16->Text=Pulses2Rad(Xp).ToString();
				 //label17->Text=Pulses2Rad(Yp).ToString();
				 //label18->Text=Pulses2Rad(Zp).ToString();

				 Phi[0] = 0;
				 Phi[1] = 2*pi/3;
				 Phi[2] = 4*pi/3;
				 RotMat(3,Phi[0],Rot);				
				 //textBox5->Text=System::String::Concat((Phi[0]).ToString(),"   ",(Phi[1]).ToString());//,(Phi[1]).ToString());
				 //textBox5->Text=System::String::Concat(textBox5->Text,"   ",(Phi[2]).ToString());
				 //textBox5->Text=System::String::Concat(textBox5->Text,"\r\n");
				 textBox5->Text=" USB SHOULD BE ALREADY CONNECTED TO PC.";
				 j=3;
				 /*for(i=0;i<16;i++){
					 textBox5->Text=System::String::Concat(textBox5->Text,"   ",Rot[i].ToString());
					 if(j==i){
						 j=j+4;
						 textBox5->Text=System::String::Concat(textBox5->Text,"\r\n");
					 }

				 }*/


				 for(i=0;i<3000;i++){
					 domainUpDown1->Items->Add(i.ToString());
					 domainUpDown2->Items->Add(i.ToString());
					 domainUpDown3->Items->Add(i.ToString());
				 }
				 for(i=0;i<3000;i++){
					 domainUpDown4->Items->Add(i.ToString());
					 domainUpDown5->Items->Add(i.ToString());
					 domainUpDown6->Items->Add(i.ToString());
				 }
				 for(i=0;i<3000;i++){
					 domainUpDown7->Items->Add(i.ToString());
					 domainUpDown8->Items->Add(i.ToString());
					 domainUpDown9->Items->Add(i.ToString());
				 }
				 kp1=900;
				 kd1=600;
				 ki1=20;
				 kp2=900;
				 kd2=600;
				 ki2=20;
				 kp3=900;
				 kd3=600;
				 ki3=20;

				 domainUpDown1->SelectedIndex=kp1;
				 domainUpDown2->SelectedIndex=kd1;
				 domainUpDown3->SelectedIndex=ki1;
				 domainUpDown4->SelectedIndex=kp2;
				 domainUpDown5->SelectedIndex=kd2;
				 domainUpDown6->SelectedIndex=ki2;
				 domainUpDown7->SelectedIndex=kp3;
				 domainUpDown8->SelectedIndex=kd3;
				 domainUpDown9->SelectedIndex=ki3;
				 //dimensiones del robot
				 a[0]=200;
				 a[1]=200;
				 a[2]=200;
				 a[3]=0;

				 b[0]=400;
				 b[1]=400;
				 b[2]=400;
				 b[3]=0;

				 h[0]=50;
				 h[1]=50;
				 h[2]=50;
				 h[3]=0;

				 H[0]=150;
				 H[1]=150;
				 H[2]=150;
				 H[3]=0;
				 groupBox1->Enabled=false;
				 groupBox2->Enabled=false;
				 groupBox3->Enabled=false;
				 groupBox5->Enabled=false;
				 EnDisBars(0);
				 ////posici�n inicial
				 //Pf[0]=0;
				 //Pf[1]=0;
				 //Pf[2]=-50;
				 //Pf[3]=1;

			 }

	private: System::Void button1_Click(System::Object*  sender, System::EventArgs*  e) {

				 //textBox5->Text=" DESPLAZAMIENTO EN CURSO ...\r\n";
				 groupBox1->Enabled=false;
				 groupBox3->Enabled=false;
				 if(checkBox1->Checked==true){
					checkBox1->Checked=false;
					groupBox5->Enabled=false;
					}
				 //backgroundWorker2->RunWorkerAsync( );
				 func_2();
				 //vScrollBar7->Select();
				 groupBox1->Enabled=true;
				 groupBox3->Enabled=true;
				 textBox5->Text=System::String::Concat(textBox5->Text," END EFFECTOR DISPLACEMENT: ");
				 //textBox5->Text=System::String::Concat(textBox5->Text," POSICION ACTUAL: ");
				 textBox5->Text=System::String::Concat(textBox5->Text,"  X",Xp.ToString());
				 textBox5->Text=System::String::Concat(textBox5->Text,",  Y",Yp.ToString());
			     textBox5->Text=System::String::Concat(textBox5->Text,",  Z",Zp.ToString(),"\r\n");
				 Cajadetexto();
			 }


/*			 long func_2(int n, BackgroundWorker * trabajador, DoWorkEventArgs * e){
				 int ret=1;
				 e->Result=false;
				 EnDisBars(0);
				 this->label2->BackColor = System::Drawing::Color::Green;
				 this->label3->BackColor = System::Drawing::Color::Green;
				 this->label4->BackColor = System::Drawing::Color::Green;
				 //vScrollBar1->Enabled=true;
				 //vScrollBar2->Enabled=true;
				 //vScrollBar3->Enabled=true;
				 int i=0;
				 int j=0;
				 do{
					 if(trabajador->CancellationPending==true){//esta parte la activamos con el boton cancelar
						 e->Cancel=true;
						 break;
					 }else{
						 label5->Text=System::String::Concat("BackRunning",i.ToString());
						 i++;
						 if(inversekinematics(Xp,Yp,Zp)==0){
							 ret=0;
						 }
					 }
				 }while(0);//i<1200);
				 //Calculamos velocidades y aceleraciones para producir el movimiento
				 if(ret==1){
					 double vmax=VelGen;
					 double amax=AcelGen;
					 double vel[4];
					 double acel[4];
					 double t=0;
					 TH1pulses[3]=0;
					 TH1pulses_ant[3]=0;
					 double taux=00000000000000;
					 for(i=0;i<3;i++){
						 if(Math::Abs((TH1pulses[i]-TH1pulses_ant[i]))>0)
						 {
							 t=Math::Abs((TH1pulses[i]-TH1pulses_ant[i])/vmax);
							 if(t>taux){
								 taux=t;
							 }
						 }
					 }
					 t=taux;
					 //label28->Text=t.ToString();
					 for(i=0;i<3;i++){
						 if(t==0){
							 vel[i]=0;
						 }else
						 {
							 vel[i]=Math::Abs((TH1pulses[i]-TH1pulses_ant[i])/t);
						 }
						 textBox5->Text=System::String::Concat(textBox5->Text,"\r\n","     vel[ ",i.ToString());
						 textBox5->Text=System::String::Concat(textBox5->Text,"] = ",vel[i].ToString());
						 textBox5->Text=System::String::Concat(textBox5->Text,"  ",t.ToString(),"\r\n");
						 acel[i]=amax;
					 }
					 //actualizamos los �ngulos de los motores
					 TH1pulses_ant[0]=TH1pulses[0];
					 TH1pulses_ant[1]=TH1pulses[1];
					 TH1pulses_ant[2]=TH1pulses[2];

					 Pos3To(int(TH1pulses[0]),int(vel[0]),int(acel[0]),int(TH1pulses[1]),int(vel[1]),int(acel[1]),int(TH1pulses[2]),int(vel[2]),int(acel[2]));
					 //do
					 //{
					 // nmcnoop(1);	//poll controller to get current status data
					 // statbyte1 = nmcgetstat(1);
					 // nmcnoop(2);	//poll controller to get current status data
					 // statbyte2 = nmcgetstat(2);
					 // nmcnoop(3);	//poll controller to get current status data
					 // statbyte3 = nmcgetstat(3);
					 //}
					 //while ( !(statbyte1 &statbyte2 &statbyte3 & move_done) );  //wait for move_done bit to go high

				 }


				 label5->Text="";
				 this->label2->BackColor = System::Drawing::Color::Red;
				 this->label3->BackColor = System::Drawing::Color::Red;
				 this->label4->BackColor = System::Drawing::Color::Red;
				 EnDisServCart(2);
				 //e->Resul=true;
				 return(ret);
			 };*/

			 long func_2(void){
				 int ret=1;
				 EnDisBars(0);
				 this->label2->BackColor = System::Drawing::Color::Green;
				 this->label3->BackColor = System::Drawing::Color::Green;
				 this->label4->BackColor = System::Drawing::Color::Green;
				 //vScrollBar1->Enabled=true;
				 //vScrollBar2->Enabled=true;
				 //vScrollBar3->Enabled=true;
				 int i=0;
				 int j=0;
				 do{
					 label5->Text=System::String::Concat("BackRunning",i.ToString());
					 i++;
					 if(inversekinematics(Xp,Yp,Zp)==0){
						 ret=0;
					 }
				 }while(0);//i<1200);
				 //Calculamos velocidades y aceleraciones para producir el movimiento
				 if(ret==1){
					 double vmax=VelGen;
					 double amax=AcelGen;
					 double vel[4];
					 double acel[4];
					 double t=0;
					 TH1pulses[3]=0;
					 TH1pulses_ant[3]=0;
					 double taux=00000000000000;
					 for(i=0;i<3;i++){
						 if(Math::Abs((TH1pulses[i]-TH1pulses_ant[i]))>0)
						 {
							 t=Math::Abs((TH1pulses[i]-TH1pulses_ant[i])/vmax);
							 if(t>taux){
								 taux=t;
							 }
						 }
					 }
					 t=taux;
					 //label28->Text=t.ToString();
					 for(i=0;i<3;i++){
						 if(t==0){
							 vel[i]=0;
						 }else
						 {
							 vel[i]=Math::Abs((TH1pulses[i]-TH1pulses_ant[i])/t);
						 }
						 //textBox5->Text=System::String::Concat(textBox5->Text,"\r\n","     vel[ ",i.ToString());
						 //textBox5->Text=System::String::Concat(textBox5->Text,"] = ",vel[i].ToString());
						 //textBox5->Text=System::String::Concat(textBox5->Text,"  ",t.ToString(),"\r\n");
						 acel[i]=amax;
					 }
					 //actualizamos los �ngulos de los motores
					 TH1pulses_ant[0]=TH1pulses[0];
					 TH1pulses_ant[1]=TH1pulses[1];
					 TH1pulses_ant[2]=TH1pulses[2];

					 Pos3To(int(TH1pulses[0]),int(vel[0]),int(acel[0]),int(TH1pulses[1]),int(vel[1]),int(acel[1]),int(TH1pulses[2]),int(vel[2]),int(acel[2]));
					 //do
					 //{
					 // nmcnoop(1);	//poll controller to get current status data
					 // statbyte1 = nmcgetstat(1);
					 // nmcnoop(2);	//poll controller to get current status data
					 // statbyte2 = nmcgetstat(2);
					 // nmcnoop(3);	//poll controller to get current status data
					 // statbyte3 = nmcgetstat(3);
					 //}
					 //while ( !(statbyte1 &statbyte2 &statbyte3 & move_done) );  //wait for move_done bit to go high

				 }


				 label5->Text="";
				 this->label2->BackColor = System::Drawing::Color::Red;
				 this->label3->BackColor = System::Drawing::Color::Red;
				 this->label4->BackColor = System::Drawing::Color::Red;
				 EnDisServCart(2);
				 //e->Resul=true;
				 return(ret);
			 };


			 int inversekinematics(double Xpf,double Ypf,double Zpf){
				 int i=0;
				 int j=0;
				 int k=0;
				 double Rot[16];
				 int ret=1;
				 Pf[0]=Ypf;
				 Pf[1]=Xpf;
				 Pf[2]=Zpf;
				 Pf[3]=1;

				 double aux[4];
				 aux[4]=1;
				 double aux1[4];
				 aux1[4]=1;
				 double aux2[4];
				 aux1[4]=1;
				 //double aux3[4];
				 aux1[4]=1;
				 for(i=0;i<16;i++){
					 Ti[i]=0;
				 }
				 Phi[0] = 0;
				 Phi[1] = 2*pi/3;
				 Phi[2] = 4*pi/3;
				 Phi[3] = 0;
				 //textBox5->Text=System::String::Concat(textBox5->Text,"\r\n\r\n");
				 for(i=0;i<3;i++){
					 Ti[i*4]=h[i];

					 RotMat(3,Phi[i],Rot);
					 for(j=0;j<4;j++){
						 aux[j]=Pf[j];
					 }
					 aux[3]=1;
					 aux1[0]=h[i];
					 aux1[1]=0;
					 aux1[2]=0;
					 aux1[3]=0;
					 multMat(4,4,Rot,4,1,aux1,4,1,aux2);
					 addMat(4,1,aux,4,1,aux2,4,1,aux1);

					 k=3;
					 for(j=0;j<4;j++){
						 Cf[i*4+j]=aux1[j];
					 }
					 //**************************  mostrar resultados  *********************************
					 //textBox5->Text=System::String::Concat(textBox5->Text,"Cf  =   [");
					 for(j=0;j<4;j++){
						 //textBox5->Text=System::String::Concat(textBox5->Text," ",Cf[i*4+j].ToString(),"]");
						 if(k==j){
							 k=k+4;
							 //textBox5->Text=System::String::Concat(textBox5->Text,"]","\r\n");//,"    ",(i).ToString(),"\r\n\r\n");
						 }
					 }
					 //**************************  calculamos angulos de motores  *********************************

					 //textBox5->Text=System::String::Concat(textBox5->Text,(i/3).ToString(),"\r\n");
					 R[i]=cO(Phi[i])*Cf[i*4]+sI(Phi[i])*Cf[i*4+1];
					 N[i]=b[i]*b[i]-Cf[i*4]*Cf[i*4]-Cf[i*4+1]*Cf[i*4+1]-Cf[i*4+2]*Cf[i*4+2]-H[i]*H[i]-a[i]*a[i]+2*H[i]*R[i];
					 M[i]=2*a[i]*Cf[i*4+2];
					 Q[i]=2*(H[i]*a[i]-a[i]*R[i]);
					 if((4*(M[i]*M[i]+Q[i]*Q[i]-N[i]*N[i]))>0.1)
					 {
						 TH1[i]=2*atA(   (2*M[i]+sqrT( 4*(M[i]*M[i]+Q[i]*Q[i]-N[i]*N[i])) )/(2*(Q[i]+N[i]))   );
						 TH1pulses[i]=Rad2Pulses(TH1[i]);
					 }else{
						 //textBox5->Text=System::String::Concat(textBox5->Text,"\r\n","      Error Th[ ",i.ToString());
						 TH1[i]=TH1ant[i];
						 if(i==2){
							 ret=0;
						 }
					 }
					 //textBox5->Text=System::String::Concat(textBox5->Text," R= ",R[i].ToString(),"\r\n");
					 //textBox5->Text=System::String::Concat(textBox5->Text," N= ",N[i].ToString(),"\r\n");
					 //textBox5->Text=System::String::Concat(textBox5->Text," M= ",M[i].ToString(),"\r\n");
					 //textBox5->Text=System::String::Concat(textBox5->Text," Q= ",Q[i].ToString(),"\r\n");
					 //textBox5->Text=System::String::Concat(textBox5->Text,"      Th= ",TH1[i].ToString());
					 //textBox5->Text=System::String::Concat(textBox5->Text,"      Thpulses= ",TH1pulses[i].ToString(),"\r\n\r\n");
					 //R=(cos(ph(i))*Cxyz(1)+sin(ph(i))*Cxyz(2))
					 //M=2*a*Cxyz(3,1)
					 //N=b^2-Cxyz'*Cxyz-H^2-a^2+2*H*R
					 //Q=2*H*a-2*a*R
					 //
				 }
				 // Actualizamos �ngulos anteriores en radianes
				 TH1ant[0]=TH1[0];
				 TH1ant[1]=TH1[1];
				 TH1ant[2]=TH1[2];

				 int v=50000;int a=50;

				 //Pos3To(TH1deg[0],v,a,TH1deg[1],v,a,TH1deg[2],v,a);

				 return(ret);
			 }


	private: System::Void button3_Click(System::Object*  sender, System::EventArgs*  e) {
				 int ik1=0;int p=0;int v=50000;int a=50;
				 Pos3To(4400,VelGen,AcelGen,3700,VelGen,AcelGen,4400,VelGen,AcelGen);
				 Pos3To(6500,VelGen,AcelGen,1700,VelGen,AcelGen,2500,VelGen,AcelGen);
				 Pos3To(4400,VelGen,AcelGen,3700,VelGen,AcelGen,4400,VelGen,AcelGen);
				 Pos3To(3100,VelGen,AcelGen,2300,VelGen,AcelGen,5950,VelGen,AcelGen);
				 Pos3To(4400,VelGen,AcelGen,3700,VelGen,AcelGen,4400,VelGen,AcelGen);
				 Pos3To(1900,VelGen,AcelGen,5200,VelGen,AcelGen,4400,VelGen,AcelGen);
				 Pos3To(4400,VelGen,AcelGen,3700,VelGen,AcelGen,4400,VelGen,AcelGen);
				 Pos3To(4800,VelGen,AcelGen,5100,VelGen,AcelGen,1600,VelGen,AcelGen);
				 Pos3To(4400,VelGen,AcelGen,3700,VelGen,AcelGen,4400,VelGen,AcelGen);
				 //incrementamos la velocidad
				 v=50000; a=100;
				 p=0;
				 Pos3To(p,VelGen,AcelGen,p,VelGen,AcelGen,p,VelGen,AcelGen);
				 p=4000;
				 Pos3To(p,VelGen,AcelGen,p,VelGen,AcelGen,p,VelGen,AcelGen);
				 p=0;
				 Pos3To(p,VelGen,AcelGen,p,VelGen,AcelGen,p,VelGen,AcelGen);
				 p=4000;
				 Pos3To(p,VelGen,AcelGen,p,VelGen,AcelGen,p,VelGen,AcelGen);
				 p=0;
				 Pos3To(p,VelGen,AcelGen,p,VelGen,AcelGen,p,VelGen,AcelGen);

			 }
	private: System::Void checkBox1_CheckedChanged(System::Object*  sender, System::EventArgs*  e) {
//				 if(checkBox1->Checked==true){
//					 checkBox1->Text="Disable Servo";
//					 motEnableDis(1,1);
//					 motEnableDis(2,1);
//					 motEnableDis(3,1);
//					 label2->BackColor = System::Drawing::Color::Green;
//					 label3->BackColor = System::Drawing::Color::Green;
//					 label4->BackColor = System::Drawing::Color::Green;
//					 EnDisBars(1);
//				 }else{
//					 checkBox1->Text="Enable Servo";
//					 motEnableDis(1,0);
//					 motEnableDis(2,0);
//					 motEnableDis(3,0);
//					 label2->BackColor = System::Drawing::Color::Red;
//					 label3->BackColor = System::Drawing::Color::Red;
//					 label4->BackColor = System::Drawing::Color::Red;
//					 EnDisBars(0);
//				 }


			 }
	private: System::Void vScrollBar4_Scroll(System::Object*  sender, System::Windows::Forms::ScrollEventArgs*  e) {

				 //M1posDes=-vScrollBar4->Value+vScrollBar4->Maximum/2;
				 //M2posDes=M1posDes;
				 //M3posDes=M2posDes;
				 //vScrollBar1->Value=vScrollBar4->Value;
				 //vScrollBar2->Value=vScrollBar4->Value;
				 //vScrollBar3->Value=vScrollBar4->Value;
				 //label6->Text=vScrollBar4->Value.ToString();
				 //numericUpDown1->Value=M1posDes;
				 //numericUpDown2->Value=M2posDes;
				 //numericUpDown3->Value=M3posDes;


			 }
	private: System::Void vScrollBar5_Scroll(System::Object*  sender, System::Windows::Forms::ScrollEventArgs*  e) {
				 //Xp=double(vScrollBar5->Maximum/2-vScrollBar5->Value);
				 label13->Text=Xp.ToString();
				 button1->Enabled=false;
			 }
	private: System::Void vScrollBar6_Scroll(System::Object*  sender, System::Windows::Forms::ScrollEventArgs*  e) {
				 //Yp=double(vScrollBar6->Maximum/2-vScrollBar6->Value);
				 label14->Text=Yp.ToString();
				 button1->Enabled=false;
			 }
	private: System::Void vScrollBar7_Scroll(System::Object*  sender, System::Windows::Forms::ScrollEventArgs*  e) {
				 //Zp=double(-vScrollBar7->Value);//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 label15->Text=Zp.ToString();
				 button1->Enabled=false;
			 }

	private: System::Void vScrollBar5_MouseEnter(System::Object*  sender, System::EventArgs*  e) {
				 //Zp=double(-vScrollBar7->Value);//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 //Yp=double(vScrollBar6->Maximum/2-vScrollBar6->Value);
				 //Xp=double(vScrollBar5->Maximum/2-vScrollBar5->Value);
				 button1->Enabled=true;
			 }
	private: System::Void vScrollBar6_MouseEnter(System::Object*  sender, System::EventArgs*  e) {
				 //Zp=double(-vScrollBar7->Value);//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 //Yp=double(vScrollBar6->Maximum/2-vScrollBar6->Value);
				 //Xp=double(vScrollBar5->Maximum/2-vScrollBar5->Value);
				 button1->Enabled=true;
			 }
	private: System::Void vScrollBar7_MouseEnter(System::Object*  sender, System::EventArgs*  e) {
				 //Zp=double(-vScrollBar7->Value);//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 //Yp=double(vScrollBar6->Maximum/2-vScrollBar6->Value);
				 //Xp=double(vScrollBar5->Maximum/2-vScrollBar5->Value);
				 button1->Enabled=true;
			 }
	private: System::Void button4_Click(System::Object*  sender, System::EventArgs*  e) {

				 //button4->BackColor=System::Drawing::Color::LightGray;
				 //int i=0,j=0;
				groupBox1->Enabled=false;
				groupBox2->Enabled=false;
				groupBox3->Enabled=false;
				 //EnDisBars(0);

				 ServoStopMotor(1, AMP_ENABLE | MOTOR_OFF);  //enable amp
				 ServoStopMotor(1, AMP_ENABLE | STOP_ABRUPT);  //stop at current pos.
				 ServoResetPos(1);                             //reset the posiiton counter to 0
				 ServoStopMotor(2, AMP_ENABLE | MOTOR_OFF);  //enable amp
				 ServoStopMotor(2, AMP_ENABLE | STOP_ABRUPT);  //stop at current pos.
				 ServoResetPos(2);                             //reset the posiiton counter to 0
				 ServoStopMotor(3, AMP_ENABLE | MOTOR_OFF);  //enable amp
				 ServoStopMotor(3, AMP_ENABLE | STOP_ABRUPT);  //stop at current pos.
				 ServoResetPos(3);                             //reset the posiiton counter to 0

				 //button4->Text=System::String::Concat("Home ","1, 2, 3");
				 textBox5->Text=System::String::Concat(textBox5->Text," SEARCHING ENCODER INDEX CHANNEL ...\r\n");
				 //do{i++;do{j++;}while(j<30000);j=0;}while(i<30000);
				 //backgroundWorker1->RunWorkerAsync( );

				 func_1();
				 Pos3To(0, VelGen,AcelGen,0, VelGen,AcelGen,0, VelGen,AcelGen);

				 //ServoSetPos(1,ServoGetPos(1)+4237+102-antihome1);                             //reset the posiiton counter to 0
				 //ServoSetPos(2,ServoGetPos(2)+3889+117-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)+5423-antihome3);                             //reset the posiiton counter to 0	 
				 // NUEVOS VALORES PARA LA HORIZONTAL, CALCULADOS A PARTIR DE LOS CALCULADOS POR ATM
				 // EL CALCULO FUE REALIZADO POR ECC EL 27/03/2008
				 // h1=2735, h2=843, h3=3480
				 /* PRUEBA ServoSetPos(1,ServoGetPos(1)+4237+102-antihome1-2735);                             //reset the posiiton counter to 0
				 ServoSetPos(2,ServoGetPos(2)+3889+117-antihome2-843);                             //reset the posiiton counter to 0
				 ServoSetPos(3,ServoGetPos(3)+5423-antihome3-3480);   */
				 // NUEVOS VALORES PARA LA HORIZONTAL, CALCULADOS A PARTIR DE LOS CALCULADOS POR ECC
				 // EL CALCULO FUE REALIZADO POR ECC EL 25/11/2008
				 // h1=?, h2=?, h3=?
				 //ServoSetPos(1,ServoGetPos(1)+4237+102-antihome1-391);                             //reset the posiiton counter to 0
				 //ServoSetPos(2,ServoGetPos(2)+3889+117-antihome2-2311);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)+5423-antihome3-3573);   
				 // EL CALCULO FUE REALIZADO POR ECC EL 04/03/2009
				 // h1=1300, h2=-890, h3=-420
				 //ServoSetPos(1,ServoGetPos(1)+4237+102-antihome1-391-1300);                             //reset the posiiton counter to 0
				 //ServoSetPos(2,ServoGetPos(2)+3889+117-antihome2-2311+890);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)+5423-antihome3-3573+420);   
				 //ServoSetPos(1,ServoGetPos(1)*0+285-antihome1); // motores Bernio//reset the posiiton counter to 0
				 //ServoSetPos(2,ServoGetPos(2)*0+210-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+379-antihome3); 
				 // ROBOT 1
				 //ServoSetPos(1,ServoGetPos(1)*0+876-antihome1); // Motores Maxon Sydney
				 //ServoSetPos(2,ServoGetPos(2)*0+882-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+1062-antihome3); 

				 //Robot 2

				 //ServoSetPos(1,ServoGetPos(1)*0+946-antihome1); // Motores Maxon Sydney-CICATA 22 Abril 2010
				 //ServoSetPos(2,ServoGetPos(2)*0+1054-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+811-antihome3); 

				 //ServoSetPos(1,ServoGetPos(1)*0+843-antihome1); // Motores Maxon Sydney-CICATA 27 Mayo 2010
				 //ServoSetPos(2,ServoGetPos(2)*0+1148-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+1011-antihome3); 

				 
				 //ServoSetPos(1,ServoGetPos(1)*0+1002-antihome1); // Motores Maxon UASLP 2 Sept 2010
				 //ServoSetPos(2,ServoGetPos(2)*0+941-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+884-antihome3); 

				 //ServoSetPos(1,ServoGetPos(1)*0+882-antihome1); // Robot1, Motores Maxon CECYT1 27 Oct 2010
				 //ServoSetPos(2,ServoGetPos(2)*0+1005-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+1089-antihome3); 

				 //ServoSetPos(1,ServoGetPos(1)*0+972-antihome1); // Robot2, Motores Maxon CECYT1 27 Oct 2010
				 //ServoSetPos(2,ServoGetPos(2)*0+1052-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+1036-antihome3);

                 //ServoSetPos(1,ServoGetPos(1)*0+959-antihome1); // Robot, Motores Maxon IT Irapuato 12 Nov 2013
				 //ServoSetPos(2,ServoGetPos(2)*0+1137-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+948-antihome3); 

				 
                 //ServoSetPos(1,ServoGetPos(1)*0+1026-antihome1); // Robot, Motores Maxon IT ITS LIBRES 16 FEB 2015
				 //ServoSetPos(2,ServoGetPos(2)*0+1003-antihome2);     //                        //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+1120-antihome3); //

				 
                 //ServoSetPos(1,ServoGetPos(1)*0+809-antihome1); // Robot, Motores Maxon IT CANCUN 29 MAYO 2015
				 //ServoSetPos(2,ServoGetPos(2)*0+984-antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)*0+961-antihome3); 

				 ServoSetPos(1,ServoGetPos(1)*0+1040-antihome1); // Robot, Motores Maxon IT CIUDAD JUAREZ 31 MARZO 2016
				 ServoSetPos(2,ServoGetPos(2)*0+920-antihome2);                             //reset the posiiton counter to 0
				 ServoSetPos(3,ServoGetPos(3)*0+1040-antihome3); 

				 //EnDisBars(1);
				 groupBox1->Enabled=true;
				 //button4->BackColor=System::Drawing::Color::Red;
				 //button4->Text=System::String::Concat("Acab� y cudado al"," \r\n","oprimirlo de nuevo");
				 textBox5->Text=System::String::Concat(textBox5->Text," HOME POSITION DETECTED.\r\n");
				 groupBox2->Enabled=true;
				 button4->Enabled=false;
				 label2->BackColor = System::Drawing::Color::Chartreuse;
				 label3->BackColor = System::Drawing::Color::Chartreuse;
				 label4->BackColor = System::Drawing::Color::Chartreuse;
				 //button7->Enabled=true;
				 //button8->Enabled=false;
				 //button7->BackColor=System::Drawing::Color::Green;

			 }

			 void func_1(void){
				 //PosTo(3,500, 50000, 100); En comentario por ECC 25/Marzo/2008
				 // la instruccion anterior regresaba 500 cuentas al motor 3 para encontrar
				 // la marca del INDEX
				 int i_c=0;
				 //int ik1=0;int p=0;int v=400000;int a=500;
				 //Pos3To(-4000,v,a,-4000,v,a,-4000,v,a);
				 //do
				 //{
				 // NmcNoOp(1);	//poll controller to get current status data
				 // statbyte1 = NmcGetStat(1);
				 // NmcNoOp(2);	//poll controller to get current status data
				 // statbyte2 = NmcGetStat(2);
				 // NmcNoOp(3);	//poll controller to get current status data
				 // statbyte3 = NmcGetStat(3);
				 //}
				 //while ( !(statbyte1 &statbyte2 &statbyte3 & MOVE_DONE) );  //wait for MOVE_DONE bit to go HIGH
				 PICSERVOVCExample::byte addr;
				 //unsigned char addr; //module address
				 unsigned char mode; //motion options
				 int pos, vel, acc; //position, velocity & acceleration
				 unsigned char pwm; //raw PWM value - unused
				 unsigned char homing_mode; //homing command options
				 PICSERVOVCExample::byte still_homing; //flag for checking homing			 
				 homing_mode = ON_INDEX | HOME_STOP_ABRUPT;

				 mode = (LOAD_POS | LOAD_VEL | LOAD_ACC | ENABLE_SERVO | START_NOW);
				 pos = -1000; //move to position beyond the limit switch
				 vel = 40000; //100,000 ~= 3000 encoder counts per second
				 //acc = 100; //acceleration value
				 //acc=10; // modificada por ECC. para motores Bernio 26 Agosto 2009
				 acc=20; // modificada para motores MAXON 4 nov 2009
				 pwm = 0; //pwm value is not used

				 addr=1;
				 //button4->Text=System::String::Concat("Home ",addr.ToString());
				 ServoSetHoming(addr, homing_mode);
				 ServoLoadTraj(addr, mode, pos, vel, acc, pwm);
				 //Lastly, poll the HOME_IN_PROG bit to determine when the
				 //switch has been hit
				 do{
					 NmcNoOp(addr); //update the status byte
					 still_homing = (NmcGetStat(addr) & PICSERVOVCExample::byte(HOME_IN_PROG));
				 }while (still_homing);
				 // ServoResetPos(addr);                             //reset the posiiton counter to 0
				 
				 addr=2;
				 //button4->Text=System::String::Concat("Home ",addr.ToString());
				 ServoSetHoming(addr, homing_mode);
				 ServoLoadTraj(addr, mode, pos, vel, acc, pwm);
				 //Lastly, poll the HOME_IN_PROG bit to determine when the
				 //switch has been hit
				 do{
					 NmcNoOp(addr); //update the status byte
					 still_homing = (NmcGetStat(addr) & HOME_IN_PROG);
				 }while (still_homing);
				 //ServoResetPos(addr);                             //reset the posiiton counter to 0

				 addr=3;
				 //button4->Text=System::String::Concat("Home ",addr.ToString());
					 ServoSetHoming(addr, homing_mode);
					 ServoLoadTraj(addr, mode, pos, vel, acc, pwm);
				 //Lastly, poll the HOME_IN_PROG bit to determine when the
				 //switch has been hit
					 do{
						 NmcNoOp(addr); //update the status byte
						 still_homing = (NmcGetStat(addr) & HOME_IN_PROG);
					 }while (still_homing);
				 //ServoResetPos(addr);                             //reset the posiiton counter to 0

				 antihome1=ServoGetPos(1);                             //get the posiiton
				 antihome2=ServoGetPos(2);                             //get the posiiton
				 antihome3=ServoGetPos(3);                             //get the posiiton

			 };




	private: System::Void domainUpDown1_Click(System::Object*  sender, System::EventArgs*  e) {
				 kp1=System::Convert::ToInt32(domainUpDown1->Text);
				 domainUpDown1->SelectedIndex=kp1;
				 label19->Text=kp1.ToString();
			 }
	private: System::Void domainUpDown2_Click(System::Object*  sender, System::EventArgs*  e) {
				 kd1=System::Convert::ToInt32(domainUpDown2->Text);
				 domainUpDown2->SelectedIndex=kd1;
				 label20->Text=kd1.ToString();
			 }
	private: System::Void domainUpDown3_Click(System::Object*  sender, System::EventArgs*  e) {
				 ki1=System::Convert::ToInt32(domainUpDown3->Text);
				 domainUpDown3->SelectedIndex=ki1;
				 label21->Text=ki1.ToString();
			 }
	private: System::Void domainUpDown4_Click(System::Object*  sender, System::EventArgs*  e) {
				 kp2=System::Convert::ToInt32(domainUpDown4->Text);
				 domainUpDown4->SelectedIndex=kp2;
				 //label22->Text=kp2.ToString();
			 }
	private: System::Void domainUpDown5_Click(System::Object*  sender, System::EventArgs*  e) {
				 kd2=System::Convert::ToInt32(domainUpDown5->Text);
				 domainUpDown5->SelectedIndex=kd2;
				 //label23->Text=kd2.ToString();
			 }
	private: System::Void domainUpDown6_Click(System::Object*  sender, System::EventArgs*  e) {
				 ki2=System::Convert::ToInt32(domainUpDown6->Text);
				 domainUpDown6->SelectedIndex=ki2;
				 //label24->Text=ki2.ToString();
			 }
	private: System::Void domainUpDown7_Click(System::Object*  sender, System::EventArgs*  e) {
				 kp3=System::Convert::ToInt32(domainUpDown7->Text);
				 domainUpDown7->SelectedIndex=kp3;
				 label25->Text=kp3.ToString();
			 }
	private: System::Void domainUpDown8_Click(System::Object*  sender, System::EventArgs*  e) {
				 kd3=System::Convert::ToInt32(domainUpDown8->Text);
				 domainUpDown8->SelectedIndex=kd3;
				 label26->Text=kd3.ToString();
			 }
	private: System::Void domainUpDown9_Click(System::Object*  sender, System::EventArgs*  e) {
				 ki3=System::Convert::ToInt32(domainUpDown9->Text);
				 domainUpDown9->SelectedIndex=ki3;
				 label27->Text=ki3.ToString();
			 }
	private: System::Void button5_Click(System::Object*  sender, System::EventArgs*  e) {
				 setGains( 1,kp1,kd1,ki1);//setGains( int Motor,Kp,Ki,Kd)
				 setGains( 2,kp2,kd2,ki2);
				 setGains( 3,kp3,kd3,ki3);
				 SetGainsButton->Enabled=true;
				 textBox5->Text=System::String::Concat(textBox5->Text," GAINS WERE MODIFIED. \r\n");
			     Cajadetexto();
			 }
	private: System::Void button6_Click(System::Object*  sender, System::EventArgs*  e) {

				 textBox5->Text=" ";
			 }
	private: System::Void button7_Click(System::Object*  sender, System::EventArgs*  e) {
//				 button7->BackColor=System::Drawing::Color::LightGray;
				 Pos3To(0, VelGen,AcelGen,0, VelGen,AcelGen,0, VelGen,AcelGen);
				 ServoSetPos(1,ServoGetPos(1)+4237+102-antihome1);                             //reset the posiiton counter to 0
				 ServoSetPos(2,ServoGetPos(2)+3889+117-antihome2);                             //reset the posiiton counter to 0
				 ServoSetPos(3,ServoGetPos(3)+5423-antihome3);                             //reset the posiiton counter to 0
//				 button7->Enabled=false;
//				 button8->Enabled=true;
//				 button8->BackColor=System::Drawing::Color::Green;
			 }
	private: System::Void button8_Click(System::Object*  sender, System::EventArgs*  e) {
//				 button8->BackColor=System::Drawing::Color::LightGray;
				 //ServoSetPos(1,ServoGetPos(1)+4373+antihome1);                             //reset the posiiton counter to 0
				 //ServoSetPos(2,ServoGetPos(2)+4049+antihome2);                             //reset the posiiton counter to 0
				 //ServoSetPos(3,ServoGetPos(3)+5320+antihome3);                             //reset the posiiton counter to 0
				 EnDisBars(1);
				 //           inicializamos los �ngulos de cada motor
				 //*************************************************************
				 inversekinematics(Xp,Yp,Zp);
				 TH1pulses_ant[0]=TH1pulses[0];
				 TH1pulses_ant[1]=TH1pulses[1];
				 TH1pulses_ant[2]=TH1pulses[2];
				 //*************************************************************
//				 button7->Enabled=false;
//				 button8->Enabled=false;
				 button4->Enabled=true;
			 }
	private: System::Void radioButton1_CheckedChanged_1(System::Object*  sender, System::EventArgs*  e) {
				 EnDisServCart(1); // Espacio articular
				
				 groupBox3->Enabled=false;
				 groupBox2->Enabled=true;
				 button13->Enabled=false;
				 button2->Enabled=true;
				 label2->BackColor = System::Drawing::Color::Chartreuse;
				 label3->BackColor = System::Drawing::Color::Chartreuse;
				 label4->BackColor = System::Drawing::Color::Chartreuse;
				 label10->BackColor = System::Drawing::Color::Red;
				 label11->BackColor = System::Drawing::Color::Red;
				 label12->BackColor = System::Drawing::Color::Red;
			 }
	private: System::Void radioButton2_CheckedChanged(System::Object*  sender, System::EventArgs*  e) {
				 EnDisServCart(2); // Espacio cartesiano
				 //IoSetOutBit(addr, 7); //addr=1, Set output bit 7 to logic 1
				 groupBox3->Enabled=true;
				 groupBox2->Enabled=false;
				 button13->Enabled=true;
				 button2->Enabled=true;
				 label2->BackColor = System::Drawing::Color::Red;
				 label3->BackColor = System::Drawing::Color::Red;
				 label4->BackColor = System::Drawing::Color::Red;
				 label10->BackColor = System::Drawing::Color::Chartreuse;
				 label11->BackColor = System::Drawing::Color::Chartreuse;
				 label12->BackColor = System::Drawing::Color::Chartreuse;
			 }
	private: System::Void groupBox1_Enter(System::Object*  sender, System::EventArgs*  e) {
			 }
			 void EnDisServCart(int EnDis)
			 {
				 if(EnDis==1){
					 //						 checkBox1->Enabled = true;
					 //vScrollBar1->Enabled=true;
					 //vScrollBar2->Enabled=true;
					 //vScrollBar3->Enabled=true;
					 //vScrollBar4->Enabled=true;

//					 MoveButton->Enabled = true;
					 //button3->Enabled=true;
					 //SetGainsButton->Enabled=true;
//					 ReadPosButton->Enabled=true;
					 //vScrollBar5->Enabled=false;
					 //vScrollBar6->Enabled=false;
					 //vScrollBar7->Enabled=false;
					 //domainUpDown1->Enabled=true;
					 //domainUpDown1->Enabled=true;
					 //domainUpDown2->Enabled=true;
					 //domainUpDown3->Enabled=true;
					 //domainUpDown4->Enabled=true;
					 //domainUpDown5->Enabled=true;
					 //domainUpDown6->Enabled=true;
					 //domainUpDown7->Enabled=true;
					 //domainUpDown8->Enabled=true;
					 //domainUpDown9->Enabled=true;
					  //numericUpDown7->Enabled=false;
					 //button5->Enabled=true;
					 //button9->Enabled=false;
					 //button1->Enabled=false;
					 //button13->Enabled=false;

					 //label5->Enabled=false;
					 //textBox4->Enabled=false;
				 }else{
					 if(EnDis==2){
						 //						 checkBox1->Enabled = false;
						 //vScrollBar1->Enabled=false;
						 //vScrollBar2->Enabled=false;
						 //vScrollBar3->Enabled=false;
						 //vScrollBar4->Enabled=false;
//						 MoveButton->Enabled = true;
						 //button3->Enabled=true;
						 //SetGainsButton->Enabled=true;
//						 ReadPosButton->Enabled=true;
						 //vScrollBar5->Enabled=true;
						 //vScrollBar6->Enabled=true;
						 //vScrollBar7->Enabled=true;
						 //domainUpDown1->Enabled=true;
						 //domainUpDown1->Enabled=true;
						 //domainUpDown2->Enabled=true;
						 //domainUpDown3->Enabled=true;
						 //domainUpDown4->Enabled=true;
						 //domainUpDown5->Enabled=true;
						 //domainUpDown6->Enabled=true;
						 //domainUpDown7->Enabled=true;
						 //domainUpDown8->Enabled=true;
						 //domainUpDown9->Enabled=true;
						 //button5->Enabled=true;
						 //button9->Enabled=true;
						 //button1->Enabled=true;
						 //button13->Enabled=true;

						 //label5->Enabled=true;
						 //textBox4->Enabled=true;

					 }
				 }
				 //reset the posiiton counter to 0
			 };

	private: System::Void button9_Click(System::Object*  sender, System::EventArgs*  e) {
				 textBox5->Text=" ";
				 long res=0;
				 int am=30;
				 //backgroundWorker3->RunWorkerAsync( );
				 Xp=0;
				 Yp=0;
				 Zp=-250;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=0;
				 Yp=0;
				 Zp=-550;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);


				 Xp=0;
				 Yp=0;
				 Zp=-280;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=-am;
				 Yp=0;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=-am;
				 Yp=-am;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=am;
				 Yp=-am;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=am;
				 Yp=am;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=-am;
				 Yp=am;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=-am;
				 Yp=0;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);


				 Xp=0;
				 Yp=0;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 Xp=0;
				 Yp=0;
				 Zp=0;
				 res=func_2();
				 //textBox4->Text=Convert::ToString(res);

				 //Xp=vScrollBar5->Maximum/2-vScrollBar5->Value;
				 //Yp=vScrollBar6->Maximum/2-vScrollBar6->Value;
				 //Zp=-vScrollBar7->Value;//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 //res=func_2();
				 //textBox4->Text=Convert::ToString(res);
			 }
	
	private: System::Void numericUpDown1_ValueChanged_1(System::Object*  sender, System::EventArgs*  e) {
				 //			 kp1=System::Convert::ToInt32(domainUpDown1->Text);
				 //domainUpDown1->SelectedIndex=kp1;
				 //label19->Text=kp1.ToString();
				 //			 M1posDes=-vScrollBar1->Value+vScrollBar1->Maximum/2;
				 //label7->Text=M1posDes.ToString();
				 //label16->Text=Pulses2Rad(M1posDes).ToString();
				 //vScrollBar1->Value=-int(numericUpDown1->Value)+ vScrollBar1->Maximum/2;
				 //vScrollBar4->Value=-int(numericUpDown1->Value)+ vScrollBar1->Maximum/2;
				 M1posDes=int(numericUpDown1->Value);
				 //label7->Text=M1posDes.ToString();
//				 label16->Text=Pulses2Rad(M1posDes).ToString();
			 }
	private: System::Void numericUpDown2_ValueChanged_1(System::Object*  sender, System::EventArgs*  e) {
				 //vScrollBar2->Value=-int(numericUpDown2->Value)+ vScrollBar2->Maximum/2;
				 //vScrollBar4->Value=-int(numericUpDown2->Value)+ vScrollBar2->Maximum/2;
				 M2posDes=int(numericUpDown2->Value);
				 //label8->Text=M2posDes.ToString();
//				 label17->Text=Pulses2Rad(M2posDes).ToString();

			 }
	private: System::Void numericUpDown3_ValueChanged_1(System::Object*  sender, System::EventArgs*  e) {
				 //vScrollBar3->Value=-int(numericUpDown3->Value)+ vScrollBar3->Maximum/2;
				 //vScrollBar4->Value=-int(numericUpDown3->Value)+ vScrollBar3->Maximum/2;
				 M3posDes=int(numericUpDown3->Value);
				 //label9->Text=M3posDes.ToString();
//				 label18->Text=Pulses2Rad(M3posDes).ToString();
			 }
	private: System::Void button10_Click_1(System::Object*  sender, System::EventArgs*  e) {
				 //textBox5->Text=System::String::Concat(textBox5->Text," DESPLAZAMIENTO ARTICULAR: ...");
				 groupBox1->Enabled=false;
				 groupBox2->Enabled=false;
				 if(checkBox1->Checked==true){
					checkBox1->Checked=false;
					groupBox5->Enabled=false;
					}
				 Pos3To(M1posDes,VelGen,AcelGen,M2posDes,VelGen,AcelGen,M3posDes,VelGen,AcelGen);
				 //label7->Text=M1posDes.ToString();
				 //PosTo(1,M1posDes, VelGen, AcelGen);
				 //label8->Text=M2posDes.ToString();
				 //PosTo(2,M2posDes,VelGen, AcelGen);
				 //label9->Text=M3posDes.ToString();
				 //PosTo(3,M3posDes,VelGen, AcelGen);

				 groupBox1->Enabled=true;
				 groupBox2->Enabled=true;
				 textBox5->Text=System::String::Concat(textBox5->Text," JOINT DISPLACEMENT: ");
				 textBox5->Text=System::String::Concat(textBox5->Text," M1:",M1posDes.ToString());
				 textBox5->Text=System::String::Concat(textBox5->Text,", M2:",M2posDes.ToString());
				 textBox5->Text=System::String::Concat(textBox5->Text,", M3:",M3posDes.ToString(),"\r\n");
				 Cajadetexto();
			 }
	private: System::Void numericUpDown4_ValueChanged(System::Object*  sender, System::EventArgs*  e) {
				 VelGen=int(numericUpDown4->Value);

			 }
	private: System::Void numericUpDown5_ValueChanged(System::Object*  sender, System::EventArgs*  e) {
				 AcelGen=int(numericUpDown5->Value);
			 }
	private: System::Void button11_Click(System::Object*  sender, System::EventArgs*  e) {
				 textBox5->Text=" ";
				 //textBox6->Text=" ";
				 long res=0;
				 int am=30;
				 int flag=0;
				 int i=0;
				 int j=0;

				 int Yaux[7];
				 int Xaux[7];


				 StreamWriter *fs;
				 String *ruta;
				 saveFileDialog1->Title="Guardar Entorno como:";
				 saveFileDialog1->FileName="Entorno.txt";
				 saveFileDialog1->AddExtension=true;
				 saveFileDialog1->DefaultExt="txt";
				 saveFileDialog1->ShowDialog();
				 ruta=saveFileDialog1->FileName->ToString();


				 Xaux[0]=-30;
				 Xaux[1]=-20;
				 Xaux[2]=-10;
				 Xaux[3]=-0;
				 Xaux[4]=10;
				 Xaux[5]=20;
				 Xaux[6]=30;

				 Yaux[0]=-30;
				 Yaux[1]=-20;
				 Yaux[2]=-10;
				 Yaux[3]=-0;
				 Yaux[4]=10;
				 Yaux[5]=20;
				 Yaux[6]=30;


				 if (ruta->Empty){//ruta->Length){
					 //textBox6->Text=System::String::Concat(textBox6->Text,"TH1 "," TH2 ");
					 //textBox6->Text=System::String::Concat(textBox6->Text," TH3 "," Xp ");
					 //textBox6->Text=System::String::Concat(textBox6->Text," Yp "," Zp ");
					 //textBox6->Text=System::String::Concat(textBox6->Text,"\r\n");

					 Xp=0;
					 Yp=0;
					 Zp=double(-numericUpDown9->Value);//vScrollBar7->Maximum/2-vScrollBar7->Value;
					 res=func_2();
					 Cajadetexto();
					 Sleep(sleepingtime);



					 flag=0;
					 //Zp=-31;
					 //res=func_2();
					 //Cajadetexto();
					 //Sleep(sleepingtime);

					 for(j=0;j<7;j=j+1)
					 {
						 Yp=Yaux[j];
						 if (flag==0)
						 {
							 for(i=0;i<7;i=i+1){
								 Xp=Xaux[i];
								 if(i==6){
									 flag=1;
								 }
								 res=func_2();
								 Cajadetexto();
								 Sleep(sleepingtime);
							 }
						 }else{
							 for(i=6;i>=0;i=i-1){
								 Xp=Xaux[i];
								 if(i==0){
									 flag=0;
								 }
								 res=func_2();
								 Cajadetexto();
								 Sleep(sleepingtime);
							 }

						 }	

					 }


					 Xp=0;
					 Yp=0;
					 Zp=-260;
					 res=func_2();
					 Cajadetexto();
					 Sleep(sleepingtime);


					 fs = new StreamWriter(ruta); 
					 //fs->Write(textBox6->Text);
					 fs->Close();

				 }
				 //Xp=vScrollBar5->Maximum/2-vScrollBar5->Value;
				 //Yp=vScrollBar6->Maximum/2-vScrollBar6->Value;
				 //Zp=-vScrollBar7->Value;//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 //res=func_2();
				 //textBox4->Text=Convert::ToString(res);


				 //timer2->Enabled=true;
			 }
			 void Cajadetexto(void)
			 {
				 //textBox5->Text=System::String::Concat(textBox6->Text," ",TH1pulses[0].ToString());
				 //textBox5->Text=System::String::Concat(textBox6->Text," ",TH1pulses[1].ToString());
				 //textBox5->Text=System::String::Concat(textBox6->Text," ",TH1pulses[2].ToString());
				 //textBox5->Text=System::String::Concat(textBox6->Text,TH1[0].ToString());
				 // NUEVO
				 //textBox5->Text=System::String::Concat(textBox5->Text," ",TH1[0].ToString());
				 //textBox5->Text=System::String::Concat(textBox5->Text," ",TH1[1].ToString());
				 //textBox5->Text=System::String::Concat(textBox5->Text," ",TH1[2].ToString());
				 //textBox5->Text=System::String::Concat(textBox5->Text," ",Xp.ToString());
				 //textBox5->Text=System::String::Concat(textBox5->Text," ",Yp.ToString());
				 //textBox5->Text=System::String::Concat(textBox5->Text," ",Zp.ToString(),"\r\n");
				 textBox5->Select();//selecciona la caja de texto
				 textBox5->SelectionStart=textBox5->Text->Length;//revisa la longitud del texto en la caja
				 textBox5->SelectionLength=0;//se va a seleccionar una cadena de 0 de longitud
				 textBox5->ScrollToCaret();//manda a donde se encuentra el caret
			 }

	private: System::Void button12_Click(System::Object*  sender, System::EventArgs*  e) {
				 //vScrollBar7->Value=310;
				 //Zp=-vScrollBar7->Value;
				 //label15->Text=Zp.ToString();
				 //button11->Enabled=true;
			 }
	private: System::Void timer2_Tick(System::Object*  sender, System::EventArgs*  e) {
				 double ic=0;

				 timer2->Enabled=false;

			 }
	private: System::Void label31_Click(System::Object*  sender, System::EventArgs*  e) {
			 }
	private: System::Void numericUpDown6_ValueChanged(System::Object*  sender, System::EventArgs*  e) {
				 sleepingtime=int(numericUpDown6->Value);

			 }
	private: System::Void button13_Click(System::Object*  sender, System::EventArgs*  e) {

				 numericUpDown9->Value=220;
				 textBox5->Text=System::String::Concat(textBox5->Text," CALIBRATION SEQUENCE ...\r\n");
				 groupBox1->Enabled=false;
				 groupBox3->Enabled=false;
				 if(checkBox1->Checked==true){
					checkBox1->Checked=false;
					groupBox5->Enabled=false;
					}
				 //textBox6->Text=" ";
				 long res=0;
				 int am=3;
				 int flag=0;
				 int i=0;
				 int j=0;

				 int k=0;
				 int Yaux[7];
				 int Xaux[7];




				 Xaux[0]=-30;
				 Xaux[1]=-20;
				 Xaux[2]=-10;
				 Xaux[3]=-0;
				 Xaux[4]=10;
				 Xaux[5]=20;
				 Xaux[6]=30;

				 Yaux[0]=-30;
				 Yaux[1]=-20;
				 Yaux[2]=-10;
				 Yaux[3]=-0;
				 Yaux[4]=10;
				 Yaux[5]=20;
				 Yaux[6]=30;


				 //textBox6->Text=System::String::Concat(textBox6->Text,"TH1 "," TH2 ");
				 //textBox6->Text=System::String::Concat(textBox6->Text," TH3 "," Xp ");
				 //textBox6->Text=System::String::Concat(textBox6->Text," Yp "," Zp ");
				 //textBox6->Text=System::String::Concat(textBox6->Text,"\r\n");


				 Xp=0;
				 Yp=0;
				 Zp=double(-numericUpDown9->Value);//-vScrollBar7->Value;//vScrollBar7->Maximum/2-vScrollBar7->Value;
				 res=func_2();
				 Cajadetexto();
				 Sleep(0);
				 //numericUpDown4->Value=160000;
				 //numericUpDown5->Value=190;
				numericUpDown4->Value=50000;
				 numericUpDown5->Value=50;
				 for(k=0;k<8;k++){

					 flag=0;
					 for(j=0;j<7;j=j+2)
					 {
						 Yp=Yaux[j];
						 if (flag==0)
						 {
							 for(i=0;i<7;i=i+2){
								 Xp=Xaux[i];
								 if(i==6){
									 flag=1;
								 }
								 res=func_2();
								 Cajadetexto();
								 Sleep(0);
							 }
						 }else{
							 for(i=6;i>=0;i=i-2){
								 Xp=Xaux[i];
								 if(i==0){
									 flag=0;
								 }
								 res=func_2();
								 Cajadetexto();
								 Sleep(0);
							 }

						 }	

					 }
					 Xp=0;
					 Yp=0;
					 Zp=double(-numericUpDown9->Value)-k*30;//-vScrollBar7->Value-k*30;//vScrollBar7->Maximum/2-vScrollBar7->Value;
					 res=func_2();
					 Cajadetexto();
					 Sleep(0);

				 }
				 numericUpDown4->Value=40000;
				 numericUpDown5->Value=20;

				 Xp=0;
				 Yp=0;
				 Zp=-260;
				 res=func_2();
				 Cajadetexto();
				 Sleep(0);

				 groupBox1->Enabled=true;
				 groupBox3->Enabled=true;
				 textBox5->Text=System::String::Concat(textBox5->Text," CALIBRATION FINISHED.\r\n");
				 Cajadetexto();

			 }
	private: System::Void label2_Click(System::Object*  sender, System::EventArgs*  e) {
			 }
private: System::Void textBox1_TextChanged(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void label7_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void domainUpDown1_SelectedItemChanged(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void textBox6_TextChanged(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void label28_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void textBox4_TextChanged(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void numericUpDown7_ValueChanged(System::Object*  sender, System::EventArgs*  e) {
			Xp=double(numericUpDown7->Value);
			label13->Text=Xp.ToString();
			button1->Enabled=true;
		 }
private: System::Void numericUpDown8_ValueChanged(System::Object*  sender, System::EventArgs*  e) {
			Yp=double(numericUpDown8->Value);
			label14->Text=Yp.ToString();
			button1->Enabled=true;
		 }
private: System::Void numericUpDown9_ValueChanged(System::Object*  sender, System::EventArgs*  e) {
			Zp=double(-numericUpDown9->Value);
			label15->Text=Zp.ToString();
			button1->Enabled=true;
		 }
private: System::Void label29_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void label8_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void saveFileDialog1_FileOk(System::Object*  sender, System::ComponentModel::CancelEventArgs*  e) {
		 }
private: System::Void label1_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void button2_Click(System::Object*  sender, System::EventArgs*  e) {
					textBox5->Text=System::String::Concat(textBox5->Text," EMERGENCY STOP, AMPLIFIERS DISABLED. \r\n");
					Cajadetexto();
					groupBox1->Enabled=false;
					groupBox3->Enabled=false;
					groupBox2->Enabled=false;
					button13->Enabled=false;
					if(checkBox1->Checked==true){
						checkBox1->Checked=false;
						groupBox5->Enabled=false;
					}
					 motEnableDis(1,0);
					 motEnableDis(2,0);
					 motEnableDis(3,0);
					button11->Enabled=true;
					 //label2->BackColor = System::Drawing::Color::Red;
					 //label3->BackColor = System::Drawing::Color::Red;
					 //label4->BackColor = System::Drawing::Color::Red;
					 //EnDisBars(0);		 
		 
		 }

private: System::Void button7_Click_1(System::Object*  sender, System::EventArgs*  e) {
			 int idesp=0; 
			 long res=0;

			 textBox5->Text=System::String::Concat(textBox5->Text," SEQUENCE IN EXECUTION ... \r\n");
			 for(idesp=1;idesp<=numdesp;idesp=idesp+1){
				// Escribir aqui el codigo para generar una salida digital con PIC-IO   
				// Vefector[idesp]
					if (Vefector[idesp]==1){
					IoSetOutBit(4, 6); //addr=1, Set output bit 7 to logic 1, use number 6 for bit #7
					} else {
					IoClrOutBit(4, 6); //addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
					}
					//IoClrOutBit(4, 6); //addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
					//IoSetOutBit(4, 6); //addr=1, Set output bit 7 to logic 1, use number 6 for bit #7
					//
					Xp=VXp[idesp];
					Yp=VYp[idesp];
					Zp=VZp[idesp];
					VelGen=VVelGen[idesp];
					AcelGen=VAcelGen[idesp];
					res=func_2();
					//Cajadetexto();
					Sleep(Vespera[idesp]);
			 }
			textBox5->Text=System::String::Concat(textBox5->Text," SEQUENCE FINISHED. \r\n");
			IoClrOutBit(4, 6); //addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
			Cajadetexto();
		 }
private: System::Void button3_Click_1(System::Object*  sender, System::EventArgs*  e) {
			 numdesp=numdesp+1;
			 if(numdesp>50){
				 textBox5->Text=System::String::Concat(textBox5->Text," ERROR: Only 50 displacement can be saved !\r\n");
				 numdesp=50;
				}
			 textBox5->Text=System::String::Concat(textBox5->Text,numdesp.ToString(),": ");
			 textBox5->Text=System::String::Concat(textBox5->Text,"  X",Xp.ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  Y",Yp.ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  Z",Zp.ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  E",efector.ToString());
		     textBox5->Text=System::String::Concat(textBox5->Text,",  D",sleepingtime.ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  V",VelGen.ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  A",AcelGen.ToString(),"\r\n");
			 Cajadetexto();
			 VXp[numdesp]=Xp;
			 VYp[numdesp]=Yp;
			 VZp[numdesp]=Zp;
			 Vefector[numdesp]=efector;
			 Vespera[numdesp]=sleepingtime;
			 VVelGen[numdesp]=VelGen;
			 VAcelGen[numdesp]=AcelGen;
		 }
private: System::Void radioButton3_CheckedChanged(System::Object*  sender, System::EventArgs*  e) {
			 efector=0;
		 }
private: System::Void radioButton4_CheckedChanged(System::Object*  sender, System::EventArgs*  e) {
			 efector=1;
		 }
private: System::Void button8_Click_1(System::Object*  sender, System::EventArgs*  e) {
			 int idesp=0; 
			 textBox5->Text=System::String::Concat(textBox5->Text," SEQUENCE SAVED: \r\n");
			 for(idesp=1;idesp<=numdesp;idesp=idesp+1){
			   textBox5->Text=System::String::Concat(textBox5->Text,idesp.ToString(),": ");
			 textBox5->Text=System::String::Concat(textBox5->Text,"  X",VXp[idesp].ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  Y",VYp[idesp].ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  Z",VZp[idesp].ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  E",Vefector[idesp].ToString());
		     textBox5->Text=System::String::Concat(textBox5->Text,",  D",Vespera[idesp].ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  V",VVelGen[idesp].ToString());
			 textBox5->Text=System::String::Concat(textBox5->Text,",  A",VAcelGen[idesp].ToString(),"\r\n");
			 }
			 Cajadetexto();
		 }
private: System::Void button9_Click_1(System::Object*  sender, System::EventArgs*  e) {
			 textBox5->Text=System::String::Concat(textBox5->Text," SEQUENCE DELETED. \r\n");
			 Cajadetexto();
			 numdesp=0;
		 }
private: System::Void button11_Click_1(System::Object*  sender, System::EventArgs*  e) {
			         motEnableDis(1,1);
					 motEnableDis(2,1);
					 motEnableDis(3,1);
					 label2->BackColor = System::Drawing::Color::Green;
					 label3->BackColor = System::Drawing::Color::Green;
					 label4->BackColor = System::Drawing::Color::Green;
					 EnDisBars(1);
					 button4->Enabled=true;
					 textBox5->Text=System::String::Concat(textBox5->Text," AMPLIFIERS ENABLED. \r\n");
					 Cajadetexto();
					 inicioOK = 0;
		 }
private: System::Void label13_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void label16_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void checkBox1_CheckedChanged_1(System::Object*  sender, System::EventArgs*  e) {
			 if(checkBox1->Checked==true){
				checkBox1->Text=" ENABLING MODIFICATION OF CONTROL PARAMETERS.";
				groupBox5->Enabled=true;
			 } else {
				checkBox1->Text=" APPLY GAINS.";
				groupBox5->Enabled=false;
			 }
		 }
private: System::Void groupBox3_Enter(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void groupBox2_Enter(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void textBox5_TextChanged(System::Object*  sender, System::EventArgs*  e) {
		 }
private: System::Void label6_Click(System::Object*  sender, System::EventArgs*  e) {
		 }
};
}


