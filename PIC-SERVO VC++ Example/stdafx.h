// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// TODO: reference additional headers your program requires here
bool	addMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut);
bool	subMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut);
double	dotMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2);
bool	equalMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2);
bool	crossMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut);
bool	skewMat(int r1, int c1, const double *M1, int rOut, int cOut, double *MOut);
bool	multMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut);
bool	setMat(int r1, int c1, const double *M1, int rOut, int cOut, double *MOut);
bool	RotMat(int eje,double theta, double *MOut);
bool	RotMat(int eje,double theta,double *trasl, double *MOut);

double cO(double th);
double sI(double th);
double Ta(double th);
double acO(double th);
double asI(double th);
double atA(double th);
double sqrT(double R);




