#include "stdafx.h"
#include <fstream>
#include <sstream>
#include <string>
#include "Form1.h"
#include <windows.h>


using namespace PICSERVOVCExample;



int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	System::Threading::Thread::CurrentThread->ApartmentState = System::Threading::ApartmentState::STA;
	Application::Run(new Form1());
	return 0;
}
