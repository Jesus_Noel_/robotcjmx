typedef unsigned char byte;

typedef struct _NMCMOD {
	byte	modtype;		//module type
    byte	modver;			//module version number
	byte	statusitems;	//definition of items to be returned
	byte	stat;  			//status byte
    byte	groupaddr;		//current group address
    BOOL	groupleader;	//TRUE if group leader
    void *	p;				//pointer to specific module's data structure
    } NMCMOD;

#define MAXSIOERROR 2

//Define PIC baud rate divisors
#define	PB19200		63
#define	PB57600		20
#define	PB115200	10
#define	PB230400	5

//Module type definitions:
#define	SERVOMODTYPE	0
#define	ADCMODTYPE		1
#define	IOMODTYPE		2
#define	STEPMODTYPE		3
//The following must be created for each new module type:
//		data structure XXXMOD
//		Initializer function NewXXXMod
//		Status reading function GetXXXStat
//		NMCInit and SendNmcCmd must be modified to include calls
//			to the two functions above

#define CKSUM_ERROR		0x02	//Checksum error bit in status byte

//--------------------- ADC Module specific stuff --------------------------
typedef struct _ADCMOD {
    //******  Move all this data to the NMCMOD structure *******
	short int ad0;	//definition of items to be returned
	short int ad1;
	short int ad2;
	short int ad3;
	short int ad4;
	short int ad5;
    } ADCMOD;


#define MAXNUMMOD	33

//Function prototypes:
[DllImport("NMCLIB04.dll", EntryPoint = "AdcNewMod", CharSet = Unicode)]
ADCMOD * AdcNewMod();

[DllImport("NMCLIB04.dll", EntryPoint = "AdcGetStat", CharSet = Unicode)]
BOOL AdcGetStat(byte addr);

//Initialization and shutdown
[DllImport("NMCLIB04.dll", EntryPoint = "NmcInit", CharSet = Unicode)]
int NmcInit(char *portname, unsigned int baudrate);

[DllImport("NMCLIB04.dll", EntryPoint = "InitVars", CharSet = Unicode)]
void InitVars(void);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcSendCmd", CharSet = Unicode)]
BOOL NmcSendCmd(byte addr, byte cmd, char *datastr, byte n, byte stataddr);

[DllImport("NMCLIB04.dll", EntryPoint = "FixSioError", CharSet = Unicode)]
void FixSioError(byte addr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcShutdown", CharSet = Unicode)]
void NmcShutdown(void);

//Module type independant commands (supported by all module types)
[DllImport("NMCLIB04.dll", EntryPoint = "NmcSetGroupAddr", CharSet = Unicode)]
BOOL NmcSetGroupAddr(byte addr, byte groupaddr, BOOL leader);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcDefineStatus", CharSet = Unicode)]
BOOL NmcDefineStatus(byte addr, byte statusitems);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcReadStatus", CharSet = Unicode)]
BOOL NmcReadStatus(byte addr, byte statusitems);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcSynchOutput", CharSet = Unicode)]
BOOL NmcSynchOutput(byte groupaddr, byte leaderaddr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcChangeBaud", CharSet = Unicode)]
BOOL NmcChangeBaud(byte groupaddr, unsigned int baudrate);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcSynchInput", CharSet = Unicode)]
BOOL NmcSynchInput(byte groupaddr, byte leaderaddr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcNoOp", CharSet = Unicode)]
BOOL NmcNoOp(byte addr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcHardReset", CharSet = Unicode)]
BOOL NmcHardReset(byte addr);

//Retrieve module type independant data from a module's data structure
[DllImport("NMCLIB04.dll", EntryPoint = "NmcGetStat", CharSet = Unicode)]
byte NmcGetStat(byte addr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcGetStatItems", CharSet = Unicode)]
byte NmcGetStatItems(byte addr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcGetModType", CharSet = Unicode)]
byte NmcGetModType(byte addr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcGetModVer", CharSet = Unicode)]
byte NmcGetModVer(byte addr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcGetGroupAddr", CharSet = Unicode)]
byte NmcGetGroupAddr(byte addr);

[DllImport("NMCLIB04.dll", EntryPoint = "NmcGroupLeader", CharSet = Unicode)]
BOOL NmcGroupLeader(byte addr);




