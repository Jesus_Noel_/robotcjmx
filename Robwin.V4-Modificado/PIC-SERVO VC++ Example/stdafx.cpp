// stdafx.cpp : source file that includes just the standard includes
// PIC-SERVO VC++ Example.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

bool addMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut) {
	//MOut = M1+M2 (of course, only works if size(M1) = size(M2))
	if(r1 == r2 && c1 == c2 && r1 == rOut && c1 == cOut) {
		for(int i=0; i<r1*c1; i++)
			MOut[i] = M1[i] + M2[i];
		return true;
	}
	return false;
}

bool subMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut) {
	//MOut = M1-M2 (of course, only works if size(M1) = size(M2) and Ans must be of the same size)
	if(r1 == r2 && c1 == c2 && r1 == rOut && c1 == cOut) {
		for(int i=0; i<r1*c1; i++)
			MOut[i] = M1[i] - M2[i];
		return true;
	}
	return false;
}

double dotMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2) {
	//Ans = M1�M2 (M1 and M2 must be 1x3 vectors)
	if(r1 == 1 && c1 == 3 && r2 == 1 && c2 == 3) {
		double ans=0;
		for(int i=0; i < c1; i++)
			ans += M1[i]*M2[i];
		return ans;
	}
	return 0;
};

bool equalMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2) {
	//returns true if size(M1)=size(M2) and for every i,j M1(i,j) = M2(i,j), otherwise false.
	if(r1 == r2 && c1 == c2) {
		for(int i=0; i < r1*c1; i++) 
			if(M1[i] != M2[i])
				return false;

		return true;
	}
	return false;
}

bool crossMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut) {
	//Ans = M1 x M2. M1 and M2 and Ans must be 1x3 vectors. 
	if(r1 == 1 && c1 == 3 && r2 == 1 && c2 == 3 && rOut == 1 && cOut == 3) {
		double M1_skew[9]; 
		skewMat(1,3,M1,3,3,M1_skew);
		multMat(3,3,M1_skew,3,1,M2,3,1,MOut);
		return true;
	}
	return false;
}

bool skewMat(int r1, int c1, const double *M1, int rOut, int cOut, double *MOut) {
	//M must be of size 1x3 and Ans must be 3x3.
	if(r1 == 1 && c1 == 3 && rOut == 3 && cOut == 3) {
		MOut[0] = 0;
		MOut[1] = -M1[2];
		MOut[2] = M1[1];
		MOut[3] = M1[2];
		MOut[4] = 0;
		MOut[5] = -M1[0];
		MOut[6] = -M1[1];
		MOut[7] = M1[0];
		MOut[8] = 0;

		return true;
	}
	return false;
}


bool multMat(int r1, int c1, const double *M1, int r2, int c2, const double *M2, int rOut, int cOut, double *MOut) {
	//MOut = M1*M2. Dimensions of matrixes must be correct (you know how it's supposed to be)
	double ans = 0;
	if(c1 == r2 && r1 == rOut && c2 == cOut) {
		for(int i=0; i < r1; i++) {
			for(int j=0; j < c2; j++) {
				for(int k=0; k < c1; k++) {
					ans += M1[i*c1 + k] * M2[k*c2 + j];
				}
				MOut[i*cOut + j] = ans;
				ans=0;
			}
		}
		return true;
	}
	return false;
}
bool setMat(int r1, int c1, const double *M1, int rOut, int cOut, double *MOut) {
	// MOut = M1.
	if(r1 == rOut && c1 == cOut) {
		for(int i=0; i < r1*c1; i++)
			MOut[i] = M1[i];
		return true;
	}
	return false;
}

bool	RotMat(int eje,double theta,double *trasl, double *MOut){

	if(eje==1){
		MOut[0]=1;
		MOut[1]=0;
		MOut[2]=0;
		MOut[3]=trasl[0];

		MOut[4]=0;
		MOut[5]=System::Math::Cos(theta);
		MOut[6]=-System::Math::Sin(theta);
		MOut[7]=trasl[1];

		MOut[8]=0;
		MOut[9]=System::Math::Sin(theta);
		MOut[10]=System::Math::Cos(theta);
		MOut[11]=trasl[2];

		MOut[12]=0;
		MOut[13]=0;
		MOut[14]=0;
		MOut[15]=1;
	}else {

		/*		     [       1                     0                  0           traslac(1);
		0                cos(theta)    -sin(theta)     traslac(2);
		0                sin(theta)      cos(theta)    traslac(3);
		0                     0                  0      escala];
		*/ 
		if(eje==2){
			MOut[0]=System::Math::Cos(theta);
			MOut[1]=0;
			MOut[2]=System::Math::Sin(theta);
			MOut[3]=trasl[0];

			MOut[4]=0;
			MOut[5]=1;
			MOut[6]=0;
			MOut[7]=trasl[1];

			MOut[8]=-System::Math::Sin(theta);
			MOut[9]=0;
			MOut[10]=System::Math::Cos(theta);
			MOut[11]=trasl[2];

			MOut[12]=0;
			MOut[13]=0;
			MOut[14]=0;
			MOut[15]=1;
		}else{
			//RotacYtrasl=[    cos(theta)        0         sin(theta)         traslac(1);
			//                   0                 1               0               traslac(2);
			//               -sin(theta)        0         cos(theta)        traslac(3);
			//                   0                 0               0            escala];
			if(eje==3){
				MOut[0]=System::Math::Cos(theta);
				MOut[1]=-System::Math::Sin(theta);
				MOut[2]=0;
				MOut[3]=trasl[0];

				MOut[4]=System::Math::Sin(theta);
				MOut[5]=System::Math::Cos(theta);
				MOut[6]=0;
				MOut[7]=trasl[1];

				MOut[8]=0;
				MOut[9]=0;
				MOut[10]=1;
				MOut[11]=trasl[2];

				MOut[12]=0;
				MOut[13]=0;
				MOut[14]=0;
				MOut[15]=1;
			}else{
				return(false);
			}
		}
	}
	//RotacYtrasl=[ cos(theta)     -sin(theta)        0          traslac(1);
	//            sin(theta)       cos(theta)        0          traslac(2);
	//                 0                     0             1           traslac(3);
	//                 0                     0             0         escala];
	return(true);
}
bool	RotMat(int eje,double theta, double *MOut){

	if(eje==1){
		MOut[0]=1;
		MOut[1]=0;
		MOut[2]=0;
		MOut[3]=0;

		MOut[4]=0;
		MOut[5]=System::Math::Cos(theta);
		MOut[6]=-System::Math::Sin(theta);
		MOut[7]=0;

		MOut[8]=0;
		MOut[9]=System::Math::Sin(theta);
		MOut[10]=System::Math::Cos(theta);
		MOut[11]=0;

		MOut[12]=0;
		MOut[13]=0;
		MOut[14]=0;
		MOut[15]=1;
	}else {

		/*		     [       1                     0                  0           traslac(1);
		0                cos(theta)    -sin(theta)     traslac(2);
		0                sin(theta)      cos(theta)    traslac(3);
		0                     0                  0      escala];
		*/ 
		if(eje==2){
			MOut[0]=System::Math::Cos(theta);
			MOut[1]=0;
			MOut[2]=System::Math::Sin(theta);
			MOut[3]=0;

			MOut[4]=0;
			MOut[5]=1;
			MOut[6]=0;
			MOut[7]=0;

			MOut[8]=-System::Math::Sin(theta);
			MOut[9]=0;
			MOut[10]=System::Math::Cos(theta);
			MOut[11]=0;

			MOut[12]=0;
			MOut[13]=0;
			MOut[14]=0;
			MOut[15]=1;
		}else{
			//RotacYtrasl=[    cos(theta)        0         sin(theta)         traslac(1);
			//                   0                 1               0               traslac(2);
			//               -sin(theta)        0         cos(theta)        traslac(3);
			//                   0                 0               0            escala];
			if(eje==3){
				MOut[0]=System::Math::Cos(theta);
				MOut[1]=-System::Math::Sin(theta);
				MOut[2]=0;
				MOut[3]=0;

				MOut[4]=System::Math::Sin(theta);
				MOut[5]=System::Math::Cos(theta);
				MOut[6]=0;
				MOut[7]=0;

				MOut[8]=0;
				MOut[9]=0;
				MOut[10]=1;
				MOut[11]=0;

				MOut[12]=0;
				MOut[13]=0;
				MOut[14]=0;
				MOut[15]=1;
			}else{
				return(false);
			}
		}
	}
	//RotacYtrasl=[ cos(theta)     -sin(theta)        0          traslac(1);
	//            sin(theta)       cos(theta)        0          traslac(2);
	//                 0                     0             1           traslac(3);
	//                 0                     0             0         escala];
	return(true);
}

double cO(double th){
	return(System::Math::Cos(th));
}

double sI(double th){
	return(System::Math::Sin(th));
};
double Ta(double th){
	return(System::Math::Tan(th));
};
double acO(double th){
	return(System::Math::Acos(th));
};
double asI(double th){
	return(System::Math::Asin(th));
};
double atA(double th){
	return(System::Math::Atan(th));
};
double sqrT(double R){
	return(System::Math::Sqrt(R));
};


