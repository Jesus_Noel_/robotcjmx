﻿Module FuncionesLi
    '
    'This module contains constant and function definitions used
    'in NMCLIB04.DLL.  There are four sets of constants and function
    'declarations for the following areas of operations:
    '       Core NMC module communications
    '       PIC-SERVO operations
    '       PIC-I/O operations
    '       PIC-STEP operations
    '

    '
    '****** Core NMC Communications definitions ******
    '

    '
    'Module type definitions:
    '
    Public Const SERVOMODTYPE = 0
    Public Const IOMODTYPE = 2
    Public Const STEPMODTYPE = 3


    '
    'NMC Communications functions
    '
    Declare Function NmcInit Lib "nmclib04" (ByVal portname As String, ByVal baudrate As Integer) As Integer
    Declare Sub NmcShutdown Lib "nmclib04" ()
 Declare Function NmcReadStatus Lib "nmclib04" (ByVal addr As Byte, ByVal statusitems As Byte) As Boolean
 Declare Function NmcNoOp Lib "nmclib04" (ByVal addr As Byte) As Boolean
    Declare Function NmcGetStat Lib "nmclib04" (ByVal addr As Byte) As Byte
    Public Const SEND_POS = &H1         '4 bytes data
    Public Const AMP_ENABLE = &H1       '1 = raise amp enable output, 0 = lower amp enable
    Public Const MOTOR_OFF = &H2        ' set to turn motor off
    Public Const STOP_ABRUPT = &H4      ' set to stop motor immediately
    Public Const STOP_SMOOTH = &H8      ' set to decellerate motor smoothly
   Public Const LOAD_POS = &H1         ' +4 bytes
    Public Const LOAD_VEL = &H2         ' +4 bytes
    Public Const LOAD_ACC = &H4         ' +4 bytes
    Public Const ENABLE_SERVO = &H10    ' 1 = servo mode, 0 = PWM mode
     Public Const START_NOW = &H80       ' 1 = start now, 0 = wait for START_MOVE command
    Public Const ON_LIMIT1 = &H1        ' home on change in limit 1
    Public Const ON_LIMIT2 = &H2        ' home on change in limit 2
    Public Const HOME_MOTOR_OFF = &H4   ' turn motor off when homed
    Public Const ON_INDEX = &H8           ' home on change in index
    Public Const HOME_STOP_ABRUPT = &H10  ' stop abruptly when homed
    Public Const MOVE_DONE = &H1        ' set when move done (trap. pos mode), when goal
  Public Const HOME_IN_PROG = &H80    ' set while searching for home, cleared when home found
    Public Const PATH_MODE = &H40       ' path mode is enabled (v.5)
    Declare Function ServoSetGain Lib "nmclib04" (ByVal addr As Byte, ByVal kp As Short, ByVal kd As Short, ByVal ki As Short, ByVal il As Short, ByVal ol As Byte, ByVal cl As Byte, ByVal el As Short, ByVal sr As Byte, ByVal dc As Byte) As Boolean
     Declare Function ServoResetPos Lib "nmclib04" (ByVal addr As Byte) As Boolean
    Declare Function ServoSetPos Lib "nmclib04" (ByVal addr As Byte, ByVal pos As Integer) As Boolean
     Declare Function ServoStopMotor Lib "nmclib04" (ByVal addr As Byte, ByVal mode As Byte) As Boolean
    Declare Function ServoSetHoming Lib "nmclib04" (ByVal addr As Byte, ByVal mode As Byte) As Boolean
    Declare Function ServoLoadTraj Lib "nmclib04" (ByVal addr As Byte, ByVal mode As Byte, ByVal pos As Integer, ByVal vel As Integer, ByVal acc As Integer, ByVal pwm As Byte) As Boolean
    Declare Function ServoGetPos Lib "nmclib04" (ByVal addr As Byte) As Integer
    Public Const SEND_INPUTS = &H1      ' 2 bytes data
 Declare Function IoInBitVal Lib "nmclib04" (ByVal addr As Byte, ByVal bitnum As Byte) As Boolean
    Declare Function IoSetOutBit Lib "nmclib04" (ByVal addr As Byte, ByVal bitnum As Integer) As Boolean
    Declare Function IoClrOutBit Lib "nmclib04" (ByVal addr As Byte, ByVal bitnum As Integer) As Boolean
    Declare Function IoBitDirIn Lib "nmclib04" (ByVal addr As Byte, ByVal bitnum As Integer) As Boolean
    Declare Function IoBitDirOut Lib "nmclib04" (ByVal addr As Byte, ByVal bitnum As Integer) As Boolean

    Public Const ACCEL_DONE = &H8       ' set when acceleration portion of a move is done
    Public Const SLEW_DONE = &H10       ' set when slew portion of a move is done
    Public Const SERVO_OVERRUN = &H20   ' set if servo takes longer than the specified
    '****** Stepper Module Definitions ******
    '(note: some defined constants which are repeats of constants for the 
    ' PIC-SERVO are shown for your reference, but are commented out)
    Declare Function NmcSynchOutput Lib "nmclib04" (ByVal groupaddr As Byte, ByVal leaderaddr As Byte) As Boolean
    Declare Function NmcSetGroupAddr Lib "nmclib04" (ByVal addr As Byte, ByVal groupaddr As Byte, ByVal leader As Boolean) As Boolean
    Public Const STOP_MOTOR = &H7
    Public Const AMP_DISABLE As Integer = 0
    Public Function motEnableDis(ByVal Motor As Integer, ByVal EnDis As Integer)
        If EnDis = 1 Then
            ServoStopMotor(Motor, AMP_ENABLE And MOTOR_OFF)
            ServoStopMotor(Motor, AMP_ENABLE And STOP_ABRUPT)
            ' ServoResetPos(Motor)
        Else
            ServoStopMotor(Motor, AMP_DISABLE And MOTOR_OFF)
            ServoStopMotor(Motor, AMP_DISABLE And STOP_ABRUPT)
            ' ServoResetPos(Motor)
        End If
    End Function

    Public Function ActualizarDataGrid(ByVal gridno As Integer)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim StrSQL As String = "SELECT NUM, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM POSICIONES"
        Dim adp As New OleDb.OleDbDataAdapter(StrSQL, conn)
        ds.Tables.Add("Tabla")
        adp.Fill(ds.Tables("Tabla"))
        If gridno = 1 Then

            MainMenu.DataGridView1.DataSource = ds.Tables("Tabla")
        ElseIf gridno = 3 Then
            MenuRutinas.Posiciones.DataSource = ds.Tables("Tabla")
        End If
    End Function
    Public Function CreateAccessDatabase(ByVal DataBaseFullPath As String) As Boolean
        Dim bAns As Boolean
        Dim cat As New ADOX.Catalog()
        Try

            Dim sCreateString As String
            sCreateString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DataBaseFullPath
            cat.Create(sCreateString)

            bAns = True

        Catch Excep As System.Runtime.InteropServices.COMException
            bAns = False

        Finally
            cat = Nothing
        End Try
        Return bAns
    End Function
    Public Function LeerSetup()
        conn.Close()

        cmd.Connection = conn
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT KP, KI, KD, EL, DC, VEL, ACEL, ENCODERRES1, CONVFACTOR1, ENCODERRES2, CONVFACTOR2, ENCODERRES3, CONVFACTOR3, ENCONTRARHOMEPOR FROM CONFIGURACION"


        Try
            conn.Open()

            Try
                dr = cmd.ExecuteReader()

                While dr.Read()
                    kp01 = dr(0).ToString
                    ki01 = dr(1).ToString
                    kd01 = dr(2).ToString
                    kp02 = dr(0).ToString
                    ki02 = dr(1).ToString
                    kd02 = dr(2).ToString
                    kp03 = dr(0).ToString
                    ki03 = dr(1).ToString
                    kd03 = dr(2).ToString
                    EL = dr(3).ToString
                    DC = dr(4).ToString
                    VelGen = dr(5).ToString
                    AcelGen = dr(6).ToString
                    EncoderResolution1 = dr(7).ToString
                    ConversionFactor1 = dr(8).ToString
                    EncoderResolution2 = dr(9).ToString
                    ConversionFactor2 = dr(10).ToString
                    EncoderResolution3 = dr(11).ToString
                    ConversionFactor3 = dr(12).ToString
                    FindHomeBy = dr(13).ToString

                End While



            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
        dr.Close()
    End Function
    Public Function RadToPulses(ByVal Rad As Double) As Double



        Dim puls As Integer

        If interacion = 1 Then
            puls = (Rad * 100 * ((EncoderResolution1 * 4) * 14) / (2 * MainMenu.pi)) / 100

        ElseIf interacion = 2 Then
            puls = (Rad * 100 * ((EncoderResolution2 * 4) * 14) / (2 * MainMenu.pi)) / 100

        Else
            puls = (Rad * 100 * ((EncoderResolution3 * 4) * 14) / (2 * MainMenu.pi)) / 100
        End If
        'puls = (Rad * 100 * ((EncoderResolution * 4) * 14) / (2 * Form1.pi)) / 100
        Return puls

    End Function

    Public Function Calculate_Conversion_Factor()
        ConversionFactor1 = NewResolution1 / ReferenceResolution
        ConversionFactor2 = NewResolution2 / ReferenceResolution
        ConversionFactor3 = NewResolution3 / ReferenceResolution

        ' ReferenceResolution()
    End Function

    Public Function Calculate_Conversion_Factor2()
        If (ConversionFactor1 >= ConversionFactor2) And (ConversionFactor1 >= ConversionFactor3) Then
            ConversionFactorVelAcc1 = 1
            ConversionFactorVelAcc2 = (ConversionFactor1 / ConversionFactor2)
            ConversionFactorVelAcc3 = (ConversionFactor1 / ConversionFactor3)
        ElseIf (ConversionFactor2 >= ConversionFactor1) And (ConversionFactor2 >= ConversionFactor3) Then
            ConversionFactorVelAcc2 = 1
            ConversionFactorVelAcc1 = (ConversionFactor2 / ConversionFactor1)
            ConversionFactorVelAcc3 = (ConversionFactor2 / ConversionFactor3)
        ElseIf (ConversionFactor3 >= ConversionFactor1) And (ConversionFactor3 >= ConversionFactor2) Then
            ConversionFactorVelAcc3 = 1
            ConversionFactorVelAcc1 = (ConversionFactor3 / ConversionFactor1)
            ConversionFactorVelAcc2 = (ConversionFactor3 / ConversionFactor2)
        ElseIf (ConversionFactor3 = ConversionFactor1) And (ConversionFactor3 = ConversionFactor2) Then
            ConversionFactorVelAcc3 = 1
            ConversionFactorVelAcc1 = 1
            ConversionFactorVelAcc2 = 1

        End If
    End Function
    Public Function Home()
        Dim statbyte(3) As Int32
        Dim mode As Byte
        Dim pos, vel, acc As Integer
        Dim pwm As Integer
        Dim homing_mode As Byte
        Dim a As Boolean

        Dim still_homing(3) As Byte
        Calculate_Conversion_Factor2()

        homing_mode = 0

        ServoStopMotor(1, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(1, AMP_ENABLE Or STOP_ABRUPT)
        ServoStopMotor(2, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(2, AMP_ENABLE Or STOP_ABRUPT)
        ServoStopMotor(3, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(3, AMP_ENABLE Or STOP_ABRUPT)


        If FindHomeBy = 1 Then
            homing_mode = (ON_INDEX Or HOME_STOP_ABRUPT)
        ElseIf FindHomeBy = 2 Then
            homing_mode = (ON_LIMIT1 Or HOME_STOP_ABRUPT)
        Else
            homing_mode = (ON_LIMIT2 Or HOME_STOP_ABRUPT)
        End If
        mode = (LOAD_POS Or LOAD_VEL Or LOAD_ACC Or ENABLE_SERVO Or START_NOW)
        pos = -10000
        vel = VelGen * ConversionFactorVelAcc1

        acc = AcelGen * ConversionFactorVelAcc1
        pwm = 0
        addr = 1


        ServoSetHoming(1, homing_mode)
        ServoSetHoming(2, homing_mode)
        ServoSetHoming(3, homing_mode)
        ServoLoadTraj(1, mode, pos, vel, acc, 0)
       


        'DO ORIGINAL
        'Do


        '    NmcNoOp(1)               'NoOp command to read current module status

        '    still_homing(1) = (NmcGetStat(1) And HOME_IN_PROG)
        '    NmcNoOp(2)               'NoOp command to read current module status

        '    still_homing(2) = (NmcGetStat(2) And HOME_IN_PROG)
        '    NmcNoOp(3)               'NoOp command to read current module status

        '    still_homing(3) = (NmcGetStat(3) And HOME_IN_PROG)

        'Loop While (still_homing(1) Or still_homing(2) Or still_homing(3)) <> 0

        Do


            NmcNoOp(1)               'NoOp command to read current module status

            still_homing(1) = (NmcGetStat(1) And HOME_IN_PROG)
          

        Loop While (still_homing(1)) <> 0

        ServoLoadTraj(2, mode, pos, vel, acc, 0)

        do 
            NmcNoOp(2)               'NoOp command to read current module status

            still_homing(2) = (NmcGetStat(2) And HOME_IN_PROG)
          
        Loop While (still_homing(2)) <> 0

        ServoLoadTraj(3, mode, pos, vel, acc, 0)
        Do
            NmcNoOp(3)               'NoOp command to read current module status

            still_homing(3) = (NmcGetStat(3) And HOME_IN_PROG)
        Loop While (still_homing(3)) <> 0

        ''Loop While still_homing
        ServoStopMotor(1, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(1, AMP_ENABLE Or STOP_ABRUPT)
        'ServoResetPos(1)
        ServoStopMotor(2, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(2, AMP_ENABLE Or STOP_ABRUPT)
        ' ServoResetPos(2)
        ServoStopMotor(3, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(3, AMP_ENABLE Or STOP_ABRUPT)
        ServoResetPos(3)
        NmcReadStatus(1, SEND_POS)  'Read the current position data from the cotnroller
        NmcReadStatus(2, SEND_POS)
        NmcReadStatus(3, SEND_POS)




        rdy = 1

    End Function
    Public Function MovLin()
        EnableLecturaIO = False

        Dim statbyte(3) As Int32
        If ((Xp = XpOld) And (Yp = YpOld) And (Zp = ZpOld)) Then


        Else
            xnew = CInt(Xp)
            ynew = CInt(Yp)
            znew = CInt(Zp)
            puntos = 0
            xxx = XpOld
            yyy = YpOld
            zzz = ZpOld
            dx = xnew - XpOld
            dy = ynew - YpOld
            dz = znew - ZpOld
            If dx < 0 Then
                x_in = -1
            Else
                x_in = 1
            End If
            If dy < 0 Then
                y_in = -1
            Else
                y_in = 1
            End If
            If dz < 0 Then
                z_in = -1
            Else
                z_in = 1
            End If
            Adx = Math.Abs(dx)
            Ady = Math.Abs(dy)
            Adz = Math.Abs(dz)
            dx2 = dx * 2
            dy2 = dy * 2
            dz2 = dz * 2
            If ((Adx >= Ady) And (Adx >= Adz)) Then
                err_1 = dy2 - Adx
                err_2 = dz2 - Adx
                For i As Integer = 1 To Adx
                    If err_1 > 0 Then
                        yyy = yyy + y_in
                        err_1 = err_1 - dx2
                    End If
                    If err_2 > 0 Then
                        zzz = zzz + z_in
                        err_2 = err_2 - dx2
                    End If
                    err_1 = err_1 + dy2
                    err_2 = err_2 + dz2
                    xxx = xxx + x_in


                    TrayBreX(puntos) = xxx
                    TrayBreY(puntos) = yyy
                    TrayBreZ(puntos) = zzz
                    puntos = puntos + 1


                Next
            End If
            If ((Ady > Adx) And (Ady >= Adz)) Then
                err_1 = dx2 - Ady
                err_2 = dz2 - Ady
                For i As Integer = 1 To Ady
                    If (err_1 > 0) Then
                        xxx = xxx + x_in
                        err_1 = err_1 - dy2
                    End If
                    If (err_2 > 0) Then
                        zzz = zzz + z_in
                        err_2 = err_2 - dy2
                    End If
                    err_1 = err_1 + dx2
                    err_2 = err_2 + dz2
                    yyy = yyy + y_in


                    TrayBreX(puntos) = xxx
                    TrayBreY(puntos) = yyy
                    TrayBreZ(puntos) = zzz
                    puntos = puntos + 1
                Next
            End If
            If ((Adz > Adx) And (Adz >= Ady)) Then
                err_1 = dy2 - Adz
                err_2 = dx2 - Adz
                For i As Integer = 1 To Adz
                    If (err_1 > 0) Then
                        yyy = yyy + y_in
                        err_1 = err_1 - dz2
                    End If
                    If (err_2 > 0) Then
                        xxx = xxx + x_in
                        err_2 = err_2 - dz2
                    End If
                    err_1 = err_1 + dy2
                    err_2 = err_2 + dx2
                    zzz = zzz + z_in

                    TrayBreX(puntos) = xxx
                    TrayBreY(puntos) = yyy
                    TrayBreZ(puntos) = zzz
                    puntos = puntos + 1

                Next
            End If

            Dim iter As Integer = 0
            iter = 0
            While iter < (puntos)
                Xp = TrayBreX(iter)
                Yp = TrayBreY(iter)
                Zp = TrayBreZ(iter)
                res = Func_2()
                iter = iter + 1


         
         
                Do
                    NmcNoOp(1)               'NoOp command to read current module status
                    statbyte(1) = NmcGetStat(1) 'Fetch the module status byte
                    NmcNoOp(2)               'NoOp command to read current module status
                    statbyte(2) = NmcGetStat(2) 'Fetch the module status byte
                    NmcNoOp(3)               'NoOp command to read current module status
                    statbyte(3) = NmcGetStat(3) 'Fetch the module status byte
                Loop While ((statbyte(1) And MOVE_DONE) = 0) Or ((statbyte(2) And MOVE_DONE) = 0) Or ((statbyte(3) And MOVE_DONE) = 0)


            End While
            XpOld = xnew
            YpOld = ynew
            ZpOld = znew
        End If

    End Function
    Public Function Func_2() ' 2190
        LectHilo = True
        EnableLecturaIO = False
        Dim ret As Integer = 1
        Dim i As Integer = 0
        Dim j As Integer = 0
        MainMenu.inversekinematics(Xp, Yp, Zp)
        Dim vmax As Double = VelGen
        Dim amax As Double = AcelGen


        TH1pulses(3) = 0
        TH1pulses_ant(3) = 0
        Dim tauxiliar As Double = 0
        '//old
        Dim t As Double
        For i = 0 To 3
            If (Math.Abs(TH1pulses(i) - TH1pulses_ant(i)) > 0) Then
                t = (Math.Abs(TH1pulses(i) - TH1pulses_ant(i)) / vmax)
                If (t > tauxiliar) Then
                    tauxiliar = t
                End If
                t = tauxiliar
                For j = 0 To 3
                    If t = 0 Then
                        vel(j) = 0
                    Else
                        vel(j) = (Math.Abs(TH1pulses(j) - TH1pulses_ant(j)) / t)

                    End If
                    acel(j) = amax

                Next
                'actualizamos angulos de motores
                TH1pulses_ant(0) = TH1pulses(0)
                TH1pulses_ant(1) = TH1pulses(1)
                TH1pulses_ant(2) = TH1pulses(2)
                Pulsos1 = TH1pulses(0)
                Pulsos2 = TH1pulses(1)
                Pulsos3 = TH1pulses(2)
                Moveto(TH1pulses(0), vel(0), acel(0), TH1pulses(1), vel(1), acel(1), TH1pulses(2), vel(2), acel(2))

                XpOld = Xp
                YpOld = Yp
                ZpOld = Zp
            End If
        Next


        '////////////// F I N V1.0
       
    End Function
    Public Function Moveto(ByVal P1 As Integer, ByVal V1 As Integer, ByVal A1 As Integer, ByVal P2 As Integer, ByVal V2 As Integer, ByVal A2 As Integer, ByVal P3 As Integer, ByVal V3 As Integer, ByVal A3 As Integer)
        Dim auxbyte(3) As Int32
        Dim statbyte(3) As Byte
        Calculate_Conversion_Factor2()




        ServoLoadTraj(1, LOAD_POS Or LOAD_VEL Or LOAD_ACC Or ENABLE_SERVO, P1, (V1 * (ConversionFactor1 * ConversionFactorVelAcc1)), (A1 * (ConversionFactor1 * ConversionFactorVelAcc1)), 0)

        ServoLoadTraj(2, LOAD_POS Or LOAD_VEL Or LOAD_ACC Or ENABLE_SERVO, P2, (V2 * (ConversionFactor2 * ConversionFactorVelAcc2)), (A2 * (ConversionFactor2 * ConversionFactorVelAcc2)), 0)

        ServoLoadTraj(3, LOAD_POS Or LOAD_VEL Or LOAD_ACC Or ENABLE_SERVO, P3, (V3 * (ConversionFactor3 * ConversionFactorVelAcc3)), (A3 * (ConversionFactor3 * ConversionFactorVelAcc3)), 0)
        Dim sum As Byte = &H1
        NmcSetGroupAddr(1, &H80, 0)
        NmcSetGroupAddr(2, &H80, 0)
        NmcSetGroupAddr(3, &H80, 0)
        NmcSynchOutput(&H80, &H0)



        Do



            NmcNoOp(1)               'NoOp command to read current module status
            statbyte(1) = NmcGetStat(1) 'Fetch the module status byte
            NmcNoOp(2)               'NoOp command to read current module status
            statbyte(2) = NmcGetStat(2) 'Fetch the module statleeeelus byte

            NmcNoOp(3)               'NoOp command to read current module status
            statbyte(3) = NmcGetStat(3) 'Fetch the module status byte

        Loop While ((statbyte(1) And SLEW_DONE) = 0) Or ((statbyte(2) And SLEW_DONE) = 0) Or ((statbyte(3) And SLEW_DONE) = 0)
        'Loop While ((statbyte(1) And SLEW_DONE) = 0) Or ((statbyte(2) And SLEW_DONE) = 0) Or ((statbyte(3) And SLEW_DONE) = 0)
        'Loop While ((statbyte(1) And MOVE_DONE) = 0) Or ((statbyte(2) And MOVE_DONE) = 0) Or ((statbyte(3) And MOVE_DONE) = 0)




    End Function
    Public Function ActualizarSecuenceGrid(ByVal Caso As Integer)
        If Caso = 1 Then
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim StrSQL As String = "SELECT COMANDO, NUM, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM SECUENCIAS"
            Dim adp As New OleDb.OleDbDataAdapter(StrSQL, conn)
          
            ds.Tables.Add("Tabla")
            adp.Fill(ds.Tables("Tabla"))
              MenuRutinas.Secuencia.DataSource = ds.Tables("Tabla")
        ElseIf Caso = 2 Then
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim StrSQL As String = "SELECT X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM " & MenuRutinas.ComboBox1.SelectedItem & ""

            Dim adp As New OleDb.OleDbDataAdapter(StrSQL, conn)
           
            ds.Tables.Add("Tabla")
            adp.Fill(ds.Tables("Tabla"))

            MenuRutinas.Secuencia.DataSource = ds.Tables("Tabla")


        End If

    End Function
    Public Function MostrarMensaje(ByVal tipo As Integer)
        Select Case tipo
            Case 0
                MainMenu.RichTextBox1.Clear()
            Case 1
                MainMenu.RichTextBox1.AppendText("Conexion establecida" & vbCrLf)
            Case 2
                MainMenu.RichTextBox1.AppendText("Home encontrado" & vbCrLf)
            Case 3
                MainMenu.RichTextBox1.AppendText("Motores Deshabilitados" & vbCrLf)
            Case 4
                MainMenu.RichTextBox1.AppendText("Movimiento A: X= " & CoorX & ", Y= " & CoorY & ", Z= " & CoorZ & vbCrLf)
            Case 5
                MainMenu.RichTextBox1.AppendText("Succion ON" & vbCrLf)
            Case 6
                MainMenu.RichTextBox1.AppendText("Succion OFF" & vbCrLf)
            Case 7
                MainMenu.RichTextBox1.AppendText("Posicion X= " & CoorX & ", Y= " & CoorY & ", Z= " & CoorZ & " guardada" & vbCrLf)
            Case 8
                MainMenu.RichTextBox1.AppendText("Apague, vuelva a encender el robot y reinicie la aplicacion.")
        End Select

    End Function


    Public Function GoHome()
        Dim auxbyte(3) As Int32
        Dim statbyte(3) As Byte
        Dim RestaPulsos(2) As Double
        Dim dicc As New Dictionary(Of Double, Double)
        Dim PosPulseDirectory As New Dictionary(Of Double, Double)
        Dim porcentaje(2) As Double
        Dim k As Integer
        Dim repes As Double = 0.01
        'If ((Math.Abs(TH1pulses(0) - TH1pulses_ant(0)) > 0) And (Math.Abs(TH1pulses(1) - TH1pulses_ant(1)) > 0) And (Math.Abs(TH1pulses(1) - TH1pulses_ant(1)) > 0)) Then

        Dim i As Integer


        'End If
        For i = 0 To 2
            If (Math.Abs(HomePos(i + 1) - TH1pulses_ant(i)) > 0) Then
                RestaPulsos(i) = Math.Abs(HomePos(i + 1) - TH1pulses_ant(i))

            ElseIf (Math.Abs(HomePos(i + 1) - TH1pulses_ant(i)) = 0) Then
                RestaPulsos(i) = 1
            End If
            Try


                dicc.Add(RestaPulsos(i), HomePos(i + 1))
            Catch ex As Exception

                If ex.ToString.Contains("same key") Then

                    RestaPulsos(i) = RestaPulsos(i) + repes
                    dicc.Add(RestaPulsos(i), HomePos(i + 1))
                    repes = repes + 0.01
                End If
            End Try
        Next

        Dim keys As List(Of Double) = dicc.Keys.ToList
        ' Sort the keys.
        keys.Sort()
        keys.Reverse()
        porcentaje(0) = keys(0) / keys(0)
        porcentaje(1) = keys(1) / keys(0)
        porcentaje(2) = keys(2) / keys(0)
        System.Array.Sort(RestaPulsos)
        System.Array.Reverse(RestaPulsos)
        For Each Str As Double In keys
            'MsgBox(Str)
            'MsgBox(dicc.Item(Str))
            'MsgBox(PosPulseDirectory(dicc.Item(Str)))
            repes = 0.01

            Try


                PosPulseDirectory.Add(dicc.Item(Str), porcentaje(k))
            Catch ex As Exception

                If ex.ToString.Contains("same key") Then

                    dicc.Item(Str) = dicc.Item(Str) + repes
                    PosPulseDirectory.Add(dicc.Item(Str), porcentaje(k))
                    repes = repes + 0.01
                End If
            End Try
            '  MsgBox(Str & " " & porcentaje(k))
            k = k + 1
        Next
        Moveto(HomePos(1), (VelGen * PosPulseDirectory.Item(HomePos(1))), (AcelGen * PosPulseDirectory.Item(HomePos(1))), HomePos(2), (VelGen * PosPulseDirectory.Item(HomePos(2))), (AcelGen * PosPulseDirectory.Item(HomePos(2))), HomePos(3), (VelGen * PosPulseDirectory.Item(HomePos(3))), (AcelGen * PosPulseDirectory.Item(HomePos(3))))

        TH1pulses_ant(0) = HomePos(1)
        TH1pulses_ant(1) = HomePos(2)
        TH1pulses_ant(2) = HomePos(3)
    End Function
    Public Function SQLJustExe()
        Try
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            If ex.ToString.Contains("la está utilizando otro usuario") Then
                MenuRutinas.RichTextBox1.AppendText("Cierre la tabla en la base de datos." & vbCrLf)
            Else
                MsgBox(ex.ToString)

            End If





        End Try
    End Function
End Module
