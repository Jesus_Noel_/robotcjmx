﻿Imports System.Net.Sockets
Imports System.Text


Imports System.Management
Imports System.Net.Mime.MediaTypeNames

Imports System.IO
Public Class PIDMenu

    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click
        NmcShutdown()
        conn.Close()
        conn.Open()
        NmcInit(COMPort, 19200)
        IoBitDirOut(4, 6)
        IoClrOutBit(4, 6) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
        'IoBitDirOut(4, 4)
        'IoSetOutBit(4, 4)
        Dim a, b, c, d, f, g, h, j As Boolean
        a = IsNumeric(TextBox9.Text)
        b = IsNumeric(TextBox10.Text)
        c = IsNumeric(TextBox11.Text)
        d = IsNumeric(TextBox12.Text)
        f = IsNumeric(TextBox13.Text)
        g = IsNumeric(TextBox1.Text)
        h = IsNumeric(TextBox2.Text)
        j = IsNumeric(TextBox3.Text)




        If a = True And b = True And c = True And d = True And f = True And g = True Then
            kp01 = TextBox9.Text
            kd01 = TextBox11.Text
            ki01 = TextBox10.Text
            kp02 = TextBox9.Text
            kd02 = TextBox11.Text
            ki02 = TextBox10.Text
            kp03 = TextBox9.Text
            kd03 = TextBox11.Text
            ki03 = TextBox10.Text
            EL = TextBox12.Text
            DC = TextBox13.Text

            VelGen = TextBox130.Text
            AcelGen = TextBox140.Text
            EncoderResolution1 = TextBox1.Text
            EncoderResolution2 = TextBox2.Text
            EncoderResolution3 = TextBox3.Text
            NewResolution1 = EncoderResolution1
            NewResolution2 = EncoderResolution2
            NewResolution3 = EncoderResolution3
            Calculate_Conversion_Factor()

            setGains(1, kp01, kd01, ki01, EL, DC)
            setGains(2, kp02, kd02, ki02, EL, DC)
            setGains(3, kp03, kd03, ki03, EL, DC)
            If RadioButton1.Checked = True Then
                FindHomeBy = 1
            ElseIf RadioButton2.Checked = True Then
                FindHomeBy = 2
            Else
                FindHomeBy = 3
            End If

            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "UPDATE CONFIGURACION SET KP = " & kp01 & ", KI = " & ki01 & ", KD = " & kd01 & ", EL = " & EL & ", DC = " & DC & ", VEL = " & VelGen & ", ACEL = " & AcelGen & ", ENCODERRES1 = " & EncoderResolution1 & ", CONVFACTOR1 = " & ConversionFactor1 & ", ENCODERRES2 = " & EncoderResolution2 & ", CONVFACTOR2 = " & ConversionFactor2 & ", ENCODERRES3 = " & EncoderResolution3 & ", CONVFACTOR3 = " & ConversionFactor3 & ", ENCONTRARHOMEPOR = " & FindHomeBy & " WHERE ID=" & 1


            Try
                dr = cmd.ExecuteReader()
                Button1.Enabled = True


            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            dr.Close()
        Else
        End If



    End Sub
    Public Function setGains(ByVal motor As Integer, ByVal kp As Integer, ByVal kd As Integer, ByVal ki As Integer, ByVal EL As Integer, ByVal DC As Integer)
        ServoSetGain(motor, kp, kd, ki, 0, 128, 255, EL, 1, DC)
        ' ServoSetGain(motor, kp, kd, ki, 0, 255, 0, EL, 1, DC) original
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Hide()
        MainMenu.Show()

    End Sub

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LeerSetup()
        TextBox9.Text = kp01
        TextBox10.Text = ki01
        TextBox11.Text = kd01




        TextBox12.Text = EL
        TextBox13.Text = DC
        TextBox130.Text = VelGen
        TextBox140.Text = AcelGen
        TextBox1.Text = EncoderResolution1
        TextBox2.Text = EncoderResolution2
        TextBox3.Text = EncoderResolution3
        Me.BackColor = Color.FromArgb(10, 42, 30)
        GroupBox7.BackColor = Color.FromArgb(10, 42, 30)
        If FindHomeBy = 1 Then
            RadioButton1.Checked = True
        ElseIf FindHomeBy = 2 Then
            RadioButton2.Checked = True
        ElseIf FindHomeBy = 3 Then
            RadioButton3.Checked = True
        End If
        TextBox4.Text = conn.ConnectionString
    End Sub


   
    Private Sub Button29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button29.Click
        Dim ofd As New OpenFileDialog
        If ofd.ShowDialog = Windows.Forms.DialogResult.OK AndAlso ofd.FileName <> "" Then

            TextBox4.Text = Path.GetFullPath(ofd.FileName)
        End If
    End Sub

    Private Sub Button30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button30.Click
        If My.Computer.FileSystem.FileExists(configpath) Then
            Dim writter As New System.IO.StreamWriter(configpath)
            writter.Write("")
            writter.Write("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & TextBox4.Text & ";Mode= ReadWrite; Persist Security Info=False")
            writter.Close()
            conn.Close()
            conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & TextBox4.Text & ";Mode= ReadWrite; Persist Security Info=False"
            ActualizarDataGrid(1)
        End If
    End Sub
End Class