﻿Public Module Variables
    Public Rot(50) As Double
    Public aux(50) As Double
    Public aux1(50) As Double
    Public aux2(50) As Double
    Public conn As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Application.StartupPath & "\DB\Pos.accdb;Mode= ReadWrite; Persist Security Info=False")
    Public cmd As New OleDb.OleDbCommand
    Public dr As OleDb.OleDbDataReader         'Almacena la informacion obtenida de la BD
    Public sql As String
    Public conn2 As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Application.StartupPath & "\DB\Pos.accdb;Mode= ReadWrite; Persist Security Info=False")
    Public cmd2 As New OleDb.OleDbCommand
    Public dr2 As OleDb.OleDbDataReader         'Almacena la informacion obtenida de la BD
    Public sql2 As String

    Public NumOp As Integer = 0
    Public Indx As Integer = 0

    Public VelGen As Integer = 50000
    Public AcelGen As Integer = 50
    

    Public Vel1 As Integer = 50000
    Public Acel1 As Integer = 50
    Public Vel2 As Integer = 50000
    Public Acel2 As Integer = 50
    Public Vel3 As Integer = 50000
    Public Acel3 As Integer = 50
    Public Pulsos1 As Double
    Public Pulsos2 As Double
    Public Pulsos3 As Double

    Public kp01 As Integer
    Public kd01 As Integer
    Public ki01 As Integer
    Public kp02 As Integer
    Public kd02 As Integer
    Public ki02 As Integer
    Public kp03 As Integer
    Public kd03 As Integer
    Public ki03 As Integer
    Public EL, EL2, EL3 As Integer
    Public DC, DC2, DC3 As Integer



    Public PulsosCon(4) As Double
    Public RadCon(4) As Double
    Public EncoderResolution1, EncoderResolution2, EncoderResolution3 As Integer
    Public Const ReferenceResolution As Integer = 100
    Public ConversionFactor1, ConversionFactor2, ConversionFactor3 As Double
    Public ConversionFactorVelAcc1, ConversionFactorVelAcc2, ConversionFactorVelAcc3 As Double
    Public NewResolution1, NewResolution2, NewResolution3 As Integer
    Public pos(3) As Int32
    Public Pf(4) As Double ' punto final
    Public Cf(16) As Double
    Public Ti(16) As Double
    Public Phi(4) As Double
    Public TH1(4) As Double


    Public TH1ant(4) As Double
    Public Th1deg(4) As Double
    Public TH1pulses(4) As Double
    Public TH1pulses_ant(4) As Double
    'dimenciones del robot 
    Public a(4) As Double
    Public b(4) As Double
    Public h(4) As Double
    Public H1(4) As Double
    'formulas (?)..
    Public M(4) As Double
    Public N(4) As Double
    Public Q(4) As Double
    Public R(4) As Double
    Public functions As New nmspace.maclass
    Public Xp As Double = 0
    Public Yp As Double = 0
    Public Zp As Double = 0
    Public XpOld As Double = 0
    Public YpOld As Double = 0
    Public ZpOld As Double = 0
    Public res As Long
    Public interacion As Integer
    Public addr As Byte
    Public antihome3, antihome2, antihome1 As Integer
    Public rdy As Integer


    Public LimZ As Integer = -230
    Public LimX As Integer
    Public LimY As Integer
    Public CoorIniX As Integer = 0
    Public CoorIniY As Integer = 0
    Public CoorIniZ As Integer = 0
    Public CoorX, CoorY, CoorZ As Integer
    'Bresenham variables
    Public contador As Integer = 0
    Public xxx As Integer = 0
    Public yyy As Integer = 0
    Public zzz As Integer = 0
    Public xnew As Integer = 0
    Public ynew As Integer = 0
    Public znew As Integer = 0
    Public dx As Integer = 0
    Public dy As Integer = 0
    Public dz As Integer = 0
    Public dx2 As Integer = 0
    Public dy2 As Integer = 0
    Public dz2 As Integer = 0
    Public x_in As Integer = 0
    Public y_in As Integer = 0
    Public z_in As Integer = 0
    Public Adx As Integer = 0
    Public Ady As Integer = 0
    Public Adz As Integer = 0
    Public err_1 As Integer = 0
    Public err_2 As Integer = 0
    Public puntos As Integer
    Public TrayBreX(5000) As Integer
    Public TrayBreY(5000) As Integer
    Public TrayBreZ(5000) As Integer
    Public vel(4) As Double
    Public acel(4) As Double
    Public MovimientoLienal As Boolean
    Public frm1 As New MainMenu
    Public frm2 As New PIDMenu
    Public frm3 As New MenuRutinas
    Public lastcommand As Integer
    Public LastDeleted, LastCreated, LastModified As Integer
    Public ReadEnabled As Boolean
    Public ConstanteA, ConstanteB, ConstanteC As Integer
    Public ReadEnabelStop, EnableSecuence, EnableSecuence2, EnableLecturaIO, EnableTeachMode, EnablePosRead As Boolean
    Public BufferInUse As Boolean

    Public CalculatedPosition1(1000) As Double
    Public CalculatedPosition2(1000) As Double
    Public CalculatedPosition3(1000) As Double
    Public CalculatedVel1(1000) As Double
    Public CalculatedVel2(1000) As Double
    Public CalculatedVel3(1000) As Double
    Public CalculatedAccel1(1000) As Double
    Public CalculatedAccel2(1000) As Double
    Public CalculatedAccel3(1000) As Double
    Public IsSucc(1000) As Integer
    Public Waiting(1000) As Integer

    Public FirstInteration As Boolean
    Public COMPort As String
    Public LectHilo As Boolean

    Public DivisorKine As Double
    Public HomePos(3) As Integer
    Public FindHomeBy As Integer = 1
    Public Posvar As String = Application.StartupPath & "\CJMXSOFT.txt"
    Public configpath As String = Application.StartupPath & "\config.txt"


    Public Radianes_Motor(3) As Double

End Module
