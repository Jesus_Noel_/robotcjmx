﻿Module Practicas
    Public Function AngulosToRad(ByVal Angulo As Double)
        Dim pi As Double = 3.1416
        Dim Rad As Double

        Rad = (Angulo / 180) * pi


        Return Rad
    End Function
End Module
