﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainMenu))
        Me.Boton_Inicio = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button_Leer_Pos = New System.Windows.Forms.Button()
        Me.Button_Deshabilitar = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox_X = New System.Windows.Forms.TextBox()
        Me.TextBox_Y = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox_Z = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button_Mov_PTP = New System.Windows.Forms.Button()
        Me.Button_New_Hombe = New System.Windows.Forms.Button()
        Me.Button_Z_UP = New System.Windows.Forms.Button()
        Me.Button_Z_DOWN = New System.Windows.Forms.Button()
        Me.ComboBox_Resolucion = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PosDataSet = New CJMXSOFT.PosDataSet()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button_Down_Right = New System.Windows.Forms.Button()
        Me.Button_Down_Left = New System.Windows.Forms.Button()
        Me.Button_UP_Right = New System.Windows.Forms.Button()
        Me.Button_Up_left = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Button_Right = New System.Windows.Forms.Button()
        Me.Button_left = New System.Windows.Forms.Button()
        Me.Button_UP = New System.Windows.Forms.Button()
        Me.Button_Down = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button_Mov_Ang = New System.Windows.Forms.Button()
        Me.Button_Go_Home = New System.Windows.Forms.Button()
        Me.Button_Habilitar = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox_Ang1 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox_Ang2 = New System.Windows.Forms.TextBox()
        Me.Textbox_Ang3 = New System.Windows.Forms.TextBox()
        Me.Button_Mov_Lin = New System.Windows.Forms.Button()
        Me.Button_Guardar_Pos = New System.Windows.Forms.Button()
        Me.PosDataSet1 = New CJMXSOFT.PosDataSet1()
        Me.PosicionesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PosicionesTableAdapter = New CJMXSOFT.PosDataSet1TableAdapters.PosicionesTableAdapter()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button_Eliminar_Pos = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.COMS = New System.Windows.Forms.ComboBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.TextBoxDelay = New System.Windows.Forms.TextBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Button_Config = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBox_Acel = New System.Windows.Forms.TextBox()
        Me.TextBox_Vel = New System.Windows.Forms.TextBox()
        Me.Button_Rutnas = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Mov_Coordenadas = New System.Windows.Forms.RadioButton()
        Me.Mov_Angular = New System.Windows.Forms.RadioButton()
        Me.Current_Pos = New System.ComponentModel.BackgroundWorker()
        CType(Me.PosDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PosDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PosicionesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Boton_Inicio
        '
        Me.Boton_Inicio.BackColor = System.Drawing.Color.PaleGreen
        Me.Boton_Inicio.Enabled = False
        Me.Boton_Inicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Boton_Inicio.Location = New System.Drawing.Point(14, 71)
        Me.Boton_Inicio.Name = "Boton_Inicio"
        Me.Boton_Inicio.Size = New System.Drawing.Size(105, 48)
        Me.Boton_Inicio.TabIndex = 0
        Me.Boton_Inicio.Text = "Conectar"
        Me.Boton_Inicio.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(13, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "x"
        '
        'Button_Leer_Pos
        '
        Me.Button_Leer_Pos.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Leer_Pos.ForeColor = System.Drawing.Color.White
        Me.Button_Leer_Pos.Location = New System.Drawing.Point(13, 227)
        Me.Button_Leer_Pos.Name = "Button_Leer_Pos"
        Me.Button_Leer_Pos.Size = New System.Drawing.Size(108, 50)
        Me.Button_Leer_Pos.TabIndex = 7
        Me.Button_Leer_Pos.Text = "Leer Pos"
        Me.Button_Leer_Pos.UseVisualStyleBackColor = False
        '
        'Button_Deshabilitar
        '
        Me.Button_Deshabilitar.BackColor = System.Drawing.Color.Moccasin
        Me.Button_Deshabilitar.Enabled = False
        Me.Button_Deshabilitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_Deshabilitar.Location = New System.Drawing.Point(129, 71)
        Me.Button_Deshabilitar.Name = "Button_Deshabilitar"
        Me.Button_Deshabilitar.Size = New System.Drawing.Size(112, 51)
        Me.Button_Deshabilitar.TabIndex = 10
        Me.Button_Deshabilitar.Text = "Deshabilitar"
        Me.Button_Deshabilitar.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 656)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 36)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Desconectado"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox_X
        '
        Me.TextBox_X.Enabled = False
        Me.TextBox_X.Location = New System.Drawing.Point(78, 31)
        Me.TextBox_X.Name = "TextBox_X"
        Me.TextBox_X.Size = New System.Drawing.Size(116, 24)
        Me.TextBox_X.TabIndex = 13
        Me.TextBox_X.Text = "0"
        Me.TextBox_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox_Y
        '
        Me.TextBox_Y.Enabled = False
        Me.TextBox_Y.Location = New System.Drawing.Point(78, 74)
        Me.TextBox_Y.Name = "TextBox_Y"
        Me.TextBox_Y.Size = New System.Drawing.Size(116, 24)
        Me.TextBox_Y.TabIndex = 15
        Me.TextBox_Y.Text = "0"
        Me.TextBox_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(13, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 18)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "y"
        '
        'TextBox_Z
        '
        Me.TextBox_Z.Enabled = False
        Me.TextBox_Z.Location = New System.Drawing.Point(78, 121)
        Me.TextBox_Z.Name = "TextBox_Z"
        Me.TextBox_Z.Size = New System.Drawing.Size(116, 24)
        Me.TextBox_Z.TabIndex = 17
        Me.TextBox_Z.Text = "-500"
        Me.TextBox_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(13, 124)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 18)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "z"
        '
        'Button_Mov_PTP
        '
        Me.Button_Mov_PTP.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Mov_PTP.Enabled = False
        Me.Button_Mov_PTP.ForeColor = System.Drawing.Color.White
        Me.Button_Mov_PTP.Location = New System.Drawing.Point(16, 157)
        Me.Button_Mov_PTP.Name = "Button_Mov_PTP"
        Me.Button_Mov_PTP.Size = New System.Drawing.Size(100, 58)
        Me.Button_Mov_PTP.TabIndex = 18
        Me.Button_Mov_PTP.Text = "MovPTP"
        Me.Button_Mov_PTP.UseVisualStyleBackColor = False
        '
        'Button_New_Hombe
        '
        Me.Button_New_Hombe.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_New_Hombe.Enabled = False
        Me.Button_New_Hombe.ForeColor = System.Drawing.Color.White
        Me.Button_New_Hombe.Location = New System.Drawing.Point(228, 91)
        Me.Button_New_Hombe.Name = "Button_New_Hombe"
        Me.Button_New_Hombe.Size = New System.Drawing.Size(105, 55)
        Me.Button_New_Hombe.TabIndex = 19
        Me.Button_New_Hombe.Text = "New Home"
        Me.Button_New_Hombe.UseVisualStyleBackColor = False
        '
        'Button_Z_UP
        '
        Me.Button_Z_UP.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Z_UP.ForeColor = System.Drawing.Color.White
        Me.Button_Z_UP.Location = New System.Drawing.Point(175, 121)
        Me.Button_Z_UP.Name = "Button_Z_UP"
        Me.Button_Z_UP.Size = New System.Drawing.Size(58, 43)
        Me.Button_Z_UP.TabIndex = 25
        Me.Button_Z_UP.Text = "Z+"
        Me.Button_Z_UP.UseVisualStyleBackColor = False
        '
        'Button_Z_DOWN
        '
        Me.Button_Z_DOWN.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Z_DOWN.ForeColor = System.Drawing.Color.White
        Me.Button_Z_DOWN.Location = New System.Drawing.Point(175, 170)
        Me.Button_Z_DOWN.Name = "Button_Z_DOWN"
        Me.Button_Z_DOWN.Size = New System.Drawing.Size(58, 45)
        Me.Button_Z_DOWN.TabIndex = 26
        Me.Button_Z_DOWN.Text = "Z-"
        Me.Button_Z_DOWN.UseVisualStyleBackColor = False
        '
        'ComboBox_Resolucion
        '
        Me.ComboBox_Resolucion.FormattingEnabled = True
        Me.ComboBox_Resolucion.Items.AddRange(New Object() {"1", "5", "10", "20", "50", "100", "200"})
        Me.ComboBox_Resolucion.Location = New System.Drawing.Point(38, 49)
        Me.ComboBox_Resolucion.Name = "ComboBox_Resolucion"
        Me.ComboBox_Resolucion.Size = New System.Drawing.Size(121, 26)
        Me.ComboBox_Resolucion.TabIndex = 27
        Me.ComboBox_Resolucion.Text = "10"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(18, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 18)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Resolución"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.DarkOrange
        Me.Label7.Location = New System.Drawing.Point(10, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(19, 18)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "X"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.DarkOrange
        Me.Label9.Location = New System.Drawing.Point(147, 45)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(18, 18)
        Me.Label9.TabIndex = 32
        Me.Label9.Text = "Y"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.DarkOrange
        Me.Label11.Location = New System.Drawing.Point(277, 45)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(18, 18)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Z"
        '
        'PosDataSet
        '
        Me.PosDataSet.DataSetName = "PosDataSet"
        Me.PosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.LightGray
        Me.GroupBox2.Location = New System.Drawing.Point(247, 72)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(394, 80)
        Me.GroupBox2.TabIndex = 39
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Posicion"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(7, 45)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 18)
        Me.Label14.TabIndex = 36
        Me.Label14.Text = "X"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(60, 45)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(74, 18)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "Posicion"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.DarkOrange
        Me.Label16.Location = New System.Drawing.Point(144, 45)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(18, 18)
        Me.Label16.TabIndex = 38
        Me.Label16.Text = "Y"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(307, 45)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(74, 18)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Posicion"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(180, 45)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 18)
        Me.Label18.TabIndex = 39
        Me.Label18.Text = "Posicion"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(274, 45)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(18, 18)
        Me.Label19.TabIndex = 40
        Me.Label19.Text = "Z"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.GroupBox3.Controls.Add(Me.Button_Down_Right)
        Me.GroupBox3.Controls.Add(Me.Button_Down_Left)
        Me.GroupBox3.Controls.Add(Me.Button_UP_Right)
        Me.GroupBox3.Controls.Add(Me.Button_Up_left)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.ComboBox_Resolucion)
        Me.GroupBox3.Controls.Add(Me.Button_Right)
        Me.GroupBox3.Controls.Add(Me.Button_left)
        Me.GroupBox3.Controls.Add(Me.Button_UP)
        Me.GroupBox3.Controls.Add(Me.Button_Down)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Button_Z_UP)
        Me.GroupBox3.Controls.Add(Me.Button_Z_DOWN)
        Me.GroupBox3.Enabled = False
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.LightGray
        Me.GroupBox3.Location = New System.Drawing.Point(380, 179)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(253, 231)
        Me.GroupBox3.TabIndex = 40
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Movimiento Manual"
        '
        'Button_Down_Right
        '
        Me.Button_Down_Right.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Down_Right.BackgroundImage = CType(resources.GetObject("Button_Down_Right.BackgroundImage"), System.Drawing.Image)
        Me.Button_Down_Right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_Down_Right.Location = New System.Drawing.Point(116, 172)
        Me.Button_Down_Right.Name = "Button_Down_Right"
        Me.Button_Down_Right.Size = New System.Drawing.Size(52, 43)
        Me.Button_Down_Right.TabIndex = 42
        Me.Button_Down_Right.UseVisualStyleBackColor = False
        '
        'Button_Down_Left
        '
        Me.Button_Down_Left.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Down_Left.BackgroundImage = CType(resources.GetObject("Button_Down_Left.BackgroundImage"), System.Drawing.Image)
        Me.Button_Down_Left.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_Down_Left.Location = New System.Drawing.Point(6, 172)
        Me.Button_Down_Left.Name = "Button_Down_Left"
        Me.Button_Down_Left.Size = New System.Drawing.Size(52, 43)
        Me.Button_Down_Left.TabIndex = 41
        Me.Button_Down_Left.UseVisualStyleBackColor = False
        '
        'Button_UP_Right
        '
        Me.Button_UP_Right.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_UP_Right.BackgroundImage = CType(resources.GetObject("Button_UP_Right.BackgroundImage"), System.Drawing.Image)
        Me.Button_UP_Right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_UP_Right.Location = New System.Drawing.Point(117, 81)
        Me.Button_UP_Right.Name = "Button_UP_Right"
        Me.Button_UP_Right.Size = New System.Drawing.Size(52, 43)
        Me.Button_UP_Right.TabIndex = 40
        Me.Button_UP_Right.UseVisualStyleBackColor = False
        '
        'Button_Up_left
        '
        Me.Button_Up_left.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Up_left.BackgroundImage = CType(resources.GetObject("Button_Up_left.BackgroundImage"), System.Drawing.Image)
        Me.Button_Up_left.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_Up_left.Location = New System.Drawing.Point(6, 81)
        Me.Button_Up_left.Name = "Button_Up_left"
        Me.Button_Up_left.Size = New System.Drawing.Size(52, 43)
        Me.Button_Up_left.TabIndex = 39
        Me.Button_Up_left.UseVisualStyleBackColor = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(183, 49)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(36, 18)
        Me.Label20.TabIndex = 38
        Me.Label20.Text = "mm"
        '
        'Button_Right
        '
        Me.Button_Right.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Right.BackgroundImage = CType(resources.GetObject("Button_Right.BackgroundImage"), System.Drawing.Image)
        Me.Button_Right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_Right.Location = New System.Drawing.Point(116, 127)
        Me.Button_Right.Name = "Button_Right"
        Me.Button_Right.Size = New System.Drawing.Size(52, 43)
        Me.Button_Right.TabIndex = 21
        Me.Button_Right.UseVisualStyleBackColor = False
        '
        'Button_left
        '
        Me.Button_left.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_left.BackgroundImage = CType(resources.GetObject("Button_left.BackgroundImage"), System.Drawing.Image)
        Me.Button_left.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_left.Location = New System.Drawing.Point(6, 127)
        Me.Button_left.Name = "Button_left"
        Me.Button_left.Size = New System.Drawing.Size(52, 43)
        Me.Button_left.TabIndex = 22
        Me.Button_left.UseVisualStyleBackColor = False
        '
        'Button_UP
        '
        Me.Button_UP.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_UP.BackgroundImage = CType(resources.GetObject("Button_UP.BackgroundImage"), System.Drawing.Image)
        Me.Button_UP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_UP.Location = New System.Drawing.Point(61, 81)
        Me.Button_UP.Name = "Button_UP"
        Me.Button_UP.Size = New System.Drawing.Size(52, 43)
        Me.Button_UP.TabIndex = 23
        Me.Button_UP.UseVisualStyleBackColor = False
        '
        'Button_Down
        '
        Me.Button_Down.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Down.BackgroundImage = CType(resources.GetObject("Button_Down.BackgroundImage"), System.Drawing.Image)
        Me.Button_Down.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button_Down.Location = New System.Drawing.Point(59, 172)
        Me.Button_Down.Name = "Button_Down"
        Me.Button_Down.Size = New System.Drawing.Size(52, 43)
        Me.Button_Down.TabIndex = 24
        Me.Button_Down.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.GroupBox4.Controls.Add(Me.Button_Mov_Ang)
        Me.GroupBox4.Controls.Add(Me.Button_Go_Home)
        Me.GroupBox4.Controls.Add(Me.Button_Habilitar)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.TextBox_Ang1)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.TextBox_Y)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.TextBox_Ang2)
        Me.GroupBox4.Controls.Add(Me.Textbox_Ang3)
        Me.GroupBox4.Controls.Add(Me.TextBox_X)
        Me.GroupBox4.Controls.Add(Me.Button_Mov_Lin)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.Button_New_Hombe)
        Me.GroupBox4.Controls.Add(Me.TextBox_Z)
        Me.GroupBox4.Controls.Add(Me.Button_Mov_PTP)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.Color.LightGray
        Me.GroupBox4.Location = New System.Drawing.Point(7, 183)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(361, 231)
        Me.GroupBox4.TabIndex = 41
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Movimiento Por Coordenadas"
        '
        'Button_Mov_Ang
        '
        Me.Button_Mov_Ang.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Mov_Ang.Enabled = False
        Me.Button_Mov_Ang.ForeColor = System.Drawing.Color.White
        Me.Button_Mov_Ang.Location = New System.Drawing.Point(41, 157)
        Me.Button_Mov_Ang.Name = "Button_Mov_Ang"
        Me.Button_Mov_Ang.Size = New System.Drawing.Size(100, 58)
        Me.Button_Mov_Ang.TabIndex = 61
        Me.Button_Mov_Ang.Text = "MovAng"
        Me.Button_Mov_Ang.UseVisualStyleBackColor = False
        Me.Button_Mov_Ang.Visible = False
        '
        'Button_Go_Home
        '
        Me.Button_Go_Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Go_Home.Enabled = False
        Me.Button_Go_Home.ForeColor = System.Drawing.Color.White
        Me.Button_Go_Home.Location = New System.Drawing.Point(228, 160)
        Me.Button_Go_Home.Name = "Button_Go_Home"
        Me.Button_Go_Home.Size = New System.Drawing.Size(105, 55)
        Me.Button_Go_Home.TabIndex = 60
        Me.Button_Go_Home.Text = "Go Home"
        Me.Button_Go_Home.UseVisualStyleBackColor = False
        '
        'Button_Habilitar
        '
        Me.Button_Habilitar.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Habilitar.Enabled = False
        Me.Button_Habilitar.ForeColor = System.Drawing.Color.White
        Me.Button_Habilitar.Location = New System.Drawing.Point(228, 23)
        Me.Button_Habilitar.Name = "Button_Habilitar"
        Me.Button_Habilitar.Size = New System.Drawing.Size(105, 54)
        Me.Button_Habilitar.TabIndex = 59
        Me.Button_Habilitar.Text = "Habilitar"
        Me.Button_Habilitar.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Enabled = False
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(13, 34)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 18)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Motor 1"
        Me.Label6.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Enabled = False
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(13, 77)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 18)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Motor 2"
        Me.Label12.Visible = False
        '
        'TextBox_Ang1
        '
        Me.TextBox_Ang1.Enabled = False
        Me.TextBox_Ang1.Location = New System.Drawing.Point(90, 31)
        Me.TextBox_Ang1.Name = "TextBox_Ang1"
        Me.TextBox_Ang1.Size = New System.Drawing.Size(116, 24)
        Me.TextBox_Ang1.TabIndex = 15
        Me.TextBox_Ang1.Text = "0"
        Me.TextBox_Ang1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox_Ang1.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Enabled = False
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(17, 125)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 18)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Motor 3"
        Me.Label13.Visible = False
        '
        'TextBox_Ang2
        '
        Me.TextBox_Ang2.Enabled = False
        Me.TextBox_Ang2.Location = New System.Drawing.Point(90, 74)
        Me.TextBox_Ang2.Name = "TextBox_Ang2"
        Me.TextBox_Ang2.Size = New System.Drawing.Size(116, 24)
        Me.TextBox_Ang2.TabIndex = 13
        Me.TextBox_Ang2.Text = "0"
        Me.TextBox_Ang2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox_Ang2.Visible = False
        '
        'Textbox_Ang3
        '
        Me.Textbox_Ang3.Enabled = False
        Me.Textbox_Ang3.Location = New System.Drawing.Point(90, 122)
        Me.Textbox_Ang3.Name = "Textbox_Ang3"
        Me.Textbox_Ang3.Size = New System.Drawing.Size(116, 24)
        Me.Textbox_Ang3.TabIndex = 17
        Me.Textbox_Ang3.Text = "0"
        Me.Textbox_Ang3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Textbox_Ang3.Visible = False
        '
        'Button_Mov_Lin
        '
        Me.Button_Mov_Lin.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Mov_Lin.Enabled = False
        Me.Button_Mov_Lin.ForeColor = System.Drawing.Color.White
        Me.Button_Mov_Lin.Location = New System.Drawing.Point(122, 159)
        Me.Button_Mov_Lin.Name = "Button_Mov_Lin"
        Me.Button_Mov_Lin.Size = New System.Drawing.Size(100, 58)
        Me.Button_Mov_Lin.TabIndex = 58
        Me.Button_Mov_Lin.Text = "MovLin"
        Me.Button_Mov_Lin.UseVisualStyleBackColor = False
        '
        'Button_Guardar_Pos
        '
        Me.Button_Guardar_Pos.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Guardar_Pos.ForeColor = System.Drawing.Color.White
        Me.Button_Guardar_Pos.Location = New System.Drawing.Point(127, 227)
        Me.Button_Guardar_Pos.Name = "Button_Guardar_Pos"
        Me.Button_Guardar_Pos.Size = New System.Drawing.Size(115, 50)
        Me.Button_Guardar_Pos.TabIndex = 42
        Me.Button_Guardar_Pos.Text = "Guardar Pos"
        Me.Button_Guardar_Pos.UseVisualStyleBackColor = False
        '
        'PosDataSet1
        '
        Me.PosDataSet1.DataSetName = "PosDataSet1"
        Me.PosDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PosicionesBindingSource
        '
        Me.PosicionesBindingSource.DataMember = "Posiciones"
        Me.PosicionesBindingSource.DataSource = Me.PosDataSet1
        '
        'PosicionesTableAdapter
        '
        Me.PosicionesTableAdapter.ClearBeforeFill = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 19)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(494, 187)
        Me.DataGridView1.TabIndex = 43
        '
        'Button_Eliminar_Pos
        '
        Me.Button_Eliminar_Pos.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Eliminar_Pos.ForeColor = System.Drawing.Color.White
        Me.Button_Eliminar_Pos.Location = New System.Drawing.Point(248, 227)
        Me.Button_Eliminar_Pos.Name = "Button_Eliminar_Pos"
        Me.Button_Eliminar_Pos.Size = New System.Drawing.Size(126, 50)
        Me.Button_Eliminar_Pos.TabIndex = 44
        Me.Button_Eliminar_Pos.Text = "Eliminar Pos"
        Me.Button_Eliminar_Pos.UseVisualStyleBackColor = False
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.Color.Moccasin
        Me.Button16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Button16.Location = New System.Drawing.Point(1022, 666)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(147, 51)
        Me.Button16.TabIndex = 45
        Me.Button16.Text = "Salir"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.DataGridView1)
        Me.GroupBox5.Controls.Add(Me.Button_Eliminar_Pos)
        Me.GroupBox5.Controls.Add(Me.Button_Guardar_Pos)
        Me.GroupBox5.Controls.Add(Me.Button_Leer_Pos)
        Me.GroupBox5.Enabled = False
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.LightGray
        Me.GroupBox5.Location = New System.Drawing.Point(653, 72)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(516, 316)
        Me.GroupBox5.TabIndex = 57
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Posiciones Capturadas"
        '
        'GroupBox11
        '
        Me.GroupBox11.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.GroupBox11.Controls.Add(Me.RichTextBox1)
        Me.GroupBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox11.ForeColor = System.Drawing.Color.LightGray
        Me.GroupBox11.Location = New System.Drawing.Point(660, 422)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(509, 221)
        Me.GroupBox11.TabIndex = 83
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Mensajes"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(6, 25)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(497, 190)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'Label22
        '
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.LightGray
        Me.Label22.Location = New System.Drawing.Point(901, 9)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(279, 59)
        Me.Label22.TabIndex = 84
        Me.Label22.Text = "CJMXSOFT"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'COMS
        '
        Me.COMS.FormattingEnabled = True
        Me.COMS.Location = New System.Drawing.Point(129, 131)
        Me.COMS.Name = "COMS"
        Me.COMS.Size = New System.Drawing.Size(105, 21)
        Me.COMS.TabIndex = 86
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label34.Location = New System.Drawing.Point(21, 127)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(88, 25)
        Me.Label34.TabIndex = 87
        Me.Label34.Text = "Puerto:"
        '
        'GroupBox9
        '
        Me.GroupBox9.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.GroupBox9.Controls.Add(Me.GroupBox12)
        Me.GroupBox9.Controls.Add(Me.Button_Config)
        Me.GroupBox9.Controls.Add(Me.GroupBox6)
        Me.GroupBox9.Controls.Add(Me.GroupBox8)
        Me.GroupBox9.Controls.Add(Me.Button_Rutnas)
        Me.GroupBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox9.ForeColor = System.Drawing.Color.LightGray
        Me.GroupBox9.Location = New System.Drawing.Point(7, 433)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(626, 227)
        Me.GroupBox9.TabIndex = 89
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Opciones"
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.Label8)
        Me.GroupBox12.Controls.Add(Me.Label23)
        Me.GroupBox12.Controls.Add(Me.RadioButton6)
        Me.GroupBox12.Controls.Add(Me.RadioButton7)
        Me.GroupBox12.Controls.Add(Me.TextBoxDelay)
        Me.GroupBox12.Controls.Add(Me.CheckBox1)
        Me.GroupBox12.Location = New System.Drawing.Point(356, 100)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(252, 103)
        Me.GroupBox12.TabIndex = 4
        Me.GroupBox12.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(53, 29)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(69, 18)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "Succión"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label23.Location = New System.Drawing.Point(211, 58)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(31, 18)
        Me.Label23.TabIndex = 60
        Me.Label23.Text = "ms"
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton6.Location = New System.Drawing.Point(23, 58)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton6.TabIndex = 55
        Me.RadioButton6.Text = "ON"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton7.Location = New System.Drawing.Point(87, 58)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(59, 22)
        Me.RadioButton7.TabIndex = 56
        Me.RadioButton7.Text = "OFF"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'TextBoxDelay
        '
        Me.TextBoxDelay.Location = New System.Drawing.Point(167, 55)
        Me.TextBoxDelay.Name = "TextBoxDelay"
        Me.TextBoxDelay.Size = New System.Drawing.Size(38, 24)
        Me.TextBoxDelay.TabIndex = 59
        Me.TextBoxDelay.Text = "250"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CheckBox1.Location = New System.Drawing.Point(167, 27)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(69, 22)
        Me.CheckBox1.TabIndex = 58
        Me.CheckBox1.Text = "Delay"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Button_Config
        '
        Me.Button_Config.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Config.ForeColor = System.Drawing.Color.Black
        Me.Button_Config.Location = New System.Drawing.Point(78, 22)
        Me.Button_Config.Name = "Button_Config"
        Me.Button_Config.Size = New System.Drawing.Size(128, 72)
        Me.Button_Config.TabIndex = 75
        Me.Button_Config.Text = "Setup"
        Me.Button_Config.UseVisualStyleBackColor = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.RadioButton9)
        Me.GroupBox6.Controls.Add(Me.RadioButton8)
        Me.GroupBox6.ForeColor = System.Drawing.Color.LightGray
        Me.GroupBox6.Location = New System.Drawing.Point(436, 20)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(172, 71)
        Me.GroupBox6.TabIndex = 61
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Movimiento"
        Me.GroupBox6.Visible = False
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = True
        Me.RadioButton9.Checked = True
        Me.RadioButton9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton9.Location = New System.Drawing.Point(13, 27)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(58, 22)
        Me.RadioButton9.TabIndex = 60
        Me.RadioButton9.TabStop = True
        Me.RadioButton9.Text = "PTP"
        Me.RadioButton9.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton8.Location = New System.Drawing.Point(92, 27)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(70, 22)
        Me.RadioButton8.TabIndex = 59
        Me.RadioButton8.Text = "Lineal"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Label21)
        Me.GroupBox8.Controls.Add(Me.Label10)
        Me.GroupBox8.Controls.Add(Me.TextBox_Acel)
        Me.GroupBox8.Controls.Add(Me.TextBox_Vel)
        Me.GroupBox8.Location = New System.Drawing.Point(19, 100)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(333, 103)
        Me.GroupBox8.TabIndex = 62
        Me.GroupBox8.TabStop = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(203, 25)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(96, 18)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Aceleracion"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(47, 25)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(81, 18)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Velocidad"
        '
        'TextBox_Acel
        '
        Me.TextBox_Acel.Location = New System.Drawing.Point(199, 60)
        Me.TextBox_Acel.Name = "TextBox_Acel"
        Me.TextBox_Acel.Size = New System.Drawing.Size(100, 24)
        Me.TextBox_Acel.TabIndex = 1
        Me.TextBox_Acel.Text = "50"
        Me.TextBox_Acel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox_Vel
        '
        Me.TextBox_Vel.Location = New System.Drawing.Point(36, 60)
        Me.TextBox_Vel.Name = "TextBox_Vel"
        Me.TextBox_Vel.Size = New System.Drawing.Size(100, 24)
        Me.TextBox_Vel.TabIndex = 0
        Me.TextBox_Vel.Text = "50000"
        Me.TextBox_Vel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button_Rutnas
        '
        Me.Button_Rutnas.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button_Rutnas.Enabled = False
        Me.Button_Rutnas.ForeColor = System.Drawing.Color.Black
        Me.Button_Rutnas.Location = New System.Drawing.Point(250, 22)
        Me.Button_Rutnas.Name = "Button_Rutnas"
        Me.Button_Rutnas.Size = New System.Drawing.Size(128, 72)
        Me.Button_Rutnas.TabIndex = 76
        Me.Button_Rutnas.Text = "Secuencias"
        Me.Button_Rutnas.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.CJMXSOFT.My.Resources.Resources.robotcjmx_gray_version
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Location = New System.Drawing.Point(-29, 9)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(311, 56)
        Me.PictureBox1.TabIndex = 88
        Me.PictureBox1.TabStop = False
        '
        'Mov_Coordenadas
        '
        Me.Mov_Coordenadas.AutoSize = True
        Me.Mov_Coordenadas.Checked = True
        Me.Mov_Coordenadas.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Mov_Coordenadas.Location = New System.Drawing.Point(58, 160)
        Me.Mov_Coordenadas.Name = "Mov_Coordenadas"
        Me.Mov_Coordenadas.Size = New System.Drawing.Size(90, 17)
        Me.Mov_Coordenadas.TabIndex = 91
        Me.Mov_Coordenadas.TabStop = True
        Me.Mov_Coordenadas.Text = "RadioButton1"
        Me.Mov_Coordenadas.UseVisualStyleBackColor = True
        '
        'Mov_Angular
        '
        Me.Mov_Angular.AutoSize = True
        Me.Mov_Angular.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Mov_Angular.Location = New System.Drawing.Point(154, 160)
        Me.Mov_Angular.Name = "Mov_Angular"
        Me.Mov_Angular.Size = New System.Drawing.Size(90, 17)
        Me.Mov_Angular.TabIndex = 92
        Me.Mov_Angular.Text = "RadioButton2"
        Me.Mov_Angular.UseVisualStyleBackColor = True
        '
        'Current_Pos
        '
        '
        'MainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1229, 751)
        Me.Controls.Add(Me.Mov_Angular)
        Me.Controls.Add(Me.Mov_Coordenadas)
        Me.Controls.Add(Me.GroupBox9)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.COMS)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.GroupBox11)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button_Deshabilitar)
        Me.Controls.Add(Me.Boton_Inicio)
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainMenu"
        Me.Text = "Menu Principal"
        CType(Me.PosDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.PosDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PosicionesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Boton_Inicio As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button_Leer_Pos As System.Windows.Forms.Button
    Friend WithEvents Button_Deshabilitar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox_X As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Y As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Z As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button_Mov_PTP As System.Windows.Forms.Button
    Friend WithEvents Button_New_Hombe As System.Windows.Forms.Button
    Friend WithEvents Button_Right As System.Windows.Forms.Button
    Friend WithEvents Button_left As System.Windows.Forms.Button
    Friend WithEvents Button_UP As System.Windows.Forms.Button
    Friend WithEvents Button_Down As System.Windows.Forms.Button
    Friend WithEvents Button_Z_UP As System.Windows.Forms.Button
    Friend WithEvents Button_Z_DOWN As System.Windows.Forms.Button
    Friend WithEvents ComboBox_Resolucion As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Button_Guardar_Pos As System.Windows.Forms.Button
    Friend WithEvents PosDataSet As CJMXSOFT.PosDataSet
    Friend WithEvents PosDataSet1 As CJMXSOFT.PosDataSet1
    Friend WithEvents PosicionesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PosicionesTableAdapter As CJMXSOFT.PosDataSet1TableAdapters.PosicionesTableAdapter
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button_Eliminar_Pos As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button_Mov_Lin As System.Windows.Forms.Button
    Friend WithEvents Button_Down_Right As System.Windows.Forms.Button
    Friend WithEvents Button_Down_Left As System.Windows.Forms.Button
    Friend WithEvents Button_UP_Right As System.Windows.Forms.Button
    Friend WithEvents Button_Up_left As System.Windows.Forms.Button
    Friend WithEvents Button_Habilitar As System.Windows.Forms.Button
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents COMS As System.Windows.Forms.ComboBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBoxDelay As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button_Config As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton9 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Acel As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Vel As System.Windows.Forms.TextBox
    Friend WithEvents Button_Rutnas As System.Windows.Forms.Button
    Friend WithEvents Button_Go_Home As System.Windows.Forms.Button
    Friend WithEvents TextBox_Ang1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Ang2 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Textbox_Ang3 As System.Windows.Forms.TextBox
    Friend WithEvents Mov_Coordenadas As System.Windows.Forms.RadioButton
    Friend WithEvents Mov_Angular As System.Windows.Forms.RadioButton
    Friend WithEvents Button_Mov_Ang As System.Windows.Forms.Button
    Friend WithEvents Current_Pos As System.ComponentModel.BackgroundWorker

End Class
