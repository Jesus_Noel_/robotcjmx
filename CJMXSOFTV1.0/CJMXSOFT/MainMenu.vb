﻿'Version Final 1.0 ( Ventas )
Imports nmspace
Imports ADOX
Imports Angulos
Imports Kinematics
Imports PossControl


Imports System.Management
Imports System.IO
Imports System.Net.Sockets
Imports System.Text


Imports System.Net.Mime.MediaTypeNames
Public Class MainMenu
    Public Const pi = 3.1415926535897931
    Dim ang As New AngulosD
    Dim kine As New Kinematics.Class1
    Dim functions As New nmspace.maclass
    Dim CurrentP As Integer
    Dim PosCont As New PossControl.Class1





    Dim objMOS As ManagementObjectSearcher

    Dim objMOC As Management.ManagementObjectCollection

    Dim objMO As Management.ManagementObject
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'PosDataSet1.Posiciones' Puede moverla o quitarla según sea necesario.
        'Me.PosicionesTableAdapter.Fill(Me.PosDataSet1.Posiciones)
        'TODO: esta línea de código carga datos en la tabla 'PosDataSet.Posiciones' Puede moverla o quitarla según sea necesario.
        'Me.PosicionesTableAdapter.Fill(Me.PosDataSet.Posiciones)
      
        DataGridView1.ForeColor = Color.Black
        COMS.Items.Clear()
        LectHilo = False

        For Each sp As String In My.Computer.Ports.SerialPortNames
            COMS.Items.Add(sp)
        Next
        GroupBox9.Enabled = False
        Control.CheckForIllegalCrossThreadCalls = False
        conn.Close()
        'Try
        '    conn.Open()
        '    ActualizarDataGrid(1)
        'Catch ex As Exception
        '    MsgBox(ex.ToString)

        'End Try
        RichTextBox1.AppendText("BIENVENIDO!..." & vbCrLf)
        RichTextBox1.AppendText("Seleccione un puerto disponible." & vbCrLf)
        Button_Config.Enabled = True
        Button_Rutnas.Enabled = True
    End Sub
    Dim Nummod As Integer
    Private Sub Boton_Inicio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Boton_Inicio.Click
        GroupBox4.BringToFront()



        Nummod = NmcInit(COMPort, 19200)
        If Nummod = 0 Then
            MsgBox("No hay modulos conectados")
            Close()
        Else


            '    'proceso

            Dim leer As Boolean
            addr = 1
            'IoBitDirOut(4, 3)
            'IoSetOutBit(4, 3) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
            'IoBitDirOut(4, 4)
            'IoSetOutBit(4, 4) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7.
            ' Try

            IoBitDirOut(4, 6)
            'para SDE set= encender
            'para tc clr= encender
            IoClrOutBit(4, 6) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
            IoBitDirIn(4, 0)
            IoBitDirIn(4, 1)
            IoBitDirIn(4, 2)
            Label2.Text = "Conectado"
            Label2.BackColor = Color.PaleGreen
            Me.Refresh()
            Dim l As Integer
            If l = 0 Then
                If My.Computer.FileSystem.FileExists(configpath) Then
                    Dim reader As New System.IO.StreamReader(configpath)
                    Dim serie As String
                    Dim leido As String
                    Dim allLines As List(Of String) = New List(Of String)
                    Dim Linecounter As Integer

                    Do While Not reader.EndOfStream
                        leido = reader.ReadLine
                        'MsgBox(leido)
                        Linecounter = Linecounter + 1
                    Loop


                    If Linecounter > 0 Then

                        conn.Close()

                        conn.ConnectionString = leido
                        Try
                            conn.Open()
                            ActualizarDataGrid(1)
                        Catch ex As Exception
                            MsgBox(ex.ToString)

                        End Try
                    Else
                        reader.Close()
                        Dim writter As New System.IO.StreamWriter(configpath)
                        writter.Write(conn.ConnectionString)
                        writter.Close()
                        Try
                            conn.Open()
                            ActualizarDataGrid(1)
                        Catch ex As Exception
                            MsgBox(ex.ToString)

                        End Try
                    End If
                    reader.Close()

                End If
                LeerSetup()
                a(0) = 200
                a(1) = 200
                a(2) = 200
                a(3) = 0
                b(0) = 400
                b(1) = 400
                b(2) = 400
                b(3) = 0
                h(0) = 50
                h(1) = 50
                h(2) = 50
                h(3) = 0
                H1(0) = 150
                H1(1) = 150
                H1(2) = 150
                H1(3) = 0
                setGains(1, kp01, kd01, ki01, EL, DC)
                setGains(2, kp02, kd02, ki02, EL, DC)
                setGains(3, kp03, kd03, ki03, EL, DC)
                FuncionesLi.motEnableDis(1, 1)
                FuncionesLi.motEnableDis(2, 1)
                FuncionesLi.motEnableDis(3, 1)
                TextBox_Vel.Text = VelGen
                TextBox_Acel.Text = AcelGen

                ServoResetPos(1)

                ServoResetPos(2)

                ServoResetPos(3)
                GroupBox9.Enabled = True
                Button_Config.Enabled = True
                Button_Rutnas.Enabled = True

            Else
                Close()

            End If
            ' Catch ex As Exception
            'If ex.ToString.Contains(" la madre de las I/Os cuando se desconecta alv") Then
            'MostrarMensaje(8)

            '  End If
            '    End Try




        End If
        Boton_Inicio.Enabled = False
        MostrarMensaje(1)
        GroupBox9.Enabled = True
        Button_New_Hombe.Enabled = True
        Button_New_Hombe.BackColor = Color.Aqua

        Button_Deshabilitar.Enabled = True
        'GroupBox4.Enabled = True
        RadioButton7.Checked = True


    End Sub
    Public Function setGains(ByVal motor As Integer, ByVal kp As Integer, ByVal kd As Integer, ByVal ki As Integer, ByVal EL As Integer, ByVal DC As Integer)
        ' ServoSetGain(motor, kp, kd, ki, 400, 255, 0, EL, 1, DC)
        ServoSetGain(motor, kp, kd, ki, 0, 128, 255, EL, 1, DC)
    End Function
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Leer_Pos.Click
        Dim pos1, pos2, pos3 As Int32
        Dim msg As String

        NmcReadStatus(1, SEND_POS)  'Read the current position data from the cotnroller
        pos1 = ServoGetPos(1)        'Fetch the position data 
        NmcReadStatus(2, SEND_POS)  'Read the current position data from the cotnroller
        pos2 = ServoGetPos(2)        'Fetch the position data 
        NmcReadStatus(3, SEND_POS)  'Read the current position data from the cotnroller
        pos3 = ServoGetPos(3)        'Fetch the position data 
        msg = "Motor 1 position: " & pos1 & vbCr & "Motor 2 position: " & pos2 & vbCr & "Motor 3 position: " & pos3 & vbCr
        RichTextBox1.AppendText("Motor 1 position: " & pos1 & vbCr & "Motor 2 position: " & pos2 & vbCr & "Motor 3 position: " & pos3 & vbCr)
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Deshabilitar.Click
        EnableLecturaIO = False

        For i As Integer = 0 To 2
            'ServoStopMotor(i + 1, STOP_SMOOTH)
            ServoStopMotor(i + 1, 0)
            'ServoResetPos(i + 1)
        Next

        Button_New_Hombe.Enabled = True
        MostrarMensaje(3)
        ' NmcShutdown()
        ' GroupBox8.Enabled = False
        GroupBox5.Enabled = False
        '  Button21.Enabled = False
        Button_Rutnas.Enabled = False
        GroupBox3.Enabled = False
        ' GroupBox6.Enabled = False
        ' GroupBox12.Enabled = False
        Button_Habilitar.Enabled = True
        'Button25.BackColor = Color.Aqua
        'Button4.Enabled = False
        Button_New_Hombe.Enabled = True

        TextBox_X.Enabled = False
        TextBox_Y.Enabled = False
        TextBox_Z.Enabled = False
        Button_Mov_PTP.Enabled = False
        Button_Mov_Lin.Enabled = False
        Button_Go_Home.Enabled = False
    End Sub
    Public Function inversekinematics(ByVal xpf As Double, ByVal ypf As Double, ByVal zpf As Double) ' 2452
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim ret As Integer = 1
        interacion = 0
        Pf(0) = ypf
        Pf(1) = xpf
        Pf(2) = zpf
        Pf(3) = 1
        For i = 0 To 16
            Ti(i) = 0
        Next
        Phi(0) = 0
        Phi(1) = 2 * pi / 3
        Phi(2) = 4 * pi / 3
        Phi(3) = 0
        For i = 0 To 3
            Ti(i * 4) = h(i)
            Variables.Rot = ang.RotMat(3, Phi(i), Variables.Rot)
            For j = 0 To 4
                Variables.aux(j) = Pf(j)
            Next
            Variables.aux(3) = 1
            Variables.aux1(0) = h(i)
            Variables.aux1(1) = 0
            Variables.aux1(2) = 0
            Variables.aux1(3) = 0
            Variables.aux2 = ang.multMat(4, 4, Variables.Rot, 4, 1, Variables.aux1, 4, 1, Variables.aux2)
            Variables.aux1 = ang.addMat(4, 1, Variables.aux, 4, 1, Variables.aux2, 4, 1, Variables.aux1)
            k = 3
            Variables.aux(4) = 1
            Variables.aux1(4) = 1
            Variables.aux1(4) = 1
            Variables.aux1(4) = 1
            For j = 0 To 4
                Cf(i * 4 + j) = Variables.aux1(j)
            Next
            For j = 0 To 4
                If k = j Then
                    k = k + 4
                End If
            Next
            Dim TOTO As Double
            R(i) = kine.R(functions.cO(Phi(i)), Cf(i * 4), functions.sI(Phi(i)), Cf(i * 4 + 1))
            N(i) = kine.N(b(i), Cf(i * 4), Cf(i * 4 + 1), Cf(i * 4 + 2), H1(i), a(i), R(i))
            M(i) = kine.M(a(i), Cf(i * 4 + 2))
            Q(i) = kine.Q(H1(i), a(i), R(i))
            If ((4 * (M(i) * M(i) + Q(i) * Q(i) - N(i) * N(i))) > 0.1) Then
                Dim at As Double
                Dim sq As Double
                Dim r2p As Double
                interacion = interacion + 1
                TOTO = 4 * ((M(i) * M(i)) + (Q(i) * Q(i)) - (N(i) * N(i)))
                at = functions.atA(2 * M(i))
                sq = functions.sqrT(TOTO)
                r2p = functions.Rad2Pulses(100)
                If Q(i) + N(i) = 0 Then
                    Q(i) = Q(i) + 1

                End If
                If Q(i) + N(i) = 0 Then
                    DivisorKine = 1
                Else
                    DivisorKine = Q(i) + N(i)

                End If
                TH1(i) = 2 * functions.atA((2 * M(i) + functions.sqrT(4 * (M(i) * M(i) + Q(i) * Q(i) - N(i) * N(i)))) / (2 * DivisorKine))
                RadCon(i) = TH1(i)
                PulsosCon(i) = RadToPulses(RadCon(i))
                TH1pulses(i) = RadToPulses(TH1(i))

                Dim a As Double
                a = TH1pulses(i)



            Else
                TH1(i) = TH1ant(i)
                If i = 2 Then
                    ret = 0

                End If
            End If
        Next
        TH1ant(0) = TH1(0)
        TH1ant(1) = TH1(1)
        TH1ant(2) = TH1(2)
        Dim v As Integer = 50000
        Dim an As Integer = 50

        Return (ret)

    End Function

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Mov_PTP.Click


        Dim statbyte(3) As Int32
        Dim auxbyte(2) As Int32
        MovimientoLienal = False
        Xp = TextBox_X.Text
        Yp = TextBox_Y.Text
        Zp = TextBox_Z.Text
        CoorX = Xp
        CoorY = Yp
        CoorZ = Zp

        '  System.Threading.Thread.Sleep(100)
        VelGen = TextBox_Vel.Text
        AcelGen = TextBox_Acel.Text

        res = Func_2()
        '   System.Threading.Thread.Sleep(100)
        LectHilo = False



        Do
            NmcNoOp(1)               'NoOp command to read current module status
            statbyte(1) = NmcGetStat(1) 'Fetch the module status byte
            NmcNoOp(2)               'NoOp command to read current module status
            statbyte(2) = NmcGetStat(2) 'Fetch the module status byte
            NmcNoOp(3)               'NoOp command to read current module status
            statbyte(3) = NmcGetStat(3) 'Fetch the module status byte




        Loop While ((statbyte(1) And MOVE_DONE) = 0) Or ((statbyte(2) And MOVE_DONE) = 0) Or ((statbyte(3) And MOVE_DONE) = 0)


        '  Actual()

        MostrarMensaje(4)

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_New_Hombe.Click
        Dim statbyte(3) As Int32
        CoorX = CoorIniX
        CoorY = CoorIniY
        CoorZ = CoorIniZ
        ServoResetPos(1)
        ServoResetPos(2)
        ServoResetPos(3)
        System.Threading.Thread.Sleep(100)
        VelGen = TextBox_Vel.Text
        AcelGen = TextBox_Acel.Text

        Home()

        ServoSetPos(1, ServoGetPos(1) * 0 + (1040 * ConversionFactor1) - (0))
        ServoSetPos(2, ServoGetPos(2) * 0 + (920 * ConversionFactor2) - (0))
        ServoSetPos(3, ServoGetPos(3) * 0 + (1040 * ConversionFactor3) - (0))
        'ServoSetPos(1, ServoGetPos(1) * 0 + (1 * ConversionFactor1) - (0))
        'ServoSetPos(2, ServoGetPos(2) * 0 + (1 * ConversionFactor2) - (0))
        'ServoSetPos(3, ServoGetPos(3) * 0 + (1 * ConversionFactor3) - (0))
        NmcReadStatus(1, SEND_POS)  'Read the current position data from the cotnroller
        NmcReadStatus(2, SEND_POS)
        NmcReadStatus(3, SEND_POS)
        HomePos(1) = ServoGetPos(1)
        HomePos(2) = ServoGetPos(2)
        HomePos(3) = ServoGetPos(3)
        Actual()
        Button_New_Hombe.Enabled = False
        Button_Habilitar.Enabled = False
        Button_Go_Home.Enabled = True
        GroupBox5.Enabled = True
        GroupBox3.Enabled = True
        GroupBox9.Enabled = True
        GroupBox6.Enabled = True
        GroupBox12.Enabled = True
        GroupBox8.Enabled = True
        GroupBox5.Enabled = True
        TextBox_X.Enabled = True
        TextBox_Y.Enabled = True
        TextBox_Z.Enabled = True
        Button_Mov_PTP.Enabled = True
        Button_Mov_Lin.Enabled = True
        Button_Rutnas.Enabled = True
        Button_Config.Enabled = True
        TextBox_X.Enabled = True
        TextBox_Y.Enabled = True
        TextBox_Z.Enabled = True
        Button_Mov_PTP.Enabled = True
        Button_Mov_Lin.Enabled = True
        MostrarMensaje(2)
        Button_New_Hombe.BackColor = Color.Transparent
        MovimientoLienal = False


        Actual()

        MostrarMensaje(4)
    End Sub




    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Right.Click
        Joy("X+")
        Actual()
        MostrarMensaje(4)

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_left.Click
        Joy("X-")
        Actual()
        MostrarMensaje(4)
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_UP.Click

        Joy("Y-")
        Actual()
        MostrarMensaje(4)

    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Down.Click
        Joy("Y+")
        Actual()
        MostrarMensaje(4)
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Z_UP.Click
        Joy("Z+")
        Actual()
        MostrarMensaje(4)
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Z_DOWN.Click
        Joy("Z-")
        Actual()
        MostrarMensaje(4)
    End Sub
    Public Function Joy(ByVal ref As String)
        Dim Resolucion As Integer
        Dim statbyte(3) As Int32
        Resolucion = ComboBox_Resolucion.Text
        'en Z no puede ir a mas de -600+- 
        'si se hace eso hay que sumarle hasta que velva a 600
        'asi se podra volver a mover
        'lim -600X


        Select Case ref
            Case "X+"
                Xp = CoorX + Resolucion
                Yp = CoorY
                Zp = CoorZ
                CoorX = CoorX + Resolucion
            Case "X-"
                Xp = CoorX - Resolucion
                Yp = CoorY
                Zp = CoorZ
                CoorX = CoorX - Resolucion
            Case "Y+"
                Xp = CoorX
                Yp = CoorY + Resolucion
                Zp = CoorZ
                CoorY = CoorY + Resolucion
            Case "Y-"
                Xp = CoorX
                Yp = CoorY - Resolucion
                Zp = CoorZ
                CoorY = CoorY - Resolucion
            Case "Z+"
                Xp = CoorX
                Yp = CoorY
                Zp = CoorZ + Resolucion
                CoorZ = CoorZ + Resolucion
            Case "Z-"
                Xp = CoorX
                Yp = CoorY
                Zp = CoorZ - Resolucion
                CoorZ = CoorZ - Resolucion
            Case "X+Y-"
                Xp = CoorX + Resolucion
                Yp = CoorY - Resolucion
                Zp = CoorZ
                CoorX = CoorX + Resolucion
                CoorY = CoorY - Resolucion
            Case "X-Y-"
                Xp = CoorX - Resolucion
                Yp = CoorY - Resolucion
                Zp = CoorZ
                CoorX = CoorX - Resolucion
                CoorY = CoorY - Resolucion
            Case "X-Y+"
                Xp = CoorX - Resolucion
                Yp = CoorY + Resolucion
                Zp = CoorZ
                CoorX = CoorX - Resolucion
                CoorY = CoorY + Resolucion
            Case "X+Y+"
                Xp = CoorX + Resolucion
                Yp = CoorY + Resolucion
                Zp = CoorZ
                CoorX = CoorX + Resolucion
                CoorY = CoorY + Resolucion
        End Select

        VelGen = TextBox_Vel.Text
        AcelGen = TextBox_Acel.Text

        res = Func_2()
        Do
            NmcNoOp(1)               'NoOp command to read current module status
            statbyte(1) = NmcGetStat(1) 'Fetch the module status byte
            NmcNoOp(2)               'NoOp command to read current module status
            statbyte(2) = NmcGetStat(2) 'Fetch the module status byte
            NmcNoOp(3)               'NoOp command to read current module status
            statbyte(3) = NmcGetStat(3) 'Fetch the module status byte
        Loop While ((statbyte(1) And MOVE_DONE) = 0) Or ((statbyte(2) And MOVE_DONE) = 0) Or ((statbyte(3) And MOVE_DONE) = 0)

    End Function

    Public Function Actual()
        Dim pos1, pos2, pos3 As Int32

        NmcReadStatus(1, SEND_POS)  'Read the current position data from the cotnroller
        pos1 = ServoGetPos(1)        'Fetch the position data 
        NmcReadStatus(2, SEND_POS)  'Read the current position data from the cotnroller
        pos2 = ServoGetPos(2)        'Fetch the position data 
        NmcReadStatus(3, SEND_POS)  'Read the current position data from the cotnroller
        pos3 = ServoGetPos(3)        'Fetch the position data 

        Label15.Text = CoorX
        Label18.Text = CoorY
        Label17.Text = CoorZ
        Me.Refresh()


    End Function

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Guardar_Pos.Click
        Dim void, Lin As String
        Dim Wait As Integer
        If IsNumeric(TextBox_Vel.Text) And IsNumeric(TextBox_Acel.Text) Then

            If Label15.Text = "Home" Then
                conn.Close()
                conn.Open()
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text
                cmd.CommandText = "SELECT MAX(NUM) AS MAXIMO FROM POSICIONES"


                Try
                    dr = cmd.ExecuteReader()
                    If dr.HasRows Then
                        While dr.Read()
                            CurrentP = (CInt(dr("MAXIMO")) + 1)





                        End While
                    Else
                        CurrentP = 0


                    End If
                    dr.Close()


                Catch ex As Exception
                    '  MsgBox(ex.ToString)
                    If ex.ToString.Contains("Null") Then
                        CurrentP = 0
                    End If
                End Try

                dr.Close()
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text
                sql = "INSERT INTO POSICIONES (NUM, X)"
                sql += "VALUES(" & CurrentP & ",'" & Label15.Text & "')"
                cmd.CommandText = sql
                Try
                    cmd.ExecuteNonQuery()
                    MostrarMensaje(8)

                Catch ex As Exception
                    MsgBox(ex.ToString)

                End Try
                ActualizarDataGrid(1)
                NumOp += 1
                Indx += 1

            End If
            If TextBox_Acel.Text < 1000 Then


                If RadioButton7.Checked = True Then
                    void = "0"
                Else
                    void = "1"
                End If
                If RadioButton8.Checked = True Then
                    Lin = "1"
                Else
                    Lin = "0"

                End If
                If CheckBox1.Checked = True Then
                    Wait = TextBoxDelay.Text

                Else
                    Wait = 0


                End If
                conn.Close()
                conn.Open()
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text
                cmd.CommandText = "SELECT MAX(NUM) AS MAXIMO FROM POSICIONES"


                Try
                    dr = cmd.ExecuteReader()
                    If dr.HasRows Then
                        While dr.Read()
                            CurrentP = (CInt(dr("MAXIMO")) + 1)





                        End While
                    Else
                        CurrentP = 0


                    End If
                    dr.Close()


                Catch ex As Exception

                    If ex.ToString.Contains("Null") Then
                        CurrentP = 0
                    End If
                End Try

                dr.Close()
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text
                sql = "INSERT INTO POSICIONES (NUM, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION)"
                sql += "VALUES(" & CurrentP & ",'" & Label15.Text & "','" & Label18.Text & "','" & Label17.Text & "','" & void & "','" & Lin & "','" & Wait & "','" & TextBox_Vel.Text & "','" & TextBox_Acel.Text & "')"
                cmd.CommandText = sql


                Try
                    cmd.ExecuteNonQuery()
                    MostrarMensaje(7)

                Catch ex As Exception
                    MsgBox(ex.ToString)

                End Try
                ActualizarDataGrid(1)
                NumOp += 1
                Indx += 1


            Else
                MsgBox("Aceleracion Invalida")
            End If
        End If
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Eliminar_Pos.Click

        If DataGridView1.CurrentCellAddress.X > -1 Then


            Dim i As Integer
            Dim a As Integer
            conn.Close()
            conn.Open()

            i = DataGridView1.CurrentRow.Index
            a = DataGridView1.Item(0, i).Value
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            sql = "DELETE FROM POSICIONES WHERE NUM=" & a & ""
            cmd.CommandText = sql


            Try
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try
            ActualizarDataGrid(1)
            Indx = Indx - 1

            '   sql += "VALUES('" & Label15.Text & "','" & Label18.Text & "','" & Label17.Text & "')"
        End If


    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click

        Me.Close()

        '   sq
    End Sub



    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        IoSetOutBit(4, 6) 'addr=1, Set output bit 7 to logic 1, use number 6 for bit #7
    End Sub

    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        IoClrOutBit(4, 6) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7

    End Sub

    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox(IoInBitVal(4, 6))

    End Sub



    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Mov_Lin.Click
        MovimientoLienal = True
        VelGen = TextBox_Vel.Text
        AcelGen = TextBox_Acel.Text
        Xp = TextBox_X.Text
        Yp = TextBox_Y.Text
        Zp = TextBox_Z.Text
        CoorX = Xp
        CoorY = Yp
        CoorZ = Zp
        Dim asd As Long

        asd = MovLin()

        MostrarMensaje(4)

    End Sub



    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' CreateAccessDatabase(Application.StartupPath & "\DB\TEST.accdb")

    End Sub
    Private Sub Button21_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        EnableLecturaIO = False

        NmcShutdown()
        conn.Close()
        Me.Hide()
        PIDMenu.Show()


    End Sub



    Private Sub Button7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ActualizarDataGrid(1)
    End Sub






    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_UP_Right.Click
        Joy("X+Y-")
        Actual()
        MostrarMensaje(4)
    End Sub

    Private Sub Button18_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Up_left.Click
        Joy("X-Y-")
        Actual()
        MostrarMensaje(4)
    End Sub

    Private Sub Button20_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Down_Left.Click
        Joy("X-Y+")
        Actual()
        MostrarMensaje(4)
    End Sub

    Private Sub Button24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Down_Right.Click
        Joy("X+Y+")
        Actual()
        MostrarMensaje(4)
    End Sub

    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Habilitar.Click
        Dim auxbyte(3) As Int32
        Dim statbyte(3) As Byte

        Dim vel As Integer
        Dim acc As Integer
        VelGen = TextBox_Vel.Text
        AcelGen = TextBox_Acel.Text
        ' Nummod = NmcInit(COMPort, 19200)
        IoBitDirOut(4, 6)
        IoClrOutBit(4, 6) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
        'IoBitDirOut(4, 4)
        'IoSetOutBit(4, 4) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7


        FuncionesLi.motEnableDis(1, 1)
        FuncionesLi.motEnableDis(2, 1)
        FuncionesLi.motEnableDis(3, 1)
        ServoStopMotor(1, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(1, AMP_ENABLE Or STOP_MOTOR)
        'ServoResetPos(1)
        ServoStopMotor(2, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(2, AMP_ENABLE Or STOP_MOTOR)
        'ServoResetPos(2)
        ServoStopMotor(3, AMP_ENABLE Or MOTOR_OFF)
        ServoStopMotor(3, AMP_ENABLE Or STOP_MOTOR)
        ' ServoResetPos(3)

        For i As Integer = 0 To 3
            TH1pulses_ant(i) = 0

        Next
        Button_Habilitar.Enabled = False
        Button_Habilitar.BackColor = Color.Transparent


        Button_New_Hombe.Enabled = True
        Button_New_Hombe.BackColor = Color.Transparent
        Button_Deshabilitar.Enabled = True

        GoHome()


        Button_New_Hombe.Enabled = False

        GroupBox5.Enabled = True
        GroupBox3.Enabled = True
        GroupBox9.Enabled = True
        GroupBox6.Enabled = True
        GroupBox12.Enabled = True
        GroupBox8.Enabled = True
        GroupBox5.Enabled = True
        TextBox_X.Enabled = True
        TextBox_Y.Enabled = True
        TextBox_Z.Enabled = True
        Button_Mov_PTP.Enabled = True
        Button_Mov_Lin.Enabled = True
        Button_Rutnas.Enabled = True
        Button_Config.Enabled = True
        TextBox_X.Enabled = True
        TextBox_Y.Enabled = True
        TextBox_Z.Enabled = True
        Button_Mov_PTP.Enabled = True
        Button_Mov_Lin.Enabled = True
        Button_Go_Home.Enabled = True
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COMS.SelectedIndexChanged
        COMPort = COMS.SelectedItem.ToString & ":"
        Boton_Inicio.Enabled = True


    End Sub





    Private Sub Button22_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Rutnas.Click
        Dim statbyte(3) As Int32
        MovimientoLienal = False

        System.Threading.Thread.Sleep(100)


        conn.Close()
        Me.Hide()
        MenuRutinas.Show()
        EnableLecturaIO = True
    End Sub


    Private Sub RadioButton6_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        If RadioButton6.Checked = True Then
            'IoClrOutBit(4, 6)
            'IoClrOutBit(4, 4)
            'System.Threading.Thread.Sleep(60)
            IoSetOutBit(4, 6) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
            MostrarMensaje(6)

        End If
    End Sub

    Private Sub RadioButton7_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton7.CheckedChanged
        If RadioButton7.Checked = True Then



            IoClrOutBit(4, 6)
            ' IoSetOutBit(4, 4)
            ' IoSetOutBit(4, 3) 'addr=1, Set output bit 7 to logic 1, use number 6 for bit #7


            ' NmcReadStatus(4, SEND_INPUTS)
            '  MsgBox(IoInBitVal(4, 3))

            MostrarMensaje(5)
        End If
    End Sub

    Private Sub Button21_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Config.Click
        EnableLecturaIO = False
        ' NmcShutdown()
        conn.Close()
        Me.Hide()
        PIDMenu.Show()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Go_Home.Click
        VelGen = TextBox_Vel.Text
        AcelGen = TextBox_Acel.Text
        GoHome()
        Label15.Text = "HOME"
        Label18.Text = "HOME"
        Label17.Text = "HOME"
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Mov_Coordenadas.CheckedChanged
        If Mov_Coordenadas.Checked = True Then
            Label1.Enabled = True
            Label3.Enabled = True
            Label4.Enabled = True
            Label1.Visible = True
            Label3.Visible = True
            Label4.Visible = True

            Label6.Enabled = False
            Label12.Enabled = False
            Label13.Enabled = False
            Label6.Visible = False
            Label12.Visible = False
            Label13.Visible = False

            TextBox_X.Enabled = True
            TextBox_Y.Enabled = True
            TextBox_Z.Enabled = True
            TextBox_X.Visible = True
            TextBox_Y.Visible = True
            TextBox_Z.Visible = True

            TextBox_Ang1.Enabled = False
            TextBox_Ang2.Enabled = False
            Textbox_Ang3.Enabled = False
            TextBox_Ang1.Visible = False
            TextBox_Ang2.Visible = False
            Textbox_Ang3.Visible = False

            Button_Mov_Ang.Enabled = False
            Button_Mov_Ang.Visible = False
            Button_Mov_Lin.Enabled = True
            Button_Mov_Lin.Visible = True
            Button_Mov_PTP.Enabled = True
            Button_Mov_PTP.Visible = True
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Mov_Angular.CheckedChanged
        If Mov_Angular.Checked = True Then
            Label1.Enabled = False
            Label3.Enabled = False
            Label4.Enabled = False
            Label1.Visible = False
            Label3.Visible = False
            Label4.Visible = False

            Label6.Enabled = True
            Label12.Enabled = True
            Label13.Enabled = True
            Label6.Visible = True
            Label12.Visible = True
            Label13.Visible = True

            TextBox_X.Enabled = False
            TextBox_Y.Enabled = False
            TextBox_Z.Enabled = False
            TextBox_X.Visible = False
            TextBox_Y.Visible = False
            TextBox_Z.Visible = False

            TextBox_Ang1.Enabled = True
            TextBox_Ang2.Enabled = True
            Textbox_Ang3.Enabled = True
            TextBox_Ang1.Visible = True
            TextBox_Ang2.Visible = True
            Textbox_Ang3.Visible = True
            Button_Mov_Ang.Enabled = True
            Button_Mov_Ang.Visible = True

            Button_Mov_Lin.Enabled = False
            Button_Mov_Lin.Visible = False
            Button_Mov_PTP.Enabled = False
            Button_Mov_PTP.Visible = False
        End If

    End Sub



    Private Sub Button17_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Mov_Ang.Click
        Dim toResta(3) As Double

        Radianes_Motor(0) = AngulosToRad(TextBox_Ang1.Text)
        Radianes_Motor(1) = AngulosToRad(TextBox_Ang2.Text)
        Radianes_Motor(2) = AngulosToRad(Textbox_Ang3.Text)
        For i As Integer = 0 To 2
            toResta(i) = RadToPulses(Radianes_Motor(i))
            TH1pulses(i) = TH1pulses_ant(i) - toResta(i) ''con resta, sube el brazo(sentido horario en el radioboton), con suma, baja el brazo (sentido anti horario en el rb)
            TH1pulses_ant(i) = TH1pulses(i)
            vel(i) = 5000
            acel(i) = 50
        Next
        Moveto(TH1pulses(0), vel(0), acel(0), TH1pulses(1), vel(1), acel(1), TH1pulses(2), vel(2), acel(2))
    End Sub

    Private Sub Current_Pos_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles Current_Pos.DoWork
        While (1)


            Dim pos1, pos2, pos3 As Int32
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            NmcReadStatus(1, SEND_POS)  'Read the current position data from the cotnroller
            pos1 = ServoGetPos(1)        'Fetch the position data 
            NmcReadStatus(2, SEND_POS)  'Read the current position data from the cotnroller
            pos2 = ServoGetPos(2)        'Fetch the position data 
            NmcReadStatus(3, SEND_POS)  'Read the current position data from the cotnroller
            pos3 = ServoGetPos(3)        'Fetch the position data 
            Label15.Text = pos1
            Label18.Text = pos2
            Label17.Text = pos3
            sql = "INSERT INTO Current_pos (M1, M2, M3)"
            sql += "VALUES('" & pos1 & "','" & pos2 & "','" & pos3 & "')"
            cmd.CommandText = sql
            Try
                cmd.ExecuteNonQuery()


            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try
        End While
    End Sub
End Class
