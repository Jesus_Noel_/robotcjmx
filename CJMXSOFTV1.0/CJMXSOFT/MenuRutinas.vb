﻿Public Class MenuRutinas

    Dim Nummod As Integer

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Indx = Secuencia.RowCount
        conn.Close()
        conn.Open()
        If TextBox1.Text <> "" Then

            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            sql = "create table " & TextBox1.Text & " (POSICION int, X text, Y text, Z text, SUCC text, LIN text, DELAY text, VELOCIDAD text, ACELERACION text,  PRIMARY KEY (POSICION))"
            cmd.CommandText = sql
            Try
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try




            sql = "INSERT INTO RUTINAS (RUTINA)"
            sql += "VALUES('" & TextBox1.Text & "')"
            cmd.CommandText = sql


            Try
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try
            For i As Integer = 0 To Indx - 2
                Dim pos(3), void, Lin, Vel, Acel, delay As String
                Dim posicionnum As Integer
                posicionnum = i + 1
                pos(0) = Secuencia.Item(2, i).Value
                pos(1) = Secuencia.Item(3, i).Value
                pos(2) = Secuencia.Item(4, i).Value
                void = Secuencia.Item(5, i).Value
                Lin = Secuencia.Item(6, i).Value
                delay = Secuencia.Item(7, i).Value
                Vel = Secuencia.Item(8, i).Value
                Acel = Secuencia.Item(9, i).Value
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text

                sql = "INSERT INTO " & TextBox1.Text & " (POSICION, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION)"
                sql += "VALUES('" & posicionnum & "','" & pos(0) & "','" & pos(1) & "','" & pos(2) & "','" & void & "','" & Lin & "','" & delay & "','" & Vel & "','" & Acel & "')"
                cmd.CommandText = sql


                Try
                    cmd.ExecuteNonQuery()

                Catch ex As Exception
                    MsgBox(ex.ToString)

                End Try


            Next
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT DISTINCT RUTINA FROM RUTINAS"
            ComboBox1.Items.Clear()


            Control.CheckForIllegalCrossThreadCalls = False
            Try
                Try
                    dr = cmd.ExecuteReader()
                    While dr.Read()
                        ComboBox1.Items.Add(dr(0))

                    End While
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try
            dr.Close()
        Else
            RichTextBox1.AppendText("Asigne un nombre a la rutina")
        End If
    End Sub

    Private Sub Form3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        conn.Close()
        Posiciones.ForeColor = Color.Black
        Secuencia.ForeColor = Color.Black
        Posiciones.Font = New Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Point)
        Secuencia.Font = New Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Point)
        EnableLecturaIO = True


        CorrerRutinas.RunWorkerAsync()
        LecturaIO.RunWorkerAsync()

        Control.CheckForIllegalCrossThreadCalls = False
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT DISTINCT RUTINA FROM RUTINAS"
        ComboBox1.Items.Clear()

        Try
            conn.Open()

            Try
                dr = cmd.ExecuteReader()

                While dr.Read()
                    ComboBox1.Items.Add(dr(0))

                End While
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
        dr.Close()
        ActualizarDataGrid(3)
        ActualizarSecuenceGrid(1)

       
       

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Indx = Secuencia.RowCount


        Dim statbyte(3) As Int32
        For i As Integer = 0 To Indx - 1
            Dim pos(3), Void, Lin, delay, vel, Acel As String
            pos(0) = Secuencia.Item(2, i).Value
            pos(1) = Secuencia.Item(3, i).Value
            pos(2) = Secuencia.Item(4, i).Value

            Void = Secuencia.Item(5, i).Value
            Lin = Secuencia.Item(6, i).Value
            delay = Secuencia.Item(7, i).Value
            vel = Secuencia.Item(8, i).Value
            Acel = Secuencia.Item(9, i).Value
            VelGen = vel
            AcelGen = Acel
            Xp = pos(0)
            Yp = pos(1)
            Zp = pos(2)
            res = Func_2()

            Do
                NmcNoOp(1)               'NoOp command to read current module status
                statbyte(1) = NmcGetStat(1) 'Fetch the module status byte
                NmcNoOp(2)               'NoOp command to read current module status
                statbyte(2) = NmcGetStat(2) 'Fetch the module status byte
                NmcNoOp(3)               'NoOp command to read current module status
                statbyte(3) = NmcGetStat(3) 'Fetch the module status byte
            Loop While ((statbyte(1) And MOVE_DONE) = 0) Or ((statbyte(2) And MOVE_DONE) = 0) Or ((statbyte(3) And MOVE_DONE) = 0)



        Next
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Rutina()


    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        EnableLecturaIO = False

        conn.Close()
        Me.Hide()
        MainMenu.Show()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        ActualizarDataGrid(3)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim i As Integer
        Dim a As Integer



        conn.Close()
        conn.Open()
        dr.Close()

        i = Posiciones.CurrentRow.Index

        a = Posiciones.Item(0, i).Value
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text

        cmd.CommandText = "SELECT MAX(COMANDO) AS LASTCOMMAND FROM SECUENCIAS"

        Try
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()


                    lastcommand = dr("LASTCOMMAND") + 1

                End While

            Else
                lastcommand = 0

            End If
            dr.Close()


        Catch ex As Exception
            If ex.ToString.Contains("exist") Then
                lastcommand = 0
                Indx = 0
            End If
        End Try
        dr.Close()




        cmd.Connection = conn
        cmd.CommandType = CommandType.Text

        cmd.CommandText = "SELECT NUM, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM POSICIONES WHERE NUM=" & a & ""

        Try
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()
                    cmd2.Connection = conn
                    cmd2.CommandType = CommandType.Text
                    sql2 = "INSERT INTO SECUENCIAS (COMANDO, NUM, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION)"
                    sql2 += "VALUES(" & lastcommand & "," & dr(0) & ",'" & dr(1) & "','" & dr(2) & "','" & dr(3) & "','" & dr(4) & "','" & dr(5) & "','" & dr(6) & "','" & dr(7) & "','" & dr(8) & "')"
                    cmd2.CommandText = sql2
                    Indx = Indx + 1

                    Try
                        cmd2.ExecuteNonQuery()


                    Catch ex As Exception
                        MsgBox(ex.ToString)

                    End Try
                    ActualizarSecuenceGrid(1)

                End While

            End If
            dr.Close()


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        LastCreated = lastcommand
        Label5.Text = "Ultimo Agregado: " & LastCreated
        Indx = Secuencia.RowCount
        If Secuencia.RowCount = 1 Then
            Secuencia.Rows(Indx - 1).DefaultCellStyle.BackColor = Color.PaleGreen
        Else
            Secuencia.Rows(Indx - 2).DefaultCellStyle.BackColor = Color.PaleGreen
        End If


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim i As Integer
        Dim a As Integer
        conn.Close()
        conn.Open()
        If Secuencia.CurrentCellAddress.X > -1 Then


            i = Secuencia.CurrentRow.Index

            '  MsgBox(DataGridView1.Item(0, i).Value)
            a = Secuencia.Item(0, i).Value
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            sql = "DELETE FROM SECUENCIAS WHERE COMANDO=" & a & ""
            cmd.CommandText = sql


            Try
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try
            ActualizarSecuenceGrid(1)
            If lastcommand = 0 Then
                lastcommand = 0
            Else
                lastcommand = lastcommand - 1
            End If

            Indx = Indx - 1
            LastDeleted = a
            Label6.Text = "Ultimo Eliminado: " & LastDeleted
        End If

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim i, j As Integer
        Dim a, b As Integer
        conn.Close()
        conn.Open()
        dr.Close()
        i = Posiciones.CurrentRow.Index
        a = Posiciones.Item(0, i).Value
        j = Secuencia.CurrentRow.Index
        b = Secuencia.Item(0, j).Value
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT NUM, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM POSICIONES WHERE NUM=" & a & ""
        Try
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()
                    cmd2.Connection = conn
                    cmd2.CommandType = CommandType.Text
                    sql2 = "UPDATE SECUENCIAS SET NUM = " & dr(0) & ", X = " & dr(1) & ", Y = " & dr(2) & ", Z = " & dr(3) & ", SUCC = " & dr(4) & ", LIN = " & dr(5) & ", DELAY = " & dr(6) & ", VELOCIDAD = " & dr(7) & ", ACELERACION = " & dr(8) & " WHERE COMANDO=" & b
                    cmd2.CommandText = sql2
                    Try
                        cmd2.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                    ActualizarSecuenceGrid(1)
                End While
            End If
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        LastModified = b
        Label7.Text = "Ultimo Modificado: " & LastModified
        Secuencia.Rows(j).DefaultCellStyle.BackColor = Color.LightCyan
    End Sub



    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If CheckBox2.Checked = True Then
            If ComboBox1.Text <> "" Then
                ActualizarSecuenceGrid(2)
                Button11.Enabled = True
                Button12.Enabled = True
            End If



        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim i, j As Integer
        Dim a, b As Integer
        Dim num, x, y, z, Succ, Lin, Delay, Velocidad, Aceleracion As Integer


        conn.Close()
        conn.Open()
        dr.Close()
        j = Secuencia.CurrentRow.Index
        b = Secuencia.Item(0, j).Value
        num = Secuencia.Item(1, j).Value
        x = Secuencia.Item(2, j).Value
        y = Secuencia.Item(3, j).Value
        z = Secuencia.Item(4, j).Value
        Succ = Secuencia.Item(5, j).Value
        Lin = Secuencia.Item(6, j).Value
        Delay = Secuencia.Item(7, j).Value
        Velocidad = Secuencia.Item(8, j).Value
        Aceleracion = Secuencia.Item(9, j).Value
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text

        sql = "UPDATE SECUENCIAS SET NUM = " & num & ", X = " & x & ", Y = " & y & ", Z = " & z & ", SUCC = " & Succ & ", LIN = " & Lin & ", DELAY = " & Delay & ", VELOCIDAD = " & Velocidad & ", ACELERACION = " & Aceleracion & " WHERE COMANDO=" & b

        ' sql2 += "VALUES(" & dr(0) & ",'" & dr(1) & "','" & dr(2) & "','" & dr(3) & "','" & dr(4) & "','" & dr(5) & "','" & dr(6) & "')"
        cmd.CommandText = sql
        Try
            cmd.ExecuteNonQuery()


        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
        '  ActualizarSecuenceGrid()
        LastModified = b
        Label7.Text = "Ultimo Modificado: " & LastModified
        Secuencia.Rows(j).DefaultCellStyle.BackColor = Color.LightCyan
    End Sub

   


    Private Sub CorrerRutinas_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles CorrerRutinas.DoWork
        Dim salir As Boolean

        While (True)
            If EnableSecuence = True Then

                conn.Close()
                conn.Open()

                IoBitDirOut(4, 6)
                IoClrOutBit(4, 6) 'addr=1, Clear output bit 7 to logic 0, use number 6 for bit #7
                ' IoBitDirOut(4, 4)
                '  IoSetOutBit(4, 4)
                If ComboBox1.Text <> "" And TextBox2.Text IsNot Nothing Then


                    Dim bol As Boolean
                    bol = IsNumeric(TextBox2.Text)
                    If bol = True Then

                        If TextBox2.Text > 0 And TextBox2.Text < 100000 Then


                            Dim statbyte(3) As Int32
                            cmd.Connection = conn
                            cmd.CommandType = CommandType.Text
                            ' cmd.CommandText = "SELECT  X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM RUTINAS WHERE RUTINA='" & ComboBox1.SelectedItem.ToString & "'"
                            cmd.CommandText = "SELECT  X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM " & ComboBox1.SelectedItem

                            Control.CheckForIllegalCrossThreadCalls = False
                            Try

                                Try
                                    Dim j As Integer

                                    For i As Integer = 0 To (TextBox2.Text - 1)


                                        If EnableSecuence2 = True Then
                                            'Exit For

                                            Button6.Enabled = False

                                            ReadEnabelStop = False
                                            Secuencia.Enabled = False


                                            dr = cmd.ExecuteReader()
                                            If dr.HasRows Then
                                                j = 1
                                                While dr.Read()

                                                    If dr(0).ToString = "HOME" Then
                                                        Dim pos(3), succ, lin As String
                                                        Dim Wait As Integer
                                                        VelGen = dr(6)
                                                        AcelGen = dr(7)
                                                        GoHome()
                                                        Wait = dr(5)
                                                        System.Threading.Thread.Sleep(Wait)
                                                    Else
                                                        Dim pos(3), succ, lin As String
                                                        Dim Wait As Integer

                                                        pos(0) = dr(0)
                                                        pos(1) = dr(1)
                                                        pos(2) = dr(2)
                                                        succ = dr(3)
                                                        lin = dr(4)
                                                        Wait = dr(5)
                                                        Xp = pos(0)
                                                        Yp = pos(1)
                                                        Zp = pos(2)
                                                        CoorX = Xp
                                                        CoorY = Yp
                                                        CoorZ = Zp
                                                        VelGen = dr(6)
                                                        AcelGen = dr(7)
                                                        NmcReadStatus(4, SEND_INPUTS)
                                                        If IoInBitVal(4, 0) = False Then

                                                            EnableLecturaIO = True
                                                            Exit For
                                                        End If



                                                        If lin = "1" Then
                                                            MovimientoLienal = True
                                                            res = MovLin()
                                                        Else
                                                            MovimientoLienal = False
                                                            res = Func_2()



                                                        End If
                                                        If succ = "0" Then
                                                            IoClrOutBit(4, 6)
                                                            'IoSetOutBit(4, 4)

                                                        ElseIf succ = "1" Then
                                                            IoSetOutBit(4, 6)

                                                            '  IoClrOutBit(4, 4)
                                                            ' System.Threading.Thread.Sleep(50)
                                                            '  IoSetOutBit(4, 4)
                                                        End If

                                                        System.Threading.Thread.Sleep(Wait)
                                                        j = j + 1

                                                    End If
                                                End While
                                                FirstInteration = True

                                            End If
                                            dr.Close()
                                        End If
                                        ReadEnabelStop = True






                                    Next
                                    EnableSecuence = False
                                    EnableLecturaIO = True
                                    Button6.Enabled = True
                                    Secuencia.Enabled = True

                                Catch ex As Exception
                                    MsgBox(ex.ToString)
                                End Try
                            Catch ex As Exception
                                MsgBox(ex.ToString)

                            End Try
                            dr.Close()

                        Else


                        End If
                    Else
                        RichTextBox1.AppendText("Asigne un numero de repeticiones a realizar" & vbCrLf)


                    End If
                ElseIf ComboBox1.Text = "" Then
                    RichTextBox1.AppendText("Seleccione una Rutina" & vbCrLf)
                End If








            End If
            ' System.Threading.Thread.Sleep(10)

        End While
    End Sub
    Public Function setGains(ByVal motor As Integer, ByVal kp As Integer, ByVal kd As Integer, ByVal ki As Integer, ByVal EL As Integer, ByVal DC As Integer)
        ServoSetGain(motor, kp, kd, ki, 0, 255, 0, EL, 1, DC)


    End Function

    Private Sub LecturaIO_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles LecturaIO.DoWork
        'While (True)

        '    If EnableLecturaIO = True Then

        '        NmcReadStatus(4, SEND_INPUTS)
        '        If IoInBitVal(4, 1) = False Then
        '            Rutina()

        '        End If
        '        NmcReadStatus(4, SEND_INPUTS)
        '        If IoInBitVal(4, 2) = False Then
        '            JustHoming()


        '            EnableLecturaIO = True

        '        End If


        '    End If
        'End While
    End Sub
    Public Function Rutina()
        If ComboBox1.Text <> "" And TextBox2.Text IsNot Nothing Then



            Dim bol As Boolean
            bol = IsNumeric(TextBox2.Text)
            If bol = True Then



                EnableLecturaIO = False

                ReadEnabelStop = True
                System.Threading.Thread.Sleep(10)

                EnableSecuence = True
                EnableSecuence2 = True
            Else
                RichTextBox1.AppendText("Asigne un numero de repeticiones a realizar" & vbCrLf)

            End If
        ElseIf ComboBox1.Text = "" Then
            RichTextBox1.AppendText("Seleccione una Rutina" & vbCrLf)
        End If


    End Function
    Public Function JustHoming()
        Dim statbyte(3) As Int32
        MovimientoLienal = False
    GoHome()
        For i As Integer = 0 To 2
            ServoStopMotor(i + 1, 0)
        Next
       Home()
        ServoSetPos(1, ServoGetPos(1) * 0 + (1040 * ConversionFactor1) - (0))
        ServoSetPos(2, ServoGetPos(2) * 0 + (920 * ConversionFactor2) - (0))
        ServoSetPos(3, ServoGetPos(3) * 0 + (1040 * ConversionFactor3) - (0))
        NmcReadStatus(1, SEND_POS)  'Read the current position data from the cotnroller
        NmcReadStatus(2, SEND_POS)
        NmcReadStatus(3, SEND_POS)
        HomePos(1) = ServoGetPos(1)
        HomePos(2) = ServoGetPos(2)
        HomePos(3) = ServoGetPos(3)
        IoBitDirIn(4, 0)
        IoBitDirIn(4, 1)
        IoBitDirIn(4, 2)


  


    End Function
    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        RichTextBox1.Clear()

    End Sub

   

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim i, j As Integer
        Dim a, b As Integer
        Dim num, x, y, z, Succ, Lin, Delay, Velocidad, Aceleracion, intera, void As String
        intera = Secuencia.RowCount

        conn.Close()
        conn.Open()
        dr.Close()
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text
        sql = "DELETE FROM " & ComboBox1.SelectedItem & ""
        'sql = "DELETE FROM RUTINAS WHERE RUTINA='" & ComboBox1.SelectedItem.ToString & "'"
         cmd.CommandText = sql
        Try
            cmd.ExecuteNonQuery()


        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try


        conn.Close()
        conn.Open()
        dr.Close()

        For k As Integer = 0 To intera - 2
            'Dim pos(3), void, Lin, Vel, Acel, delay As String
            x = Secuencia.Item(0, k).Value
            y = Secuencia.Item(1, k).Value
            z = Secuencia.Item(2, k).Value
            void = Secuencia.Item(3, k).Value
            Lin = Secuencia.Item(4, k).Value
            Delay = Secuencia.Item(5, k).Value
            Velocidad = Secuencia.Item(6, k).Value
            Aceleracion = Secuencia.Item(7, k).Value
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            'sql = "INSERT INTO RUTINAS (RUTINA, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION)"
            'sql += "VALUES('" & ComboBox1.SelectedItem.ToString & "','" & x & "','" & y & "','" & z & "','" & void & "','" & Lin & "','" & Delay & "','" & Velocidad & "','" & Aceleracion & "')"
            sql = "INSERT INTO " & ComboBox1.SelectedItem & " (POSICION, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION)"
            sql += "VALUES('" & k + 1 & "','" & x & "','" & y & "','" & z & "','" & void & "','" & Lin & "','" & Delay & "','" & Velocidad & "','" & Aceleracion & "')"
            cmd.CommandText = sql


            Try
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try



        Next


    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            If ComboBox1.Text <> "" Then
                ActualizarSecuenceGrid(2)
                Button11.Enabled = True
                Button12.Enabled = True
                Button1.Enabled = False
                Button2.Enabled = False
                Button3.Enabled = False
                Button4.Enabled = False
                Button8.Enabled = False
                Button9.Enabled = False

            End If
        Else
            Secuencia.DataSource = Nothing
            Secuencia.Refresh()

            ActualizarSecuenceGrid(1)
            Button11.Enabled = False
            Button12.Enabled = False
            Button1.Enabled = True
            Button2.Enabled = True
            Button3.Enabled = True
            Button4.Enabled = True
            Button8.Enabled = True
            Button9.Enabled = True


        End If

    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim i, j As Integer
        Dim a As Integer
        Dim Last_pos As String

        j = 1

        conn.Close()
        conn.Open()
        dr.Close()
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT MAX(POSICION) AS ULTIMAPOS FROM " & ComboBox1.SelectedItem & ""

        i = Posiciones.CurrentRow.Index
        Try
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()

                    Last_pos = dr("ULTIMAPOS")
                    ' MsgBox(Last_pos)

                End While

            End If
            dr.Close()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        '  MsgBox(DataGridView1.Item(0, i).Value)
        a = Posiciones.Item(0, i).Value





        cmd.Connection = conn
        cmd.CommandType = CommandType.Text

        cmd.CommandText = "SELECT X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION FROM POSICIONES WHERE NUM=" & a & ""

        Try
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()
                    cmd2.Connection = conn
                    cmd2.CommandType = CommandType.Text
                    'sql2 = "INSERT INTO RUTINAS (RUTINA, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION)"
                    'sql2 += "VALUES('" & ComboBox1.SelectedItem.ToString & "'," & dr(0) & ",'" & dr(1) & "','" & dr(2) & "','" & dr(3) & "','" & dr(4) & "','" & dr(5) & "','" & dr(6) & "','" & dr(7) & "')"
                    sql2 = "INSERT INTO " & ComboBox1.SelectedItem.ToString & " (POSICION, X, Y, Z, SUCC, LIN, DELAY, VELOCIDAD, ACELERACION)"
                    sql2 += "VALUES('" & Last_pos + 1 & "','" & dr(0) & "','" & dr(1) & "','" & dr(2) & "','" & dr(3) & "','" & dr(4) & "','" & dr(5) & "','" & dr(6) & "','" & dr(7) & "')"

                    cmd2.CommandText = sql2
                    Indx = Indx + 1

                    Try
                        cmd2.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.ToString)

                    End Try
                    ActualizarSecuenceGrid(1)

                End While

            End If
            dr.Close()


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        ActualizarSecuenceGrid(2)


    End Sub


    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        If ComboBox1.SelectedItem <> Nothing Then

            conn.Close()
            conn.Open()
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            sql = "drop table " & ComboBox1.SelectedItem
            cmd.CommandText = sql
            SQLJustExe()


            cmd.CommandType = CommandType.Text

            sql = "delete from rutinas where rutina='" & ComboBox1.SelectedItem & "'"
            cmd.CommandText = sql
            SQLJustExe()

            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT DISTINCT RUTINA FROM RUTINAS"
            ComboBox1.Items.Clear()

            Try
                dr = cmd.ExecuteReader()
                While dr.Read()
                    ComboBox1.Items.Add(dr(0))

                End While
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
            ComboBox1.Text = ""
            Secuencia.DataSource = Nothing
            Secuencia.Rows.Clear()



            dr.Close()

        End If
    End Sub
End Class
